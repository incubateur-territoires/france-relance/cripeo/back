import { createLogger, format, transports, addColors } from 'winston';
import GelfTransport from 'winston-gelf';

class LoggerWinston {
  constructor() {
    const colorsLogger = {
      error: 'bold red',
      warn: 'yellow',
      info: 'cyan',
      debug: 'green',
      important: 'blue bold',
    };

    addColors(colorsLogger);

    let gelfTransporter;
    try {
      gelfTransporter = new GelfTransport({
        gelfPro: {
          fields: {
            facility: 'sent with winston',
            APP_NAME: process.env.APP_NAME,
            ENV: process.env.NODE_ENV,
          },
          adapterName: 'udp',
          adapterOptions: {
            host: process.env.GRAYLOG_HOST,
            port: process.env.GRAYLOG_PORT,
            hostName: process.env.APP_SERVER,
          },
        },
        format: format.printf((info) => {
          return `[${info.timestamp}] [${info.level.toUpperCase()}]: ${
            info.message
          }`;
        }),
        prettyPrint: true,
      });
    } catch (error) {
      console.log(error);
    }

    const transportsOptions = [
      new transports.File({
        filename: process.env.ERROR_LOGS || './logs/errors_logs.log',
        level: 'error',
      }),
      new transports.File({
        filename: process.env.IMPORTANTS_LOGS || './logs/importants_logs.log',
        level: 'important',
      }),

      new transports.File({
        filename: process.env.IMPORTANTS_LOGS || './logs/info_logs.log',
        level: 'info',
        prettyPrint: true,
        format: format.combine(
          format.printf((info) => {
            const infoUp = info.level.toUpperCase();
            return `[${info.timestamp}] [${infoUp}]: ${info.message}`;
          }),
          format.colorize({
            all: true,
          })
        ),
      }),

      new transports.Console({
        prettyPrint: true,
        format: format.combine(
          format.printf((info) => {
            const infoUp = info.level.toUpperCase();
            return `[${info.timestamp}] [${infoUp}]: ${info.message}`;
          }),
          format.colorize({
            all: true,
          })
        ),
      }),
    ];
    if (process.env.GRAYLOG_HOST && gelfTransporter)
      transportsOptions.push(gelfTransporter);

    this.logger = createLogger({
      exitOnError: false,
      level: 'info',
      levels: { error: 0, warn: 1, important: 2, info: 3, debug: 4 },
      format: format.combine(
        format.timestamp({
          format: 'YYYY-MM-DD HH:mm:ss',
        }),
        format.errors({ stack: true })
      ),
      defaultMeta: { service: process.env.APP_ID || 'cripeo-back' },
      transports: transportsOptions,
    });
    this.logger.info('Moteur de log initialisé...');
  }

  static getInstance() {
    if (!this.instance) {
      console.log('Initialisation du moteur de log...');
      this.instance = new LoggerWinston();
    }

    return this.instance;
  }
}

export default new LoggerWinston();
