import KeycloakAdminClient from '@keycloak/keycloak-admin-client';
import config from './config';

class KeycloakADM {
  constructor() {
    this._keycloak = null;
  }

  initKeycloak() {
    try {
      if (this._keycloak) {
        console.warn('Reload Keycloak!');
        return this._keycloak;
      } else {
        console.log('Initialisation de Keycloak ADM...');
        this._keycloak = new KeycloakAdminClient(config.keycloakAdmin);
        this._keycloak.auth(config.keycloakAdmin);

        setInterval(
          () => this._keycloak.auth(config.keycloakAdmin),
          300 * 1000
        );
        return this._keycloak;
      }
    } catch (e) {
      console.log('Erreur initKeycloak() dans keycloak_admin.js ', e);
      // eslint-disable-next-line no-undef
      initKeycloak();
    }
  }

  getKeycloak() {
    if (!this._keycloak) {
      console.error(
        `Keycloak ADM n'a pas été initialisé. Lancer "initkeycloak" avant.`
      );
    }
    return this._keycloak;
  }
}

class Singleton {
  constructor() {
    if (!Singleton.instance) {
      Singleton.instance = new KeycloakADM();
    }
  }

  getInstance() {
    return Singleton.instance;
  }
}

module.exports = Singleton;
