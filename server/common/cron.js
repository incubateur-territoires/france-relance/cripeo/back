import CronJob from 'node-cron';

import {
  rappelJob,
  sendMailSuppressionJob,
  suppressionDossiersClosJob,
} from './cronJobs/rappelJob';
import { cron } from './config';

const { cron_active, cron_suivi_time, cron_dossierClos_active } = cron;

exports.initScheduledJobs = () => {
  const scheduledJobFunction = CronJob.schedule(cron_suivi_time, async () => {
    try {
      const rappel = await rappelJob();
      console.log('CRON rappel status', rappel ? rappel.status : '');
    } catch (err) {
      console.log('Erreur CRON rappel', err);
    }

    console.log("Lancement de l'analyse des suivis fait");
  });

  const scheduledJobDossierClos = CronJob.schedule(
    cron_suivi_time,
    async () => {
      try {
        const sendMailSuppression = await sendMailSuppressionJob();
        console.log(
          'CRON sendMailSuppression status',
          sendMailSuppression ? sendMailSuppression.status : ''
        );
      } catch (err) {
        console.log('Erreur CRON sendMailSuppression', err);
      }
      try {
        const suppressionDossiersClos = await suppressionDossiersClosJob();
        console.log(
          'CRON suppressionDossiersClos status',
          suppressionDossiersClos ? suppressionDossiersClos.status : ''
        );
      } catch (err) {
        console.log('Erreur CRON suppressionDossiersClos', err);
      }

      console.log("Lancement de l'analyse des suivis fait");
    }
  );

  if (strToBool(cron_active)) {
    console.log('Initialisation CRON RAPPEL...');
    console.log('date', new Date());
    scheduledJobFunction.start();
  }
  if (strToBool(cron_dossierClos_active)) {
    console.log('Initialisation CRON DOSSIER CLOS...');
    console.log('date', new Date());
    scheduledJobDossierClos.start();
  }
};

function strToBool(s) {
  const regex = /^\s*(true|1|on)\s*$/i;
  return regex.test(s);
}
