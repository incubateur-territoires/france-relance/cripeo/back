import data from './messageHistorisation.json';

export const messageHistorisation = () => {
  return data;
};
export const textMessage = (tagValue, labelValue) => {
  const messageHistory = Object.values(messageHistorisation());
  const massages = messageHistory.find(
    (history) => history.tag === tagValue
  ).message;
  const text = massages.find((message) => message.label === labelValue).text;
  return text;
};
