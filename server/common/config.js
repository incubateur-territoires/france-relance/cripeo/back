const path = require('path');
const rootPath = path.normalize(__dirname + '/..');

function strToBool(s) {
  const regex = /^\s*(true|1|on)\s*$/i;
  return regex.test(s);
}

const config = {
  root: rootPath,
  app: {
    name: process.env.APP_NAME,
    uri: process.env.APP_URI,
  },
  db: process.env.MONGO || 'mongodb://cripeo-db/cripeo',
  server: process.env.APP_SERVER || 'localhost',
  port: process.env.PORT || 4583,
  proxy: {
    url: process.env.PROXY_URL || '',
    user: process.env.PROXY_USER || '',
    password: process.env.PROXY_PWD || '',
    port: process.env.PROXY_PORT || '',
    host: process.env.PROXY_HOST || '',
    on: process.env.PROXY_ON || false,
  },
  keycloak: {
    realm: process.env.KEYCLOAK_CLIENT_REALM || '',
    bearerOnly: process.env.KEYCLOAK_CLIENT_BEARERONLY || '',
    serverUrl: process.env.KEYCLOAK_CLIENT_SERVER_URL || '',
    'ssl-required': process.env.KEYCLOAK_CLIENT_SSL_REQUIRED || '',
    clientId: process.env.KEYCLOAK_CLIENT_CLIENT_ID || '',
    realmPublicKey: process.env.KEYCLOAK_CLIENT_REALM_PUBLIC_KEY || '',
    credentials: {
      secret: process.env.KEYCLOAK_CLIENT_SECRET || '',
    },
    newUserCredentials: {
      typeCredential:
        process.env.KEYCLOAK_NEWUSER_CREDENTIAL_TYPE || 'password',
      value: process.env.KEYCLOAK_NEWUSER_CREDENTIAL_DEFAULT_VALUE || '@é956Az',
      temporary: process.env.KEYCLOAK_NEWUSER_CREDENTIAL_TEMPORARY || true,
    },
  },
  keycloakAdmin: {
    baseUrl: process.env.KEYCLOAK_ADMIN_URL,
    username: process.env.KEYCLOAK_ADMIN_USER,
    password: process.env.KEYCLOAK_ADMIN_PWD,
    grantType: process.env.KEYCLOAK_ADMIN_GRANT_TYPE,
    clientId: process.env.KEYCLOAK_ADMIN_CLIENT_ID,
    realmName: process.env.KEYCLOAK_ADMIN_REAL_NAME,
  },
  atlas: {
    fluxCirco: process.env.FLUX_CIRCO,
  },
  api_bano: {
    url: process.env.API_BANO_URL,
  },
  whitelist: process.env.WHITELISTCORS.split(',') || [],
  ged: {
    url: process.env.GED_URL,
    user: process.env.GED_USERNAME,
    password: process.env.GED_PASSWORD,
    env: process.env.GED_ENV || 'Recette',
    refSite: process.env.refSite,
  },
  mailer: {
    host: process.env.MAILER_HOST || 'relais-int-smtpauth',
    port: process.env.MAILER_PORT || 25,
    emailFrom: {
      address: process.env.MAILER_EMAIL,
      name: 'Cripéo',
    },
    ignoreCertificat: process.env.MAILER_IGNORE_CERTIFICATE || 'true',
    authentication: process.env.MAILER_AUTHENTICATION || 'true',
    auth: {
      user: process.env.MAILER_AUTH_USER || null,
      password: process.env.MAILER_AUTH_PASSWORD || null,
      type: process.env.MAILER_AUTH_TYPE || 'login',
    },
    debug: process.env.MAILER_DEBUG || 'false',
    actif: strToBool(process.env.ACTIF_MAIL || 'false'),
  },
  cron: {
    cron_key:
      process.env.CRON_KEY || 'c49nUJthFrwF8mx7h3piR1PkWayv2TTMLph6Uwc6',
    cron_active: process.env.CRON_ACTIVE || 'false',
    cron_dossierClos_active: process.env.CRON_DOSSIERCLOS_ACTIVE || 'false',
    cron_suivi_time: process.env.CRON_SUIVI_TIME || '10 9 * * *',
  },
};

module.exports = config;
