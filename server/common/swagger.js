const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load(path.join(__dirname, 'api.yml'));
import * as path from 'path';
import errorHandler from '../api/middlewares/error.handler';

export default function swagger(app, routes) {
  return new Promise((resolve) => {
    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    app.enable('case sensitive routing');
    app.enable('strict routing');

    routes(app);

    // eslint-disable-next-line no-unused-vars, no-shadow
    app.use(errorHandler);
    return resolve();
  });
}
