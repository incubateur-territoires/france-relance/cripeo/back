import Keycloak from 'keycloak-connect';
const config = {
  realm: process.env.KEYCLOAK_CLIENT_REALM || '',
  bearerOnly: process.env.KEYCLOAK_CLIENT_BEARERONLY || '',
  serverUrl: process.env.KEYCLOAK_CLIENT_SERVER_URL || '',
  'ssl-required': process.env.KEYCLOAK_CLIENT_SSL_REQUIRED || '',
  clientId: process.env.KEYCLOAK_CLIENT_CLIENT_ID || '',
  publicKey: process.env.KEYCLOAK_CLIENT_REALM_PUBLIC_KEY || '',
  secret: process.env.KEYCLOAK_CLIENT_SECRET || '',
};

let _keycloak;

var keycloakConfig = {
  clientId: config.clientId,
  bearerOnly: config.bearerOnly,
  serverUrl: config.serverUrl,
  realm: config.realm,
  realmPublicKey: config.publicKey,
  secret: config.secret,
};

function initKeycloak(memoryStore) {
  if (_keycloak) {
    console.warn("Essaye d'initialiser Keycloak encore!");
    return _keycloak;
  } else {
    console.log('Initialisation de Keycloak...');
    _keycloak = new Keycloak({ store: memoryStore }, keycloakConfig);
    return _keycloak;
  }
}

function getKeycloak() {
  if (!_keycloak) {
    console.error(
      `Keycloak n'a pas été initialisé. Lancer "initkeycloak" avant.`
    );
  }
  return _keycloak;
}

module.exports = {
  initKeycloak,
  getKeycloak,
};

export default config;
