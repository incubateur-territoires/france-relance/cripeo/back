import config from '../config';
import axios from 'axios';

const { cron_key } = config.cron;

const rappelJob = async () => {
  try {
    const response = await axios.get(
      `http://localhost:${config.port}/api/v1/rappels/acteur/` + cron_key
    );
    console.log('rappelJob status', response.status);
    return response;
  } catch (err) {
    console.log('Erreur rappelJob', err);
  }
};
const sendMailSuppressionJob = async () => {
  try {
    const response = await axios.get(
      `http://localhost:${config.port}/api/v1/rappels/dossiers/emailsClos/` +
        cron_key
    );
    console.log(
      'sendMailSuppressionJob status et message',
      response.status,
      response.message
    );
    return response;
  } catch (err) {
    console.log('Erreur sendMailSuppressionJob', err);
  }
};
const suppressionDossiersClosJob = async () => {
  try {
    const response = await axios.get(
      `http://localhost:${config.port}/api/v1/rappels/dossiers/supprimerClos/` +
        cron_key
    );
    console.log(
      'suppressionDossiersClosJob status et message',
      response.status,
      response.message
    );
    return response;
  } catch (err) {
    console.log('Erreur suppressionDossiersClosJob', err);
  }
};

export { rappelJob, sendMailSuppressionJob, suppressionDossiersClosJob };
