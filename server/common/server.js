import Express from 'express';
import cookieParser from 'cookie-parser';
import * as path from 'path';
import * as bodyParser from 'body-parser';
import * as http from 'http';
import * as os from 'os';
import l from './logger';
import oas from './swagger';
import glob from 'glob';
import config from './config';
import mongoose from 'mongoose';
import cors from 'cors';
import scheduledFunctions from './cron';
import nocache from 'nocache';

const fileUpload = require('express-fileupload');

scheduledFunctions.initScheduledJobs();

let session = require('express-session');
let memoryStore = new session.MemoryStore();

const keycloak = require('./keycloak');
const keycloakADM = require('./keycloak_admin');

new keycloakADM().getInstance().initKeycloak();

mongoose.connect(config.db, {
  useFindAndModify: false,
  useUnifiedTopology: true,
  useNewUrlParser: true,
});
mongoose.connection.on('error', () => {
  throw new Error('unable to connect to database at ' + config.db);
});
const models = glob.sync(config.root + '/api/models/*.js');
models.forEach(function (model) {
  require(model);
});

const whitelist = config.whitelist;
const corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  },
  optionsSuccessStatus: 200,
};

const app = new Express();

app.use(
  session({
    secret: 'ET%T6HZgz^!BC6',
    resave: false,
    saveUninitialized: true,
    store: memoryStore,
  })
);

keycloak.initKeycloak(memoryStore);

export default class ExpressServer {
  constructor() {
    const root = path.normalize(`${__dirname}/../..`);

    app.use(bodyParser.json({ limit: process.env.REQUEST_LIMIT || '100kb' }));
    app.use(
      bodyParser.urlencoded({
        extended: true,
        limit: process.env.REQUEST_LIMIT || '100kb',
      })
    );
    app.use(cors(corsOptions));
    app.use(bodyParser.text({ limit: process.env.REQUEST_LIMIT || '100kb' }));
    app.use(cookieParser(process.env.SESSION_SECRET));
    app.use(Express.static(`${root}/public`));
    app.use(keycloak.getKeycloak().middleware({}));
    app.use(
      fileUpload({
        useTempFiles: true,
        tempFileDir: '/uploads/',
      })
    );
    app.use(nocache());
  }

  router(routes) {
    this.routes = routes;
    return this;
  }

  listen(port = process.env.PORT) {
    const welcome = (p) => () =>
      l.info(
        `up and running in ${
          process.env.NODE_ENV || 'development'
        } @: ${os.hostname()} on port: ${p}}`
      );

    oas(app, this.routes)
      .then(() => {
        http.createServer(app).listen(port, welcome(port));
      })
      .catch((e) => {
        l.error(e);
        // eslint-disable-next-line no-process-exit
        process.exit(1);
      });

    return app;
  }
}
