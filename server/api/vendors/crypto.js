'use strict';

const crypto = require('crypto');

const ENCRYPTION_KEY = process.env.ENCRYPTION_KEY; // Must be 256 bits (32 characters)
const IV_LENGTH = 12; // For AES-GCM, this is 12
const AUTH_TAG_LENGTH = 16;
const ALGO = 'aes-256-gcm';

const Duplex = require('stream').Duplex;

function encrypt(text) {
  try {
    if (!text) return null;
    let iv = crypto.randomBytes(IV_LENGTH);

    let cipher = crypto.createCipheriv(ALGO, ENCRYPTION_KEY, iv, {
      AUTH_TAG_LENGTH,
    });

    let encrypted = Buffer.concat([
      cipher.update(text.toString()),
      cipher.final(),
      cipher.getAuthTag(),
    ]);

    return Buffer.concat([encrypted, iv]).toString('base64');
  } catch (err) {
    throw 'Erreur Cypher: ' + err + '\n' + text;
  }
}

function decrypt(text) {
  try {
    if (!text || text == null || text == undefined || text == 'null') return '';
    let encryptedText = Buffer.from(text, 'base64');

    const iv = encryptedText.slice(-12);
    const authTag = encryptedText.slice(-28, -12);
    encryptedText = encryptedText.slice(0, encryptedText.length - 28);

    let decipher = crypto.createDecipheriv(ALGO, ENCRYPTION_KEY, iv, {
      AUTH_TAG_LENGTH,
    });
    decipher.setAuthTag(authTag);

    let decrypted = decipher.update(encryptedText, 'base64', 'utf8');

    decrypted += decipher.final('utf8');

    return decrypted.toString();
  } catch (err) {
    throw 'Erreur Decypher : ' + text + '-' + err;
  }
}

async function encryptStream(input) {
  try {
    let iv = crypto.randomBytes(IV_LENGTH);
    let encryptStreams = crypto.createCipheriv(ALGO, ENCRYPTION_KEY, iv, {
      AUTH_TAG_LENGTH,
    });

    const array = await new Promise((resolve) => {
      const encryptedDataArray = [];
      encryptedDataArray.push(iv);

      input.on('data', (data) => {
        const encryptedData = encryptStreams.update(data);
        encryptedDataArray.push(encryptedData);
      });

      input.on('end', () => {
        const finalData = encryptStreams.final();
        const authenticationTag = encryptStreams.getAuthTag();
        encryptedDataArray.push(finalData);
        encryptedDataArray.push(authenticationTag);
        resolve(encryptedDataArray);
      });
    });

    return Buffer.concat(array);
  } catch (err) {
    throw 'Erreur : ' + err;
  }
}

function decryptStream(output) {
  try {
    let iv = output.slice(0, 12);
    let authTag = output.slice(-16);
    let data = output.slice(12, output.length - 16);

    const decryptStreams = crypto.createDecipheriv(ALGO, ENCRYPTION_KEY, iv, {
      AUTH_TAG_LENGTH,
    });
    decryptStreams.setAuthTag(authTag);

    return Buffer.concat([decryptStreams.update(data), decryptStreams.final()]);
  } catch (err) {
    console.log('errer', err);
    throw 'Erreur : ' + err;
  }
}

function bufferToStream(buffer) {
  let stream = new Duplex();
  stream.push(buffer);
  stream.push(null);
  return stream;
}

module.exports = {
  decrypt,
  encrypt,
  encryptStream,
  decryptStream,
  bufferToStream,
};
