const TypeBoiteMail = Object.freeze({
  URGENTE: 'urgente',
  CLASSIQUE: 'classique',
  CREATION_URGENTE: 'creation_urgente',
  CREATION_NON_URGENTE: 'creation_non_urgente',
});

module.exports = TypeBoiteMail;
