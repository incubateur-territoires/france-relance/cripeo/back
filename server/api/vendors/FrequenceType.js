const FrequenceType = Object.freeze({
  ZERO: 0,
  DIRECT: 1,
});

const FrequenceLibelle = Object.freeze({
  [FrequenceType.ZERO]: 'Aucun e-mail',
  [FrequenceType.DIRECT]: 'E-mail en direct',
});

module.exports = { FrequenceType, FrequenceLibelle };
