class AccessResources {
  constructor(kauth) {
    this.kauth = kauth;
    this.roles = this.kauth.grant.access_token.content.realm_access.roles;
  }

  is_accessible(role) {
    return this.roles.reduce((acc, rl) => {
      if (acc) return acc;
      return rl === role;
    }, false);
  }

  get_kcId() {
    return this.kauth.grant.access_token.content.sub;
  }
}

module.exports = AccessResources;
