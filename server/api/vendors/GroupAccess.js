const GroupAccess = Object.freeze({
  ADMIN_FONCTIONNEL: 'Administrateur Fonctionnel',
  ADMIN_ORGA: 'Administrateur Organisationnel',
  UTILISATEUR: 'Utilisateur',
});

module.exports = GroupAccess;
