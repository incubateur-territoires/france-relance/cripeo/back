const RoleAccess = Object.freeze({
  ADMIN: 'adminCli',
  GESTIONNAIRE: 'gestionnaire',
  USER: 'user',

  LECTURE: 'Dossier_Lecture', // user get
  ECRITURE: 'Dossier_Ecriture', // user post put delete

  CREATION_ORGA: 'Organisation_Creation', // admin fonctionnel creation
  LISTE_ORGA: 'Organisations_Globales', // admin fonctionnel liste organisations

  GESTION_ORGA: 'Organisation_Gestion', // admin organisationel lecture + ecriture
});

module.exports = RoleAccess;
