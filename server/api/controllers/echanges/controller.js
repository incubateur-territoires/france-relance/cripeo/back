import LoggerWinston from '../../../common/loggerWinston';
import EchangesService from '../../services/echanges.service';
import CategoriesService from '../../services/categories.service';
import ActeursService from '../../services/acteurs.service';
import GedService from '../../services/ged.service';
import DossiersService from '../../services/dossiers.service';
import DocumentsService from '../../services/documents.service';
import UtilisateursService from '../../services/utilisateurs.service';
import MailsService from '../../services/mail.service';

import HistoriquesService from '../../services/historique.service';
import { textMessage } from '../../../common/messageHistorisation';

import AccessResources from '../../vendors/AccessRessouces';
import config from '../../../common/config';

import { v4 as uuidv4 } from 'uuid';

const fs = require('fs');
import { writeFile, rm } from 'node:fs/promises';
const { encryptStream } = require('../../vendors/crypto');

const logger = LoggerWinston.logger;

const LIBELLE_CREATION_ECHANGE = "Création de l'échange";

export class Controller {
  all(req, res) {
    EchangesService.all().then((r) => res.json(r));
  }

  byId(req, res) {
    EchangesService.byId(req.params.id).then((r) => {
      if (r) res.json(r);
      else res.status(404).end();
    });
  }

  async create(req, res) {
    if (!req.kauth) return res.status(401).send('Authentification manquante');
    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();

    const user = await UtilisateursService.byIdKc(kc_id);
    const acteur = await ActeursService.byId(user.organisation.acteur_id);

    if (!req.body._id) {
      let data = req.body;
      let categorie = await CategoriesService.byId(data.categorie.categorie_id);
      data.categorie.libelle = categorie.libelle;
      data.categorie.emetteur = categorie.emetteur.entite;
      data.categorie.destinataire = categorie.destinataire.entite;

      let emetteur = categorie.emetteur;
      let destinataire = categorie.destinataire;

      data.action.push({
        libelle: LIBELLE_CREATION_ECHANGE,
        emetteur: emetteur.entite,
        emetteur_id: emetteur.id,
        destinataire: destinataire.entite,
        destinataire_id: destinataire.id,
        dateRealisation: new Date(),
        createdBy: data.createdBy,
      });

      data.clos = 0;

      const echange = await EchangesService.create(data);

      logger.important(
        `Création d'un échange : { id: ${echange._id}, utilisateur_kc: ${kc_id} }`
      );

      await switchClosDossier(req.kauth, data.dossier_id);

      await UtilisateursService.addNotif(
        echange.dossier_id,
        echange.categorie.categorie_id,
        echange.categorie.destinataire_id,
        data.urgent
      );

      if (destinataire && config.mailer.actif) {
        const typeBoiteEnvoi = data.urgent
          ? ['creation_urgente', 'urgente']
          : ['creation_non_urgente', 'classique'];
        const emails = await MailsService.getMail(
          destinataire.id,
          typeBoiteEnvoi
        );
        const priority = data.urgent ? true : false;

        const body = `Bonjour,<br/>
        <br/>Nous venons de déposer sur la plateforme Cripéo une information préoccupante.   
        <br/>Cliquez sur ce <a href="${config.app.uri}">LIEN</a> pour accéder au dossier correspondant.<br/>
        <br/>Ceci est un email automatique, merci de ne pas répondre.`;

        if (emails) {
          await MailsService.sendMail(
            emails,
            "CRIPÉO : Création d'un échange",
            body,
            priority
          );
        }
      } else if (!config.mailer.actif) {
        // Mail non actif
      } else {
        console.log('Pas de destinataire');
      }

      const text = textMessage('ÉCHANGES', 'creation');
      const messageData = {
        dossier_id: echange.dossier_id,
        acteur_id: user.organisation.acteur_id,
        acteur: acteur.entite,
        createdBy: user.libelle,
        message: text,
        tag: 'ÉCHANGES',
      };
      await HistoriquesService.create(messageData);

      const echanges = await EchangesService.byDossierId(data.dossier_id);
      const objRetour = {
        echanges,
        echange,
      };
      res.json(objRetour);
    } else {
      const echange = await EchangesService.modify(req.body._id, req.body);
      const echanges = await EchangesService.byDossierId(echange.dossier_id);
      const objRetour = {
        echanges,
        echange,
      };

      const text = textMessage('ÉCHANGES', 'modify');
      const messageData = {
        dossier_id: echange.dossier_id,
        acteur_id: user.organisation.acteur_id,
        acteur: acteur.entite,
        createdBy: user.libelle,
        message: text,
        tag: 'ÉCHANGES',
      };
      await HistoriquesService.create(messageData);

      res.json(objRetour);
    }
  }

  byDossierId(req, res) {
    let dossier_id = req.params.id;
    EchangesService.byDossierId(dossier_id).then((r) => res.json(r));
  }

  byDossierIdByActeur(req, res) {
    const { dossier_id, acteur_id } = req.params;
    EchangesService.byDossierIdByActeur(dossier_id, acteur_id).then((r) =>
      res.json(r)
    );
  }

  byDossierIdByActeurAvecInformation(req, res) {
    const { dossier_id, acteur_id } = req.params;
    EchangesService.byDossierIdByActeurAvecInformation(
      dossier_id,
      acteur_id
    ).then((r) => res.json(r));
  }

  async addAction(req, res) {
    if (!req.kauth) return res.status(401).send('Authentification manquante');
    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();
    const user = await UtilisateursService.byIdKc(kc_id);
    let data = req.body.action;
    const { libelle, commentairePublic, commentairePrive } = data;
    if (
      !(libelle && (commentairePublic || commentairePrive)) &&
      libelle !== LIBELLE_CREATION_ECHANGE
    )
      return res
        .status(400)
        .send("Des informations de l'action sont manquantes");
    const echange = await EchangesService.byId(req.body.echange_id);

    let hasPerm = false;
    if (
      await EchangesService.droitModificationActeur(
        echange.dossier_id,
        user.organisation.acteur_id
      )
    )
      hasPerm = true;

    if (user.equipes) {
      hasPerm = user.equipes.map(async (equipe) => {
        let hasPermTemp = false;
        if (
          await EchangesService.droitModificationActeur(
            echange.dossier_id,
            equipe.acteur_id
          )
        )
          hasPermTemp = true;
        return hasPermTemp;
      });
    }

    if (hasPerm) {
      if (echange.categorie.emetteur_id.equals(req.body.acteur_id)) {
        data.emetteur_id = echange.categorie.emetteur_id;
        data.emetteur = echange.categorie.emetteur;
        data.destinataire_id = echange.categorie.destinataire_id;
        data.destinataire = echange.categorie.destinataire;
      } else {
        data.emetteur_id = echange.categorie.destinataire_id;
        data.emetteur = echange.categorie.destinataire;
        data.destinataire_id = echange.categorie.emetteur_id;
        data.destinataire = echange.categorie.emetteur;
      }

      data.dateRealisation = new Date();

      const echangeNew = await EchangesService.addAction(
        req.body.echange_id,
        data
      );

      const acteur = await ActeursService.byId(user.organisation.acteur_id);

      const text = textMessage('ÉCHANGES', 'action');
      const value = {
        dossier_id: echange.dossier_id,
        acteur_id: user.organisation.acteur_id,
        acteur: acteur.entite,
        createdBy: user.libelle,
        message: text,
        tag: 'ÉCHANGES',
      };
      await HistoriquesService.create(value);

      let destinataire = await ActeursService.byId(
        echange.categorie.destinataire_id
      );

      const echangeEstUrgente = echange.action[0].urgent === 1 ? true : false;

      if (destinataire) {
        const typeBoiteEnvoi = data.urgent
          ? echangeEstUrgente
            ? ['urgente', 'creation_urgente']
            : ['urgente', 'creation_non_urgente']
          : echangeEstUrgente
          ? ['classique', 'creation_urgente']
          : ['classique', 'creation_non_urgente'];

        const emails = await MailsService.getMail(
          data.destinataire_id,
          typeBoiteEnvoi
        );
        const priority = data.urgent ? true : false;

        const body = `Bonjour,<br/>
        <br/>Nous venons de déposer sur la plateforme Cripéo une information préoccupante.
        <br/>Cliquez sur ce <a href="${config.app.uri}">LIEN</a> pour accéder au dossier correspondant.<br/>
        <br/>Ceci est un email automatique, merci de ne pas répondre.`;

        if (emails) {
          MailsService.sendMail(
            emails,
            'CRIPÉO : Nouvelle action',
            body,
            priority
          );
        }
      } else {
        console.log('Pas de destinataire');
      }

      await UtilisateursService.addNotif(
        echange.dossier_id,
        echange.categorie.categorie_id,
        data.destinataire_id,
        data.urgent
      );

      if (data.document && data.document.length > 0) {
        for (let i = 0; i < data.document.length; i++) {
          await DocumentsService.modifierActionId({
            actionId: echangeNew.action[echangeNew.action.length - 1]._id,
            documentId: data.document[i].document_id,
          });
          let listeacteurs = [
            {
              acteur_id: data.emetteur_id,
            },
            {
              acteur_id: data.destinataire_id,
            },
          ];
          if (echange.information.length > 0) {
            for (let a = 0; a < echange.information.length; a++) {
              listeacteurs.push({
                acteur_id: echange.information[a].acteur_id,
              });
            }
          }
          await DocumentsService.modifyDroits({
            documentId: data.document[i].document_id,
            acteurs: listeacteurs,
          });
          logger.important(
            `Ajout de droits sur le document : { id: ${data.document[i].document_id}, utilisateur_kc: ${kc_id} }`
          );
        }
      }

      if (req.body.transfertDestinataire) {
        await EchangesService.copieEchange(
          req.body.echange_id,
          req.body.transfertDestinataire
        );
        await EchangesService.cloture(req.body.echange_id);
      }

      res.json(echangeNew);
    } else {
      logger.important(`Tentative d'accès : { utilisateur_kc: ${kc_id} }`);
      res.status(403).send('Accès non autorisé');
    }
  }

  async uploadDocument(req, res) {
    const { id } = req.params;

    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();
    const user = await UtilisateursService.byIdKc(kc_id);
    const acteur = await ActeursService.byId(user.organisation.acteur_id);

    const echange = await EchangesService.byId(id);
    await DossiersService.byId(echange.dossier_id);

    console.log('req.files', req.files);
    let myuuid = uuidv4();
    var timestamp = new Date().getTime();

    let data = {
      echange_id: id,
      titre: req.files.filedata.name,
      nom: '/documentsInterne/' + myuuid + '_' + timestamp,
      extension: req.files.filedata.extension,
      dossier_id: echange.dossier_id,
      tag: 'INTERNE',
    };

    try {
      writeFile(data.nom, req.files.filedata.data, (err) => {
        if (err) res.status(500).send({ error: err });
        console.log('documents interne has been uploaded');
      });
    } catch (e) {
      return res.status(500).send({ error: e });
    }
    await DocumentsService.create(data);

    const text = textMessage('DOCUMENTS', 'creation');
    const messageData = {
      dossier_id: echange.dossier_id,
      acteur_id: user.organisation.acteur_id,
      acteur: acteur.entite,
      createdBy: user.libelle,
      message: text,
      tag: 'DOCUMENTS',
    };
    await HistoriquesService.create(messageData);

    const documentList = await DocumentsService.byEchangeId(id);
    res.json(documentList);
  }

  async uploadDocumentAction(req, res) {
    const { id } = req.params;

    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();
    const user = await UtilisateursService.byIdKc(kc_id);
    const acteur = await ActeursService.byId(user.organisation.acteur_id);

    const echange = await EchangesService.byId(id);
    await DossiersService.byId(echange.dossier_id);

    let file;
    if (req.files) file = req.files;
    if (req.file) file = req.file;
    if (!file) return res.status(500).send({ error: 'Aucun fichier' });

    const allowedFileTypes = [
      'text/plain',
      'application/vnd.oasis.opendocument.text',
      'image/jpeg',
      'image/png',
      'application/pdf',
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      'application/msword',
    ];
    if (
      allowedFileTypes.findIndex((type) => type === file.filedata.mimetype) ===
      -1
    )
      return res
        .status(500)
        .send({ error: "Type de fichier n'est pas correct" });

    let filedata;
    if (file.filedata) filedata = file.filedata;
    else filedata = file;

    let fichierFile = await fs.createReadStream(filedata.tempFilePath);
    let fichierData = await encryptStream(fichierFile);

    let myuuid = uuidv4();
    var timestamp = new Date().getTime();
    let data = {
      echange_id: id,
      titre: filedata.name,
      nom: '/documentsInterne/' + myuuid + '_' + timestamp,
      extension: filedata.name.split('.').pop(),
      dossier_id: echange.dossier_id,
      tag: 'INTERNE',
    };

    const document = await DocumentsService.create(data);
    try {
      writeFile(data.nom, fichierData, (err) => {
        if (err) res.status(500).send({ error: err });
        console.log('Documents interne has been saved');
      });
    } catch (e) {
      return res.status(500).send({ error: e });
    }

    if (document) {
      const text = textMessage('DOCUMENTS', 'creation');
      const messageData = {
        dossier_id: echange.dossier_id,
        acteur_id: user.organisation.acteur_id,
        acteur: acteur.entite,
        createdBy: user.libelle,
        message: text,
        tag: 'DOCUMENTS',
      };
      await HistoriquesService.create(messageData);

      res.json(document);
    }
  }

  async integreDocumentAction(req, res) {
    if (!req.kauth) return res.status(401).send('Authentification manquante');
    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();
    const user = await UtilisateursService.byIdKc(kc_id);

    const acteur = await ActeursService.byId(user.organisation.acteur_id);

    const { echange_id } = req.params;
    const { documents } = req.body;

    let retour;
    try {
      retour = await EchangesService.integreDocumentAction(
        echange_id,
        documents
      );
    } catch (err) {
      logger.error(err);
      return res.status(500).send({ error: err });
    }

    // Ajouter les droits du destinataire aux Documents
    for (const element of documents) {
      if (
        element?.acteurs.findIndex(
          (act) =>
            act.acteur_id.toString() ===
            retour.categorie.destinataire_id.toString()
        ) === -1 &&
        element._id
      ) {
        await DocumentsService.modifyDroits({
          documentId: element._id,
          acteurs: element.acteurs.concat([
            { acteur_id: retour.categorie.destinataire_id },
          ]),
        });
      } else if (element.acteurs && element.acteurs.length === 0) {
        await DocumentsService.modifyDroits({
          documentId: element._id,
          acteurs: element.acteurs.concat([
            { acteur_id: retour.categorie.destinataire_id },
            { acteur_id: retour.categorie.emetteur_id },
          ]),
        });
      }
    }

    let documentList = await DocumentsService.byDossierIdByEquipes(
      retour.dossier_id,
      user
    );

    if (retour) {
      const text = textMessage('DOCUMENTS', 'integre');
      const messageData = {
        dossier_id: retour.dossier_id,
        acteur_id: user.organisation.acteur_id,
        acteur: acteur.entite,
        createdBy: user.libelle,
        message: text,
        tag: 'DOCUMENTS',
      };
      if (documents.length !== 0) await HistoriquesService.create(messageData);

      return res.json({
        msg: 'Transfert effectué',
        echange: retour,
        documents: documentList,
      });
    } else {
      return res
        .status(400)
        .send({ error: 'Problème dans le transfert de document' });
    }
  }

  async deleteFichier(req, res) {
    if (!req.kauth) return res.status(401).send('Authentification manquante');
    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();
    const user = await UtilisateursService.byIdKc(kc_id);

    const acteur = await ActeursService.byId(user.organisation.acteur_id);

    const { id } = req.params;
    const fichier = await DocumentsService.byId(id);
    let hasPerm = false;
    let hasPermi = false;

    if (
      await EchangesService.droitModificationActeur(
        fichier.dossier_id,
        user.organisation.acteur_id
      )
    )
      hasPerm = true;

    if (user.equipes) {
      hasPerm = user.equipes.map(async (equipe) => {
        let hasPermTemp = false;
        if (
          await EchangesService.droitModificationActeur(
            fichier.dossier_id,
            equipe.acteur_id
          )
        )
          hasPermTemp = true;
        return hasPermTemp;
      });
    }

    if (hasPerm) {
      hasPermi = true;

      let echange = await EchangesService.byId(fichier.echange_id);
      let allActions = await EchangesService.getListAllActionsByDossierId(
        fichier.dossier_id
      );
      let echangeActs = JSON.parse(JSON.stringify(echange.action));

      allActions.map((act) => {
        if (act.echange_id.toString() !== fichier.echange_id.toString()) {
          let actDoc = JSON.parse(JSON.stringify(act.document));

          // S'il y a le même document, on ne peut pas supprimer
          if (
            actDoc.findIndex(
              (element) => element.document_id.toString() === id.toString()
            ) !== -1
          ) {
            hasPermi = false;
          }
        }

        let actAvecDocIndex = echangeActs.findIndex(
          (acti) =>
            acti.document.findIndex(
              (element) => element.document_id.toString() === id.toString()
            ) !== -1
        );
        if (actAvecDocIndex !== echangeActs.length - 1) {
          hasPermi = false;
        }
      });

      const ech = await EchangesService.byDossierIdByActeurAvecInformation(
        fichier.dossier_id,
        user.organisation.acteur_id
      );
      let echang = JSON.parse(JSON.stringify(ech));
      let doc = JSON.parse(JSON.stringify(fichier));
      for (const echan of echang) {
        if (echan._id.toString() === fichier.echange_id.toString()) {
          const infor = echan.information;
          const info = JSON.parse(JSON.stringify(infor));
          let infoAct = [];
          for (const inf of info) {
            infoAct.push(inf.acteur_id);
          }
          doc.informationActeurs_id = infoAct;
        }
      }
      if (
        doc.informationActeurs_id?.length > 0 &&
        doc.informationActeurs_id?.findIndex(
          (infoId) =>
            infoId.toString() === user.organisation.acteur_id.toString()
        ) !== -1
      ) {
        hasPermi = false;
      }
    }

    if (hasPermi && hasPerm) {
      if (fichier.tag === 'INTERNE') {
        try {
          await rm(fichier.nom);
          console.log('documents interne has been deleted');
        } catch (e) {
          return res.status(500).send({ error: e });
        }
      } else {
        try {
          await GedService.effaceFichier(fichier.noderef);
        } catch (e) {
          // eslint-disable-next-line no-undef
          res.status(500).send({ error: e });
        }
      }

      await DocumentsService.suppression(id);

      const text = textMessage('DOCUMENTS', 'suppression');
      const messageData = {
        dossier_id: fichier.dossier_id,
        acteur_id: user.organisation.acteur_id,
        acteur: acteur.entite,
        createdBy: user.libelle,
        message: text,
        tag: 'DOCUMENTS',
      };
      await HistoriquesService.create(messageData);

      let documentList = await DocumentsService.byDossierIdByEquipes(
        fichier.dossier_id,
        user
      );

      logger.important(
        `Suppression d'un document : { id: ${fichier.titre}, utilisateur_kc: ${kc_id} }`
      );
      res.json(documentList);
    } else {
      res.status(403).send('Accès non autorisé');
    }
  }

  async clotureEchange(req, res) {
    if (!req.kauth) return res.status(401).send('Authentification manquante');
    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();
    const user = await UtilisateursService.byIdKc(kc_id);

    const acteur = await ActeursService.byId(user.organisation.acteur_id);

    const { id } = req.params;

    let hasPerm = false;
    if (
      await EchangesService.droitModificationActeur(
        id,
        user.organisation.acteur_id
      )
    )
      hasPerm = true;

    if (user.equipes) {
      hasPerm = await Promise.all(
        user.equipes.map(async (equipe) => {
          let hasPermTemp = false;
          if (
            await EchangesService.droitModificationActeur(id, equipe.acteur_id)
          )
            hasPermTemp = true;
          return hasPermTemp;
        })
      );
    }

    if (hasPerm) {
      const result = await EchangesService.cloture(id);

      const text = textMessage('ÉCHANGES', 'clôturer');
      const messageData = {
        dossier_id: result.dossier_id,
        acteur_id: user.organisation.acteur_id,
        acteur: acteur.entite,
        createdBy: user.libelle,
        message: text,
        tag: 'ÉCHANGES',
      };
      await HistoriquesService.create(messageData);

      await switchClosDossier(req.kauth, result.dossier_id);
      let echangeList = await EchangesService.byDossierIdByActeur(
        result.dossier_id,
        user.organisation.acteur_id
      );

      const echange = await EchangesService.byId(id);

      const echangeEstUrgente =
        echange.action[echange.action.length - 1].urgent === 1 ? true : false;
      const PrioritaireEchange = echange.action[0].urgent === 1 ? true : false;

      if (
        echange &&
        echange.categorie.destinataire_id &&
        echange.categorie.emetteur_id
      ) {
        const typeBoiteEnvoi = PrioritaireEchange
          ? echangeEstUrgente
            ? ['urgente', 'creation_urgente']
            : ['urgente', 'creation_non_urgente']
          : echangeEstUrgente
          ? ['classique', 'creation_urgente']
          : ['classique', 'creation_non_urgente'];

        const emails1 = await MailsService.getMail(
          echange.categorie.destinataire_id,
          typeBoiteEnvoi
        );
        const emails2 = await MailsService.getMail(
          echange.categorie.emetteur_id,
          typeBoiteEnvoi
        );
        const emails = emails1 + ',' + emails2;

        const body = `Bonjour,<br/>
        <br/>Un échange à été cloturé/réouvert dans un dossier.
        <br/>Vous pouvez toujours le consulter sur le lien suivant. <a href="${config.app.uri}">LIEN</a> <br/>
        <br/>Ceci est un email automatique, merci de ne pas répondre.`;

        if (emails) {
          MailsService.sendMail(
            emails,
            'CRIPÉO : Échange Clôturer',
            body,
            false
          );
        }
      } else {
        console.log('Pas de acteurs');
      }

      let echangeEquipeList;
      if (user.equipes) {
        echangeEquipeList = await Promise.all(
          user.equipes.map(async (equipe) => {
            return await EchangesService.byDossierIdByActeur(
              result.dossier_id,
              equipe.acteur_id
            );
          })
        );
      }

      if (Array.isArray(echangeEquipeList) && echangeEquipeList.length > 0)
        echangeList.push(echangeEquipeList);

      logger.important(
        `Changement de la cloture de l'échange : { id: ${id}, utilisateur_kc: ${kc_id} }`
      );

      res.json(echangeList);
    } else {
      res.status(403).send('Accès non autorisé');
    }
  }

  async setDroits(req, res) {
    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();
    const user = await UtilisateursService.byIdKc(kc_id);
    const acteur = await ActeursService.byId(user.organisation.acteur_id);

    const result = await EchangesService.setDroits(req.body);
    if (result) {
      const text = textMessage('ÉCHANGES', 'information');
      const messageData = {
        dossier_id: result.dossier_id,
        acteur_id: user.organisation.acteur_id,
        acteur: acteur.entite,
        createdBy: user.libelle,
        message: text,
        tag: 'ÉCHANGES',
      };
      if (req.body.information.length !== 0)
        await HistoriquesService.create(messageData);

      await EchangesService.byDossierIdByActeur(
        result.dossier_id,
        result.action[0].emetteur_id
      ).then((r) => res.json(r));
    }
  }

  async setLastActionUrgent(req, res) {
    const { echange_id } = req.params;
    const echange = await EchangesService.setLastActionUrgent(echange_id);
    res.json(echange);
  }

  async echangeEstLu(req, res) {
    const echange = await EchangesService.echangeEstLu(
      req.body.id,
      req.body.date
    );
    res.json(echange);
  }

  async deleteEchange(req, res) {
    if (!req.kauth) return res.status(401).send('Authentification manquante');
    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();
    const user = await UtilisateursService.byIdKc(kc_id);
    const acteur = await ActeursService.byId(user.organisation.acteur_id);

    const { id } = req.params;
    const echange = await EchangesService.byId(id);
    if (!echange) return res.status(404).send('Echange introuvable');
    if (echange.action && echange.action.length > 1)
      return res
        .status(403)
        .send("Echange ne peut être supprimé, nombre d'action > 1");
    let hasPerm = false;
    if (
      await EchangesService.droitModificationActeur(
        echange.dossier_id,
        user.organisation.acteur_id
      )
    )
      hasPerm = true;

    if (user.equipes) {
      hasPerm = user.equipes.map(async (equipe) => {
        let hasPermTemp = false;
        if (
          await EchangesService.droitModificationActeur(
            echange.dossier_id,
            equipe.acteur_id
          )
        )
          hasPermTemp = true;
        return hasPermTemp;
      });
    }

    let docs = [];
    let echanAct = JSON.parse(JSON.stringify(echange.action));
    for (const acti of echanAct) {
      let doc = JSON.parse(JSON.stringify(acti.document));
      if (doc && doc.length > 0) {
        docs = [].concat(acti.document);
      }
    }

    let allActions = await EchangesService.getListAllActionsByDossierId(
      echange.dossier_id
    );

    allActions = allActions.map((act) => {
      if (act.echange_id.toString() !== id.toString()) {
        let newAct = { ...act };
        return newAct;
      }
    });

    if (hasPerm) {
      for (const doc of docs) {
        let docInfo = await DocumentsService.byId(doc.document_id);

        if (
          docInfo !== null &&
          docInfo?.echange_id.toString() !== id.toString() &&
          docInfo.acteurs.length > 2
        ) {
          let newActeur = docInfo.acteurs.filter(
            (act) =>
              act.acteur_id.toString() !==
              echange.categorie.destinataire_id.toString()
          );

          allActions.map((act) => {
            if (act !== undefined && act.document) {
              let actDoc = JSON.parse(JSON.stringify(act.document));
              let actInfo = JSON.parse(JSON.stringify(act.information));

              if (
                actDoc.length > 0 &&
                actDoc.findIndex(
                  (element) =>
                    element.document_id.toString() ===
                    doc.document_id.toString()
                ) !== -1 &&
                (act.emetteur_id.toString() ===
                  echange.categorie.destinataire_id.toString() ||
                  act.destinataire_id.toString() ===
                    echange.categorie.destinataire_id.toString() ||
                  actInfo.findIndex(
                    (element) =>
                      element.acteur_id.toString() ===
                      echange.categorie.destinataire_id.toString()
                  ) !== -1)
              ) {
                newActeur = docInfo.acteurs;
              }
            }
          });

          // Si échange id de document n'est pas celle qu'on supprime, on change juste les droits sur le document
          await DocumentsService.modifyDroits({
            documentId: docInfo._id,
            acteurs: newActeur,
          });
        } else if (docInfo !== null) {
          let docNewActeur = [];
          let listActionAvecDoc = [];

          allActions.map((act) => {
            if (act !== undefined && act.document) {
              let actDoc = JSON.parse(JSON.stringify(act.document));
              let actInfo = JSON.parse(JSON.stringify(act.information));

              if (
                actDoc.length > 0 &&
                actDoc.findIndex(
                  (element) =>
                    element.document_id.toString() ===
                    doc.document_id.toString()
                ) !== -1
              ) {
                listActionAvecDoc.push(act);

                docInfo.acteurs.map((acte) => {
                  if (
                    acte.acteur_id.toString() === act.emetteur_id.toString() ||
                    acte.acteur_id.toString() ===
                      act.destinataire_id.toString() ||
                    actInfo?.findIndex(
                      (element) =>
                        element.acteur_id.toString() ===
                        acte.acteur_id.toString()
                    ) !== -1
                  ) {
                    docNewActeur.push(acte);
                  }
                });

                docNewActeur = docNewActeur.reduce((accumulator, current) => {
                  if (
                    !accumulator.find(
                      (item) =>
                        item.acteur_id.toString() ===
                        current.acteur_id.toString()
                    )
                  ) {
                    accumulator.push(current);
                  }
                  return accumulator;
                }, []);
              }
            }
          });

          let listEchangeAvecDoc = [];
          for (const act of listActionAvecDoc) {
            let echan = await EchangesService.byId(act.echange_id);
            let echange = JSON.parse(JSON.stringify(echan));
            listEchangeAvecDoc.push({
              echange_id: act.echange_id,
              dateCreation: echange.created_at,
            });
          }

          let date = new Date();
          let echangeId;
          listEchangeAvecDoc.map((ech) => {
            let echDate = new Date(ech.dateCreation);
            if (echDate.getTime() < date.getTime()) {
              date = echDate;
              echangeId = ech.echange_id;
            }
          });

          await DocumentsService.modifyEchangeId(doc.document_id, echangeId);

          if (docNewActeur.length > 0) {
            await DocumentsService.modifyDroits({
              documentId: doc.document_id,
              acteurs: docNewActeur,
            });
          } else {
            if (docInfo.tag === 'INTERNE') {
              try {
                rm(docInfo.nom);
                console.log('Documents interne has been deleted');
              } catch (e) {
                return res.status(500).send({ error: e });
              }
            } else {
              try {
                await GedService.effaceFichier(docInfo.noderef);
              } catch (e) {
                // eslint-disable-next-line no-undef
                res.status(500).send({ error: e });
              }
            }
            await DocumentsService.suppression(doc.document_id);
          }
        }
      }

      await EchangesService.delete(id);

      const text = textMessage('ÉCHANGES', 'suppression');
      const messageData = {
        dossier_id: echange.dossier_id,
        acteur_id: user.organisation.acteur_id,
        acteur: acteur.entite,
        createdBy: user.libelle,
        message: text,
        tag: 'ÉCHANGES',
      };
      await HistoriquesService.create(messageData);

      await switchClosDossier(req.kauth, echange.dossier_id);

      res.json({ msg: 'Echange supprimé' });
    }
  }
}

async function switchClosDossier(kauth, id) {
  const dossier = await DossiersService.byId(id);

  const accessResources = new AccessResources(kauth);
  const kc_id = accessResources.get_kcId();
  const user = await UtilisateursService.byIdKc(kc_id);
  const acteur = await ActeursService.byId(user.organisation.acteur_id);

  const echanges = await EchangesService.byDossierId(id);
  let allClos = true;

  if (echanges.length === 0) allClos = false;

  echanges.map((echange) => {
    if (!echange.clos) allClos = false;
  });

  if (allClos) {
    const dateClos = new Date();
    let dateSuppressionDossier = new Date(
      new Date().setMonth(new Date().getMonth() + 7)
    );
    dateSuppressionDossier.setDate(15);
    dateSuppressionDossier.setHours(10, 0, 0, 0);
    await DossiersService.cloture(
      id,
      allClos,
      dateClos,
      dateSuppressionDossier
    );
  } else {
    await DossiersService.cloture(id, allClos, undefined, undefined);
  }

  const text = textMessage('DOSSIER', 'clôturer');
  const messageData = {
    dossier_id: id,
    acteur_id: user.organisation.acteur_id,
    acteur: acteur.entite,
    createdBy: user.libelle,
    message: text,
    tag: 'DOSSIER',
  };
  if (dossier.clos !== allClos) await HistoriquesService.create(messageData);
}

export default new Controller();
