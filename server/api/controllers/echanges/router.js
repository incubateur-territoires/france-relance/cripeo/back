import * as express from 'express';
import controller from './controller';
import RoleAccess from '../../vendors/RoleAccess';
const multer = require('multer');
const keycloak = require('../../../common/keycloak').getKeycloak();

const upload = multer();

export default express
  .Router()

  .post(
    '/',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.create
  )
  .post(
    '/addAction',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.addAction
  )
  .get('/', keycloak.protect(`realm:${RoleAccess.LECTURE}`), controller.all)
  .get('/:id', keycloak.protect(`realm:${RoleAccess.LECTURE}`), controller.byId)
  .get(
    '/byDossierId/:id',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.byDossierId
  )
  .get(
    '/byDossierIdByActeur/:dossier_id/:acteur_id',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.byDossierIdByActeur
  )
  .get(
    '/byDossierIdByActeurAvecInformation/:dossier_id/:acteur_id',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.byDossierIdByActeurAvecInformation
  )
  .post(
    '/upload/:id',
    [
      upload.single('filedata'),
      keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    ],
    controller.uploadDocument
  )
  .post(
    '/uploadaction/:id',
    [
      upload.single('filedata'),
      keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    ],
    controller.uploadDocumentAction
  )
  .put(
    '/cloture/:id',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.clotureEchange
  )

  .put(
    '/estLu',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.echangeEstLu
  )

  .put(
    '/integreDocumentAction/:echange_id',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.integreDocumentAction
  )
  .put(
    '/estUrgent/:echange_id',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.setLastActionUrgent
  )
  .delete(
    '/deleteFichier/:id',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.deleteFichier
  )
  .delete(
    '/deleteEchange/:id',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.deleteEchange
  )
  .put(
    '/droits',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.setDroits
  );
