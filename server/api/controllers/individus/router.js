import * as express from 'express';
import controller from './controller';
import RoleAccess from '../../vendors/RoleAccess';
const keycloak = require('../../../common/keycloak').getKeycloak();

export default express
  .Router()
  .post(
    '/',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.create
  )
  .get('/', keycloak.protect(`realm:${RoleAccess.LECTURE}`), controller.all)
  .get(
    '/:id',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.byId
  );
