import IndividusService from '../../services/individus.service';

export class Controller {
  all(_req, res) {
    IndividusService.all().then((r) => res.json(r));
  }

  byId(req, res) {
    IndividusService.byId(req.params.id).then((r) => {
      if (r) res.json(r);
      else res.status(404).end();
    });
  }

  create(req, res) {
    IndividusService.create(req.body).then((r) =>
      res.status(201).location(`/api/v1/individus/${r._id}`).json(r)
    );
  }
}
export default new Controller();
