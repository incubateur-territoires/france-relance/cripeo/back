const frequences = require('../../vendors/FrequenceType');

export class Controller {
  all(_req, res) {
    res.json(frequences);
  }
}

export default new Controller();
