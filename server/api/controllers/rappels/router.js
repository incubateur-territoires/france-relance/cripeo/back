import * as express from 'express';
import controllerActeur from '../acteurs/controller';
import controllerDossier from '../dossiers/controller';
export default express
  .Router()
  .get('/acteur/:key', controllerActeur.sendRappels)
  .get('/dossiers/emailsClos/:key', controllerDossier.sendMailSuppression)
  .get(
    '/dossiers/supprimerClos/:key',
    controllerDossier.suppressionDossiersClos
  );
