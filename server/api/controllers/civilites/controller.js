const { civilites } = require('../../../config/civilite');

export class Controller {
  all(_req, res) {
    res.json(civilites);
  }
}
export default new Controller();
