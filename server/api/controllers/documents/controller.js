import DocumentsService from '../../services/documents.service';
import GedService from '../../services/ged.service';
import { mkdir, writeFile, readdir, readFile, rm } from 'node:fs/promises';
import config from '../../../common/config';
import path from 'node:path';

import jszip from 'jszip';
import AccessResources from '../../vendors/AccessRessouces';
import UtilisateursService from '../../services/utilisateurs.service';
import EchangesService from '../../services/echanges.service';
import ActeursService from '../../services/acteurs.service';

import HistoriquesService from '../../services/historique.service';
import { textMessage } from '../../../common/messageHistorisation';

import * as fs from 'fs';

const { decryptStream } = require('../../vendors/crypto');

export class Controller {
  byEchangeId(req, res) {
    DocumentsService.byEchangeId(req.params.id).then((r) => {
      if (r) res.json(r);
      else res.status(404).end();
    });
  }

  byDossierId(req, res) {
    DocumentsService.byDossierId(req.params.id).then((r) => {
      if (r) res.json(r);
      else res.status(404).end();
    });
  }

  async byDossierIdByActeur(req, res) {
    const docs = await DocumentsService.byDossierIdByActeur(
      req.params.id,
      req.params.acteur
    );
    const echs = await EchangesService.byDossierIdByActeurAvecInformation(
      req.params.id,
      req.params.acteur
    );
    const allActs = await EchangesService.getListAllActionsByDossierId(
      req.params.id
    );

    let documents = JSON.parse(JSON.stringify(docs));
    let echanges = JSON.parse(JSON.stringify(echs));
    let allActions = JSON.parse(JSON.stringify(allActs));

    try {
      let fichiers = documents?.map((doc) => {
        let newDoc = { ...doc };
        newDoc.canDelete = true;
        for (const echan of echanges) {
          // Si l'échange qui a l'information est le même échange de ce document
          if (echan._id.toString() === doc.echange_id.toString()) {
            const infor = echan.information;
            const info = JSON.parse(JSON.stringify(infor));
            let infoAct = [];
            info.map((inf) => {
              infoAct.push(inf.acteur_id);
            });
            newDoc.informationActeurs_id = infoAct;
          }
        }
        return newDoc;
      });

      for (const doc of fichiers) {
        if (
          doc.informationActeurs_id !== undefined &&
          doc.informationActeurs_id.toString() === req.params.acteur.toString()
        ) {
          doc.canDelete = false;
        }

        for (const act of allActions) {
          // Dans les actions des autres échanges, S'il y a le même document, on ne peut pas supprimer
          if (
            act.echange_id.toString() !== doc.echange_id.toString() &&
            act.document.findIndex(
              (element) =>
                element.document_id &&
                element.document_id.toString() === doc._id.toString()
            ) !== -1
          ) {
            doc.canDelete = false;
          }
        }

        let echange = await EchangesService.byId(doc.echange_id);
        let echangeActs = JSON.parse(JSON.stringify(echange.action));

        // Dans les actions de la même échange
        let actAvecDocIndex = echangeActs.findIndex(
          (acti) =>
            acti.document?.findIndex(
              (element) =>
                element.document_id &&
                element.document_id.toString() === doc._id.toString()
            ) !== -1
        );

        // S'il y a de réponse pour cette action, on ne peut pas supprimer
        if (
          actAvecDocIndex !== undefined &&
          actAvecDocIndex !== echangeActs.length - 1
        ) {
          doc.canDelete = false;
        }
        doc.mainActeurId = echangeActs[actAvecDocIndex].emetteur_id;
      }

      res.json(fichiers);
    } catch (err) {
      res.status(404).end();
    }
  }

  byId(req, res) {
    DocumentsService.byId(req.params.id).then((r) => {
      if (r) res.json(r);
      else res.status(404).end();
    });
  }

  async getFile(req, res) {
    const { id } = req.params;
    const document = await DocumentsService.byId(id);

    if (document.tag === 'INTERNE') {
      try {
        fs.readFile(document.nom, async (err, data) => {
          if (err) res.status(500).send({ error: err });
          console.log('has been read');
          let filecrypted = await decryptStream(data);
          const buffed = Buffer.from(filecrypted, 'binary').toString('base64');
          return res.send(buffed);
        });
      } catch (e) {
        return res.status(500).send({ error: e });
      }
    } else {
      const ticket = await GedService.getTicket();
      try {
        let file = await GedService.recupeFichier(
          `${config.ged.url}/alfresco/s/api/node/content/workspace/SpacesStore/${document.noderef}/${document.nom}?alf_ticket=${ticket}`
        );
        res.send(file);
      } catch (err) {
        res.status(500).send({ error: err });
      }
    }
  }

  async getAllFile(req, res) {
    if (!req.kauth) return res.status(401).send('Authentification manquante');
    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();
    const user = await UtilisateursService.byIdKc(kc_id);
    const routeDir = path.resolve('./');
    const { dossier } = req.params;
    const documents = await DocumentsService.byDossierIdByActeur(
      dossier,
      user.organisation.acteur_id
    );
    const ticket = await GedService.getTicket();
    const timestamp = Date.now();
    const routeSauvegarde = routeDir + '/tmp/' + dossier + timestamp;
    try {
      await mkdir(routeSauvegarde, { recursive: true });
    } catch (err) {
      console.error(err);
      return res.status(500).send('erreur creation dossier temp');
    }

    for (let document of documents) {
      if (document.tag === 'INTERNE') {
        try {
          fs.readFile(document.nom, async (err, data) => {
            if (err) res.status(500).send({ error: err });
            console.log('has been read all file');
            let filecrypted = await decryptStream(data);
            await writeFile(
              `${routeSauvegarde}/${document.titre}`,
              filecrypted
            );
          });
        } catch (e) {
          return res.status(500).send({ error: e });
        }
      } else {
        try {
          let file = await GedService.recupeFichier(
            `${config.ged.url}/alfresco/s/api/node/content/workspace/SpacesStore/${document.noderef}/${document.nom}?alf_ticket=${ticket}`,
            true
          );

          await writeFile(`${routeSauvegarde}/${document.titre}`, file);
        } catch (err) {
          res.status(500).send({ error: err });
        }
      }
    }

    let zip = new jszip();
    const files = await readdir(routeSauvegarde);
    for (const file of files) {
      zip.file(file, await readFile(routeSauvegarde + '/' + file));
      rm(routeSauvegarde + '/' + file);
    }
    rm(routeSauvegarde, { recursive: true });
    zip.generateAsync({ type: 'nodebuffer' }).then(async function (content) {
      const buffed = Buffer.from(content, 'binary').toString('base64');
      return res.send(buffed);
    });
  }

  async droitFile(req, res) {
    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();

    const user = await UtilisateursService.byIdKc(kc_id);
    const acteur = await ActeursService.byId(user.organisation.acteur_id);

    let hasPerm = true;

    if (req.body.dossierId && req.body.mainActeur) {
      // Récupérer toutes les actions de dossier
      let listeActions = await EchangesService.getListAllActionsByDossierId(
        req.body.dossierId
      );

      // Retirer les actions qui n'ont pas ce document
      listeActions = listeActions.map((action) => {
        let actionDoc = JSON.parse(JSON.stringify(action.document));
        actionDoc.map((doc) => {
          if (
            doc.document_id !== undefined &&
            doc.document_id.toString() === req.body.document_id.toString()
          ) {
            let newAction = { ...action };
            return newAction;
          }
        });
      });

      listeActions.map((act) => {
        if (
          act !== undefined &&
          act.echange_id.toString() !== req.body.echangeId.toString()
        ) {
          let deleteActeurs = req.body.deleteActeur;
          if (
            deleteActeurs?.length > 0 &&
            (deleteActeurs?.findIndex(
              (acteur) =>
                acteur.acteur_id.toString() === act.destinataire_id.toString()
            ) !== -1 ||
              deleteActeurs?.findIndex(
                (acteur) =>
                  acteur.acteur_id.toString() === act.emetteur_id.toString()
              ) !== -1)
          ) {
            hasPerm = false;
          }
        }
      });
    }

    if (hasPerm || req.body.deleteActeur.length === 0) {
      const doc = await DocumentsService.modifyDroits({
        documentId: req.body.document_id,
        acteurs: req.body.acteurs,
      });

      const text = textMessage('DOCUMENTS', 'droits');
      const messageData = {
        dossier_id: doc.dossier_id,
        acteur_id: user.organisation.acteur_id,
        acteur: acteur.entite,
        createdBy: user.libelle,
        message: text,
        tag: 'DOCUMENTS',
      };
      await HistoriquesService.create(messageData);

      let documents = await DocumentsService.byDossierId(doc.dossier_id);
      res.json(documents);
    }
  }
}

export default new Controller();
