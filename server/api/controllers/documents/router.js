import * as express from 'express';
import controller from './controller';
import RoleAccess from '../../vendors/RoleAccess';
const keycloak = require('../../../common/keycloak').getKeycloak();

export default express
  .Router()
  .get(
    '/getAllFile/:dossier',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.getAllFile
  )
  .get(
    '/byEchangeId/:id',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.byEchangeId
  )
  .get(
    '/byDossierId/:id',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.byDossierId
  )
  .get(
    '/byDossierIdByActeur/:id/:acteur',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.byDossierIdByActeur
  )
  .get(
    '/byId/:id',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.byDossierId
  )
  .get(
    '/getFile/:id',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.getFile
  )
  .post(
    '/droitFile',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.droitFile
  );
