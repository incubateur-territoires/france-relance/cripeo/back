import kc from '../../services/keycloak.service';
import { validationResult } from 'express-validator';

import RoleAccess from '../../vendors/RoleAccess';
import GroupAccess from '../../vendors/GroupAccess';
export class Controller {
  async all(req, res) {
    let isAdminFonctionnel;
    try {
      isAdminFonctionnel =
        req.kauth.grant.access_token.content.realm_access.roles.findIndex(
          (role) => role === RoleAccess.CREATION_ORGA
        ) !== -1;
    } catch (err) {
      return res
        .status(503)
        .send("Impossible de vérifier les droits de l'utilisateur");
    }
    let r;
    try {
      if (isAdminFonctionnel) {
        r = await kc.getGroups();
      } else {
        r = await kc.getGroupsWithMask();
      }
      if (r && r.data && r.data.length > 0) return res.json(r.data);
      return res.json(r);
    } catch (err) {
      res.status(503).send("Problème d'accès à Keycloak");
    }
  }

  async userByEmail(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    const r = await kc.getUsersWithPredicate({ email: req.params.email });
    if (r && r.data && r.data.length > 0) return res.json(r.data);
    res.json(r);
  }

  async userById(req, res) {
    res.json(await kc.getUserInfo(req.params.id));
  }

  async listRoles(req, res) {
    res.json(RoleAccess);
  }

  async listMaskGroups(req, res) {
    res.json(GroupAccess);
  }
}
export default new Controller();
