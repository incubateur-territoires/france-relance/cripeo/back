import * as express from 'express';
import controller from './controller';
import { check } from 'express-validator';
import RoleAccess from '../../vendors/RoleAccess';
const keycloak = require('../../../common/keycloak').getKeycloak();

export default express
  .Router()
  .get(
    '/groupes',
    keycloak.protect(`realm:${RoleAccess.GESTION_ORGA}`),
    controller.all
  )
  .get(
    '/utilisateur/byEmail/:email',
    [
      check('email')
        .trim()
        .normalizeEmail({ gmail_remove_dots: false })
        .isEmail()
        .withMessage('Email invalide'),
      keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    ],
    controller.userByEmail
  )
  .get(
    '/utilisateur/byId/:id',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.userById
  )
  .get('/roles', keycloak.protect(), controller.listRoles)
  .get('/masks', keycloak.protect(), controller.listMaskGroups);
