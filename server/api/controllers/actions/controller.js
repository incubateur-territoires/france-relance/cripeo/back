import ActionsService from '../../services/actions.service';

export class Controller {
  all(_req, res) {
    ActionsService.all().then((r) => res.json(r));
  }

  byId(req, res) {
    ActionsService.byId(req.params.id).then((r) => {
      if (r) res.json(r);
      else res.status(404).end();
    });
  }

  create(req, res) {
    if (!req.body._id) {
      ActionsService.create(req.body).then((r) => res.json(r));
    } else {
      ActionsService.modify(req.body).then((r) => res.json(r));
    }
  }

  byCategorieId(req, res) {
    const { categorie_id } = req.params;
    if (categorie_id) {
      ActionsService.byCategorieId(categorie_id).then((r) => res.json(r));
    } else {
      res.status(404).end();
    }
  }
}
export default new Controller();
