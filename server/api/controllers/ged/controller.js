import GedService from '../../services/ged.service';

export class Controller {
  getTicket(_req, res) {
    GedService.getTicket().then((r) => res.json(r));
  }
}

export default new Controller();
