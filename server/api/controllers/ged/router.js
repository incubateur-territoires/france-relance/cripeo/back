import * as express from 'express';
import controller from './controller';
import RoleAccess from '../../vendors/RoleAccess';
const keycloak = require('../../../common/keycloak').getKeycloak();

export default express
  .Router()
  .get(
    '/getTicket',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.getTicket
  );
