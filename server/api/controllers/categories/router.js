import * as express from 'express';
import controller from './controller';
import RoleAccess from '../../vendors/RoleAccess';
const keycloak = require('../../../common/keycloak').getKeycloak();
import { check } from 'express-validator';

export default express
  .Router()
  .post(
    '/',
    keycloak.protect(`realm:${RoleAccess.GESTION_ORGA}`),
    controller.create
  )
  .get('/', keycloak.protect(`realm:${RoleAccess.LECTURE}`), controller.all)
  .get('/:id', controller.byId)
  .get(
    '/byActeur/:id',
    [
      check('id').isMongoId().withMessage("Identifiant de l'acteur invalide"),
      keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    ],
    controller.byActeur
  )
  .get(
    '/byActeurEmeDes/:id',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.byActeurEmeDes
  )

  .delete(
    '/:id',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.removeCategorie
  )

  .post(
    '/action',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.addAction
  )
  .delete(
    '/action/:idAction',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.removeAction
  )
  .put(
    '/action',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.updateAction
  );
