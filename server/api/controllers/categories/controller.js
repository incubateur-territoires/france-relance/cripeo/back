import CategoriesService from '../../services/categories.service';

export class Controller {
  all(_req, res) {
    CategoriesService.all().then((r) => res.json(r));
  }

  byId(req, res) {
    CategoriesService.byId(req.params.id).then((r) => {
      if (r) res.json(r);
      else res.status(404).end();
    });
  }

  async create(req, res) {
    if (!req.body._id) {
      await CategoriesService.create(req.body);
    } else {
      await CategoriesService.modify(req.body);
    }
    const result = await CategoriesService.all();
    res.json(result);
  }

  async addAction(req, res) {
    const { libelle, entite, acteur_id, idCategorie } = req.body;
    let action = {
      libelle: libelle,
      entite: entite,
      acteur_id: acteur_id,
    };
    const result = await CategoriesService.addAction(idCategorie, action);
    return res.json(result);
  }

  removeAction(req, res) {
    CategoriesService.removeAction(req.params.idAction).then((r) =>
      res.json(r)
    );
  }

  async updateAction(req, res) {
    const { _id, libelle, entite, acteur_id } = req.body;
    if (!_id)
      return res
        .status(418)
        .json({ errors: 'ID manquant pour la modification' });
    let action = {
      _id,
      libelle,
      entite,
      acteur_id,
    };
    const result = CategoriesService.updateAction(action).then((r) =>
      res.json(r)
    );
    return res.json(result);
  }

  async byActeur(req, res) {
    const { id } = req.params;

    if (id) {
      const cat = await CategoriesService.byActeur(req.params.id);

      let sansEchanges = await CategoriesService.byActeurNonEchange(
        req.params.id
      );
      let categories = JSON.parse(JSON.stringify(cat));
      categories = categories?.map((categ) => {
        if (
          sansEchanges?.findIndex(
            (element) => element._id.toString() === categ._id.toString()
          ) !== -1
        ) {
          let newCateg = { ...categ };
          newCateg.noEchange = true;
          return newCateg;
        }
        return categ;
      });

      res.json(categories);
    }
  }

  byActeurEmeDes(req, res) {
    const { id } = req.params;
    if (id)
      CategoriesService.byActeurEmeDes(id).then((r) => {
        if (r) res.json(r);
        else res.status(404).end();
      });
    else res.status(404).end();
  }

  byActeurNonEchange(req, res) {
    const { id } = req.params;

    if (id)
      CategoriesService.byActeurNonEchange(id).then((r) => {
        if (r) res.json(r);
        else res.status(404).end();
      });
    else res.status(404).end();
  }

  removeCategorie(req, res) {
    const { id } = req.params;
    if (id)
      CategoriesService.removeCategorie(id).then((r) => {
        if (r) res.json(r);
        else res.status(404).end();
      });
    else res.status(404).end();
  }
}

export default new Controller();
