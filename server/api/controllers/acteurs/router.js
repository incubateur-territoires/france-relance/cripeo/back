import * as express from 'express';
import controller from './controller';
import { check } from 'express-validator';
import RoleAccess from '../../vendors/RoleAccess';
const keycloak = require('../../../common/keycloak').getKeycloak();

controller.checkServeurState();

export default express
  .Router()
  .get('/testRappel', [], controller.sendUrgentsRappels)
  .post(
    '/',
    keycloak.protect(`realm:${RoleAccess.CREATION_ORGA}`),
    controller.create
  )
  .post(
    '/equipe',
    keycloak.protect(`realm:${RoleAccess.GESTION_ORGA}`),
    controller.addEquipe
  )
  .get('/', keycloak.protect(`realm:${RoleAccess.LECTURE}`), controller.all)
  .get(
    '/test-secteur-geographique',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.testSecteurGeographique
  )
  .get('/:id', keycloak.protect(`realm:${RoleAccess.LECTURE}`), controller.byId)
  .get(
    '/byParentId/:id',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.byParentId
  )

  .put(
    '/email',
    keycloak.protect(`realm:${RoleAccess.GESTION_ORGA}`),
    controller.setEmail
  )
  .post(
    '/email',
    keycloak.protect(`realm:${RoleAccess.GESTION_ORGA}`),
    controller.addEmail
  )
  .post(
    '/secteur',
    [
      check('id').isMongoId().withMessage("Identifiant de l'acteur invalide"),
      check('secteur').isString().withMessage('Valeur de Secteur invalide'),
      keycloak.protect(`realm:${RoleAccess.GESTION_ORGA}`),
    ],
    controller.ajouterSecteur
  )
  .put(
    '/secteur',
    [
      check('id').isMongoId().withMessage('Identifiant du secteur invalide'),
      check('secteur').isString().withMessage('Valeur de Secteur invalide'),
      keycloak.protect(`realm:${RoleAccess.GESTION_ORGA}`),
    ],
    controller.editerSecteur
  )
  .delete(
    '/secteur/:id',
    [
      check('id').isMongoId().withMessage("Identifiant de l'acteur invalide"),
      keycloak.protect(`realm:${RoleAccess.GESTION_ORGA}`),
    ],
    controller.deleteSecteur
  )
  .post(
    '/secteur-geographique',
    [
      check('id').isMongoId().withMessage("Identifiant de l'acteur invalide"),
      check('url').isString().withMessage('Adresse url invalide'),
      keycloak.protect(`realm:${RoleAccess.GESTION_ORGA}`),
    ],
    controller.editSecteurGeographique
  )
  .put(
    '/secteur-geographique',
    [
      check('id').isMongoId().withMessage("Identifiant de l'acteur invalide"),
      check('actif').isBoolean().withMessage('Le booléen Actif est invalide'),
      check('url').isString().withMessage('Adresse url invalide'),
      keycloak.protect(`realm:${RoleAccess.GESTION_ORGA}`),
    ],
    controller.editSecteurGeographique
  )
  .delete(
    '/secteur-geographique',
    [
      check('id').isMongoId().withMessage("Identifiant de l'acteur invalide"),
      keycloak.protect(`realm:${RoleAccess.GESTION_ORGA}`),
    ],
    controller.deleteSecteurGeographique
  )
  .delete(
    '/email/:idEmail',
    [
      check('idEmail')
        .isMongoId()
        .withMessage("Identifiant mongo de l'email invalide"),
      keycloak.protect(`realm:${RoleAccess.GESTION_ORGA}`),
    ],
    controller.deleteEmail
  )
  .put(
    '/frequence',
    keycloak.protect(`realm:${RoleAccess.GESTION_ORGA}`),
    controller.changeFrequenceMail
  );
