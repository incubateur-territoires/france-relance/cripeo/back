import ActeursService from '../../services/acteurs.service';
import UtilisateursService from '../../services/utilisateurs.service';
import localisationService from '../../services/localisation.service';
import MailsService from '../../services/mail.service';
import { validationResult } from 'express-validator';
import config from '../../../common/config';
import moment from 'moment';
import AccessResources from '../../vendors/AccessRessouces';

import LoggerWinston from '../../../common/loggerWinston';
import TypeBoiteMail from '../../vendors/TypeBoiteMail';

const logger = LoggerWinston.logger;

export class Controller {
  all(_req, res) {
    ActeursService.all().then((r) => res.json(r));
  }

  async checkServeurState() {
    console.log('Initialisation check Acteurs et utilisateurs');
    const acteurs = await ActeursService.all();
    const utilisateurs = await UtilisateursService.all();
    if (
      acteurs &&
      acteurs.length > 0 &&
      utilisateurs &&
      utilisateurs.length > 0
    ) {
      logger.info('Acteurs et utilisateurs prêts...');
    } else {
      logger.info(
        "Préparation à l'installation des acteurs et utilisateurs..."
      );
      const acteurLibelle = process.env.INSTALLATION_ACTEUR_LIBELLE;
      const utilisateurObj = {
        login: process.env.INSTALLATION_UTILISATEUR_LOGIN,
        libelle: process.env.INSTALLATION_UTILISATEUR_LIBELLE,
        k_user: process.env.INSTALLATION_UTILISATEUR_K_USER,
        k_group: process.env.INSTALLATION_UTILISATEUR_K_GROUP,
      };
      if (!acteurLibelle) {
        return logger.error("Le libellé de l'acteur est manquant");
      }
      if (!utilisateurObj.login) {
        return logger.error("Le login de l'utilisateur est manquant");
      }
      if (!utilisateurObj.k_user) {
        return logger.error("Le k_user de l'utilisateur est manquant");
      }
      if (!utilisateurObj.k_group) {
        return logger.error("Le k_group de l'utilisateur est manquant");
      }
      if (!utilisateurObj.libelle) {
        return logger.error("Le libelle de l'utilisateur est manquant");
      }
      logger.info('Création du premier acteur...');
      const acteur = await ActeursService.create({
        entite: acteurLibelle,
        actif: true,
      });
      if (acteur) {
        logger.info('Création du premier acteur terminé...');
        logger.info('Création du premier utilisateur...');
        const utilisateur = await UtilisateursService.create({
          login: utilisateurObj.login,
          k_userId: utilisateurObj.k_user,
          k_groupId: utilisateurObj.k_group,
          actif: true,
          organisation: {
            acteur_id: acteur._id,
          },
          libelle: utilisateurObj.libelle,
          groupe: 'Administrateur Fonctionnel',
        });
        if (utilisateur) {
          logger.info('Création du premier utilisateur terminé...');
        }
      }
    }
  }

  byId(req, res) {
    try {
      ActeursService.byId(req.params.id).then((r) => {
        if (r) res.json(r);
        else res.status(404).end();
      });
    } catch (err) {
      console.log('Acteur inconnu byId', err);
      res.status(502).send('Acteur inconnu byId');
    }
  }

  async create(req, res) {
    const { utilisateurs, acteur_id } = req.body;
    let data = req.body;
    if (utilisateurs) delete data.utilisateurs;
    if (acteur_id.length == 0) delete data.acteur_id;

    if (acteur_id) return res.status(401).send('Accès non autorisé');

    let resp;
    if (!req.body._id) {
      resp = await ActeursService.create(req.body);
    } else {
      resp = await ActeursService.modify(req.body);
    }

    if (utilisateurs) {
      utilisateurs.map(async (user) => {
        if (user.organisation && user.organisation.acteur_id === acteur_id)
          await UtilisateursService.updateOrganisation({
            utilisateur: user._id,
            acteur_id: resp._id,
          });
      });
    }

    res.json(resp);
  }

  async addEquipe(req, res) {
    const { utilisateurs, acteur_id } = req.body;
    let data = req.body;
    if (utilisateurs) delete data.utilisateurs;
    if (acteur_id.length == 0) delete data.acteur_id;

    if (!acteur_id) return res.status(401).send('Accès non autorisé');

    let resp;
    if (!req.body._id) {
      resp = await ActeursService.create(req.body);
    } else {
      resp = await ActeursService.modify(req.body);
    }

    const userListByActeur = await UtilisateursService.allByEquipe(resp._id);

    if (utilisateurs) {
      utilisateurs.map(async (user) => {
        if (resp.acteur_id) {
          let hasEquipe = false;
          user.equipes.map((equipe) => {
            if (equipe.acteur_id.toString() == resp._id.toString()) {
              hasEquipe = true;
            }
          });

          if (!hasEquipe) user.equipes.push({ acteur_id: resp._id });
          await UtilisateursService.updateEquipe({
            utilisateur: user._id,
            equipes: user.equipes,
          });
        }
      });
    }

    let userToSuppress = [];
    for (let i = 0; i < userListByActeur.length; i++) {
      let stillIn = false;

      if (utilisateurs) {
        utilisateurs.map((utilisateur) => {
          if (utilisateur._id.toString() == userListByActeur[i]._id.toString())
            stillIn = true;
        });
      }

      if (!stillIn) userToSuppress.push(userListByActeur[i]);
    }

    if (resp.acteur_id) {
      userToSuppress.map(async (user) => {
        let listEquipe = [];

        user.equipes.map(async (equipe) => {
          if (equipe.acteur_id.toString() != resp._id.toString())
            listEquipe.push(equipe);
        });

        await UtilisateursService.updateEquipe({
          utilisateur: user._id,
          equipes: listEquipe,
        });
      });
    }

    res.json(resp);
  }

  async addEmail(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    const { adresse, idActeur, typeBoiteEnvoi } = req.body;

    let email = {
      adresse,
      typeBoiteEnvoi,
    };

    const result = await ActeursService.addEmail(idActeur, email);
    return res.json(result);
  }

  async setEmail(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    const { _id, adresse, typeBoiteEnvoi } = req.body;

    if (!_id)
      return res
        .status(418)
        .json({ errors: 'ID manquant pour la modification' });

    let email = {
      _id,
      adresse,
      typeBoiteEnvoi,
    };

    const result = await ActeursService.modifyEmail(email);
    return res.json(result);
  }

  async changeFrequenceMail(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    const { id, frequence } = req.body;

    if (!id)
      return res
        .status(400)
        .json({ errors: 'ID manquant pour la modification' });

    const result = await ActeursService.changeFrequenceMail(id, frequence);
    return res.json(result);
  }

  deleteEmail(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    ActeursService.deleteEmail(req.params.idEmail).then((r) => res.json(r));
  }

  async sendUrgentsRappels(req, res) {
    // Liste des acteurs
    const acteurs = await ActeursService.all();

    const checkSend = await Promise.all(
      acteurs.map(async (acteur) => {
        const email = await MailsService.getMail(acteur._id, [
          TypeBoiteMail.URGENTE,
        ]);
        if (email) {
          const actions = await ActeursService.getUrgentLastActions(acteur._id);
          if (actions.length === 0) return true;
          let body = `Vous avez ${actions.length} action${
            actions.length > 1 ? 's' : ''
          } à traiter en urgence. \n\nRendez vous sur <a href="${
            config.app.uri
          }">Cripéo</a>.`;
          const mail = await MailsService.sendMail(
            email,
            'CRIPEO : rappel action urgente',
            body,
            'high'
          );
          return mail.status;
        } else {
          return true;
        }
      })
    );

    const erreurs = checkSend.reduce(
      (acc, check) => (check ? acc : acc + 1),
      0
    );

    res.json({ status: erreurs === 0, erreurs });
  }

  async sendRappels(req, res) {
    // Liste des acteurs
    console.log('rappels du cron tab quotidiens');
    const { key } = req.params;
    const { cron_key } = config.cron;

    if (key !== cron_key)
      return res.status(500).send('Accès au rappel impossible, clé incorrecte');

    const acteurs = await ActeursService.all();

    const checkSend = await Promise.all(
      acteurs.map(async (acteur) => {
        let checkWillSend = false;
        if (
          acteur.dateDernierRappel &&
          acteur.frequenceRappel !== null &&
          acteur.frequenceRappel > 0
        ) {
          let dateDernierRappel = new Date(acteur.dateDernierRappel);
          let dateMoment = moment(dateDernierRappel.toISOString()).add(
            acteur.frequenceRappel,
            'days'
          );
          checkWillSend = moment().isSameOrAfter(dateMoment, 'day');
        }

        if (checkWillSend) {
          const emails = await MailsService.getMail(acteur._id);
          const echanges = await ActeursService.getEchangebyActeur(acteur._id);

          let status = [];

          if (emails.length !== 0) {
            console.log(
              'nombre des emails pour acteur',
              acteur.entite,
              emails.length
            );
            for (const email of emails) {
              const typrBoite = email.typeBoiteEnvoi;

              let listEchangelastAction;
              let listEchange;

              if (
                typrBoite.findIndex((type) => type === 'urgente') !== -1 &&
                typrBoite.findIndex((type) => type === 'classique') !== -1
              ) {
                listEchangelastAction = echanges;
              } else {
                listEchangelastAction =
                  typrBoite.findIndex((type) => type === 'urgente') !== -1
                    ? echanges.filter(
                        (echange) =>
                          echange.action[echange.action.length - 1].urgent === 1
                      )
                    : typrBoite.findIndex((type) => type === 'classique') !== -1
                    ? echanges.filter(
                        (echange) =>
                          echange.action[echange.action.length - 1].urgent !== 1
                      )
                    : null;
              }

              if (
                typrBoite.findIndex((type) => type === 'creation_urgente') !==
                  -1 &&
                typrBoite.findIndex(
                  (type) => type === 'creation_non_urgente'
                ) !== -1
              ) {
                listEchange = listEchangelastAction;
              } else {
                listEchange =
                  typrBoite.findIndex((type) => type === 'creation_urgente') !==
                  -1
                    ? echanges.filter(
                        (echange) => echange.action[0].urgent === 1
                      )
                    : typrBoite.findIndex(
                        (type) => type === 'creation_non_urgente'
                      ) !== -1
                    ? echanges.filter(
                        (echange) => echange.action[0].urgent !== 1
                      )
                    : null;
              }

              const body = `Bonjour,<br/>
              <br/>Vous avez <b>${listEchange.length} action${
                listEchange.length > 1 ? 's' : ''
              }</b> à traiter sur Cripéo.
              <br/>Cliquez sur ce <a href="${
                config.app.uri
              }">LIEN</a> pour accéder à l'application.<br/>             
              <br/>Ceci est un email automatique, merci de ne pas répondre.`;

              const emailAdresse = email.adresse;

              if (listEchange.length !== 0) {
                const mail = await MailsService.sendMail(
                  emailAdresse,
                  `CRIPÉO : Rappel d'action${
                    listEchange.length > 1 ? 's' : ''
                  } non traitée${listEchange.length > 1 ? 's' : ''}`,
                  body
                );

                status.push(mail.status);
                if (mail.status) {
                  ActeursService.changeDateDernierRappel(
                    acteur._id,
                    moment().format()
                  );
                }
              }
            }

            return status.every((s) => s.status === true);
          }
        } else {
          return true;
        }
      })
    );

    const erreurs = checkSend.reduce(
      (acc, check) => (check ? acc : acc + 1),
      0
    );

    res.json({ status: erreurs === 0, erreurs });
  }

  async testRappelActions(req, res) {
    res.json(false);
  }

  async testRappelUrgentActions(req, res) {
    res.json(false);
  }

  byParentId(req, res) {
    try {
      ActeursService.byParentId(req.params.id).then((r) => {
        if (r) res.json(r);
        else res.status(404).end();
      });
    } catch (err) {
      console.log('Acteur inconnu byParentId', err);
      res.status(500).send('Acteur inconnu byParentId');
    }
  }

  async ajouterSecteur(req, res) {
    const { id, secteur } = req.body;
    const acteur = await ActeursService.addSecteur(id, secteur);
    res.json(acteur);
  }

  async editerSecteur(req, res) {
    const { id, secteur } = req.body;
    const acteur = await ActeursService.editSecteur(id, secteur);
    res.json(acteur);
  }

  async deleteSecteur(req, res) {
    const { id } = req.params;
    const acteur = await ActeursService.deleteSecteur(id);
    res.json(acteur);
  }

  async testSecteurGeographique(req, res) {
    if (!req.kauth) return res.status(401).send('Authentification manquante');
    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();

    const user = await UtilisateursService.byIdKc(kc_id);
    if (user) {
      const acteur = await ActeursService.byId(user.organisation.acteur_id);

      if (
        acteur &&
        acteur.secteurGeo &&
        acteur.secteurGeo.actif &&
        acteur.secteurGeo.url
      ) {
        let response = await localisationService.testSecteur(
          acteur.secteurGeo.url
        );
        return res.status(response).send('reponse OK');
      }
    }
    return res.status(400).send();
  }

  async editSecteurGeographique(req, res) {
    const { id, actif, url } = req.body;
    const acteur = await ActeursService.editSecteurGeo(id, actif, url);
    res.json(acteur);
  }

  async deleteSecteurGeographique(req, res) {
    const { id } = req.body;
    const acteur = await ActeursService.deleteSecteurGeo(id);
    res.json(acteur);
  }
}

export default new Controller();
