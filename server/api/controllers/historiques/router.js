import * as express from 'express';
import controller from './controller';
import RoleAccess from '../../vendors/RoleAccess';
const keycloak = require('../../../common/keycloak').getKeycloak();

export default express
  .Router()

  .get('/', keycloak.protect(`realm:${RoleAccess.LECTURE}`), controller.all)
  .get('/:id', controller.byId)
  .get(
    '/dossier/:id',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.byDossierId
  );
