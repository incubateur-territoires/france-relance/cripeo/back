import HistoriquesService from '../../services/historique.service';
import categoriesService from '../../services/categories.service';

export class Controller {
  all(_req, res) {
    HistoriquesService.all().then((r) => res.json(r));
  }

  byId(req, res) {
    categoriesService.byId(req.params.id).then((r) => {
      if (r) res.json(r);
      else res.status(404).end();
    });
  }

  byDossierId(req, res) {
    HistoriquesService.byDossierId(req.params.id).then((r) => {
      if (r) res.json(r);
      else res.status(404).end();
    });
  }
}

export default new Controller();
