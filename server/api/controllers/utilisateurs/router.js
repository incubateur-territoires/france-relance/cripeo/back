import * as express from 'express';
import controller from './controller';
import { check } from 'express-validator';
import RoleAccess from '../../vendors/RoleAccess';
const keycloak = require('../../../common/keycloak').getKeycloak();

require('express-validator');

export default express
  .Router()

  .post(
    '/',
    [
      keycloak.protect(`realm:${RoleAccess.GESTION_ORGA}`),
      check('email')
        .trim()
        .normalizeEmail({ gmail_remove_dots: false })
        .isEmail()
        .withMessage('Email invalide'),
      check('prenom')
        .optional()
        .trim()
        .isLength({ min: 2, max: undefined })
        .withMessage('Le prénom est de 2 caractères minimum'),
      check('nom')
        .optional()
        .trim()
        .isLength({ min: 2, max: undefined })
        .withMessage('Le prénom est de 2 caractères minimum'),
      check('organisation.acteur_id')
        .isMongoId()
        .withMessage('Identifiant mongo du groupe invalide'),
      check('_id')
        .optional()
        .isMongoId()
        .withMessage('Identifiant mongo invalide'),
    ],
    controller.create
  )

  .delete(
    '/:id',
    keycloak.protect(`realm:${RoleAccess.GESTION_ORGA}`),
    controller.deleteUtilisateur
  )

  .get('/', keycloak.protect(`realm:${RoleAccess.LECTURE}`), controller.all)

  .get(
    '/filtres/:id',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.getFiltres
  )

  .get(
    '/:id',
    [
      check('id').isMongoId().withMessage('Identifiant mongo invalide'),
      keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    ],
    controller.byId
  )

  .get(
    '/byIdKc/:id_kc',
    [
      check('id_kc').isString().withMessage('Identifiant kc invalide'),
      keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    ],
    controller.byIdKc
  )

  .get(
    '/byLogin/:login',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.byLogin
  )

  .get(
    '/byActeur/:id',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.allByActeur
  )

  .get(
    '/byEquipe/:id',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.allByEquipe
  )

  .put(
    '/notification',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.modifyNotif
  )

  .put(
    '/filtres',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.setFiltresParams
  )

  .put(
    '/refresh',
    keycloak.protect(`realm:${RoleAccess.GESTION_ORGA}`),
    controller.refreshDataUtilisateur
  )

  .delete(
    '/notification/:idNotif',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.deleteNotif
  );
