import UtilisateursService from '../../services/utilisateurs.service';
import KeycloakService from '../../services/keycloak.service';
import { validationResult } from 'express-validator';
import MailService from '../../services/mail.service';

import GroupAccess from '../../vendors/GroupAccess';
import RoleAccess from '../../vendors/RoleAccess';
import AccessResources from '../../vendors/AccessRessouces';
import loggerWinston from '../../../common/loggerWinston';
const logger = loggerWinston.logger;

export class Controller {
  all(req, res) {
    UtilisateursService.all().then((r) => res.json(r));
  }

  byId(req, res) {
    UtilisateursService.byId(req.params.id).then((r) => {
      if (r) res.json(r);
      else res.status(404).end();
    });
  }

  async byIdKc(req, res) {
    const { id_kc } = req.params;
    try {
      const clientIP =
        req.headers['x-forwarded-for']?.split(',').shift() ||
        req.socket?.remoteAddress;
      const user = await UtilisateursService.byIdKc(id_kc);
      if (user && user.login) return res.json(user);
      logger.log({
        level: 'error',
        message:
          'Utilisateur inconnu application, adresse IP : ' +
          clientIP.toString(),
        ip: clientIP.toString(),
      });
      res.status(404).end();
    } catch (err) {
      console.log('Utilisateur inconnu application', err);
      res.status(502).send('Utilisateur inconnu application');
    }
  }

  byLogin(req, res) {
    UtilisateursService.byLogin(req.params.login).then((r) => {
      if (r) res.json(r);
      else if (r == null) res.status(204).end();
      else res.status(404).end();
    });
  }

  allByActeur(req, res) {
    UtilisateursService.allByActeur(req.params.id).then((r) => {
      if (r) res.json(r);
      else if (r == null) res.status(204).end();
      else res.status(404).end();
    });
  }

  async create(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();
    const user = await UtilisateursService.byIdKc(kc_id);

    const {
      k_user_id,
      email,
      prenom,
      nom,
      _id,
      k_groupId,
      organisation,
      libelle,
      groupe,
      equipes,
    } = req.body;
    let keycloakUser;

    const isAdminFonctionnel =
      req.kauth.grant.access_token.content.realm_access.roles.findIndex(
        (role) => role === RoleAccess.CREATION_ORGA
      ) !== -1;
    const newGroupMasked =
      GroupAccess.ADMIN_FONCTIONNEL.toString() === groupe.toString();

    if (
      !isAdminFonctionnel &&
      newGroupMasked &&
      organisation.acteur_id.toString() !==
        user.organisation.acteur_id.toString()
    )
      return res.status(401).json({ message: 'Accès non autorisé' });

    try {
      let keycloakUserTmp = await KeycloakService.getUserOrCreate(
        nom,
        prenom,
        email,
        k_user_id
      );
      keycloakUser = await KeycloakService.getUserInfo(keycloakUserTmp.id);
    } catch (e) {
      return res
        .status(405)
        .json({ message: 'Erreur de création de compte dans keycloak' });
    }
    try {
      await KeycloakService.cleanGroupesUser(keycloakUser.id);
      await KeycloakService.addToGroup(keycloakUser.id, k_groupId);
    } catch (e) {
      return res
        .status(405)
        .json({ message: 'Erreur de gestion de groupe dans keycloak' });
    }
    if (_id) {
      UtilisateursService.modify(req.body).then((r) => res.json(r));
    } else {
      if (keycloakUser && keycloakUser.username) {
        const user = await UtilisateursService.create({
          login: keycloakUser.username,
          k_userId: keycloakUser.id,
          organisation: organisation,
          k_groupId: k_groupId,
          libelle,
          actif: true,
          groupe,
          equipes,
        });

        MailService.sendMailCreateUser(
          email,
          'CRIPEO : Nouvel utilisateur cripéo',
          `<h1>Bienvenue sur CRIPEO</h1>
          <p>Vous pouvez accéder à l'application à l'adresse ${process.env.APP_URI}.<br/>
          Pour votre première connexion, cliquez sur <b>"Mot de passe oublié"</b> <br/>
          <img src="${process.env.APP_URI}/back/img/etape_1.JPG" alt="Mire de login Cripeo"/>
          <br/>
          Dans la fenêtre qui apparait, entrez votre adresse mail :
          <br/>
          <img src="${process.env.APP_URI}/back/img/etape_2.JPG" alt="entrez email"/>
          <br/>
          Ne tenez pas compte de la page "expiré" qui s'affiche. Vous allez recevoir un nouveau mail avec un lien. Cliquez sur celui-ci. Vous arriverez sur une page qui vous invitera à créer un mot de passe :
          <br/>
          <img src="${process.env.APP_URI}/back/img/etape_3.jpg" alt="Créer mot de passe"/>
          <br/>
          Vous êtes dorénavant connecté à Cripéo.
          <br/>
          Pour toute question, contacter la CRIP.
          </p>`
        );
        return res.json(user);
      } else {
        return res
          .status(500)
          .send(
            "La récupération des informations n'a pas permi de créer l'utilisateur"
          );
      }
    }
  }

  deleteNotif(req, res) {
    UtilisateursService.deleteNotif(req.params.idNotif).then((r) =>
      res.json(r)
    );
  }

  async modifyNotif(req, res) {
    const { _id } = req.body;
    if (!_id)
      return res
        .status(418)
        .json({ errors: 'ID manquant pour la modification' });

    const result = await UtilisateursService.modifyNotif(_id);
    return res.json(result);
  }

  allByEquipe(req, res) {
    UtilisateursService.allByEquipe(req.params.id).then((r) => {
      if (r) res.json(r);
      else res.status(404).end();
    });
  }

  async setFiltresParams(req, res) {
    const { id, filtres } = req.body;
    try {
      const filtresParse = Object.entries(filtres).map((filtre) => {
        return {
          cle: filtre[0],
          valeur: filtre[1],
        };
      });

      const result = await UtilisateursService.setFiltresParams(
        id,
        filtresParse
      );
      let prepareFiltres = {};
      result?.filtres?.forEach(
        (filtre) => (prepareFiltres[filtre.cle] = filtre.valeur)
      );
      return res.json(prepareFiltres);
    } catch (err) {
      return res.status(405).json({
        message:
          "Erreur de setFiltresParams de controller avec l'arguments de 'id' et 'filtres': ",
        arguments: req.body,
        error: err,
      });
    }
  }

  async getFiltres(req, res) {
    if (!req.kauth) return res.status(401).send('Authentification manquante');

    const id = req.params.id;

    const user = await UtilisateursService.byId(id);

    let prepareFiltres = {};
    user?.filtres?.forEach(
      (filtre) => (prepareFiltres[filtre.cle] = filtre.valeur)
    );

    if (user?.filtres) return res.json(prepareFiltres);
    return res.status(204).send('Requête traitée mais aucun filtre présent');
  }

  async refreshDataUtilisateur(req, res) {
    const { utilisateur } = req.body;
    if (!req.kauth) return res.status(401).send('Authentification manquante');

    if (utilisateur) {
      const objUser = {
        login: utilisateur?.username,
        libelle: `${utilisateur?.lastName} ${utilisateur?.firstName}`,
      };
      let user;
      try {
        user = await UtilisateursService.refreshDataUtilisateur(
          utilisateur.id,
          objUser
        );
      } catch (err) {
        return res
          .status(500)
          .send('Erreur impossible de modifier les données ', err);
      }
      if (user) {
        try {
          const users = await UtilisateursService.allByActeur(
            user?.organisation?.acteur_id
          );
          if (users) {
            return res.json(users);
          }
        } catch (err) {
          return res
            .status(500)
            .send(
              "Erreur impossible de récupérer les utilisateurs de l'organisation ",
              err
            );
        }
      }

      return res
        .status(500)
        .send("Le rafraichissement de l'utilisateur n'a pas fonctionné");
    }
    return res.status(401).send("Informations de l'utilisateur non présentes");
  }

  async deleteUtilisateur(req, res) {
    if (!req.kauth) return res.status(401).send('Authentification manquante');
    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();
    const user = await UtilisateursService.byIdKc(kc_id);

    const { id } = req.params;
    const utilisateur = await UtilisateursService.byId(id);

    let hasPermi = false;

    if (
      id &&
      utilisateur &&
      utilisateur?._id.toString() !== user._id.toString()
    ) {
      if (user.groupe === 'Administrateur Fonctionnel') {
        hasPermi = true;
      } else if (
        user.groupe === 'Administrateur Organisationnel' &&
        utilisateur.groupe !== 'Administrateur Fonctionnel' &&
        user.organisation.acteur_id.toString() ===
          utilisateur.organisation.acteur_id.toString()
      ) {
        hasPermi = true;
      }
    }

    if (!id) return res.status(404).send("id d'utilisateur manquant");
    if (!hasPermi)
      return res
        .status(404)
        .send("Vous n'a pas le droit de supprimer cet utilisateur");

    try {
      await KeycloakService.deleteUser(utilisateur.k_userId);
    } catch (err) {
      return res.status(405).json({
        message:
          "Erreur de deleteUtilisateur() de controller avec l'arguments de 'id' et 'kc_id' : ",
        arguments: req.params,
        kc_id,
        error: err,
      });
    }

    await UtilisateursService.deleteUtilisateur(utilisateur._id).then((r) => {
      if (r) res.json(r);
      else res.status(404).end();
    });
  }
}

export default new Controller();
