import * as express from 'express';
import controller from './controller';

export default express
  .Router()

  .get('/names', controller.videoNames)
  .get('/byName/:name', controller.byName);
