const path = '/public/videosTuto';
const fs = require('fs');

export class Controller {
  videoNames(req, res) {
    try {
      fs.readdir(path, (err, videos) => {
        if (err) res.status(500).send({ error: err });
        else res.json(videos);
      });
    } catch (e) {
      return res.status(500).send({ error: e });
    }
  }

  byName(req, res) {
    const name = req.params.name;
    const stat = fs.statSync(path + '/' + name);
    const fileSize = stat.size;
    const head = {
      'Content-Length': fileSize,
      'Content-Type': 'video/mp4',
    };
    res.writeHead(200, head);
    fs.createReadStream(path + '/' + name).pipe(res);
  }
}
export default new Controller();
