import role from '../../files/role';

export class Controller {
  all(_req, res) {
    res.json(role);
  }
}
export default new Controller();
