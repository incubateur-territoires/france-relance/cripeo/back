import config from '../../../common/config';
const turf = require('@turf/turf');
import axios from 'axios';

export class Controller {
  async getCirconscriptions(_req, res) {
    try {
      let circonscriptions = {};
      await axios
        .get(config.atlas.fluxCirco)
        .then((r) => {
          res.json(JSON.parse(r));
        })
        .catch((err) => {
          console.log(err);
          return err;
        });
      return circonscriptions.features;
    } catch (e) {
      console.log(e);
      return res.status(405).json({ message: 'Erreur' });
    }
  }

  async getCirconscription(coordonnees) {
    const x = coordonnees[0];
    const y = coordonnees[1];
    const circonscriptions = [];

    try {
      let flux = await axios.get(config.atlas.fluxCirco);

      const geojson = {
        type: 'FeatureCollection',
        features: [],
      };
      geojson.features = flux.data.map((feature) => {
        return {
          properties: {
            cas: feature.properties.nom,
            id: feature.properties.id,
          },
          geometry: feature.geometry,
        };
      });

      await turf.geomEach(
        geojson,
        async function (currentGeometry, featureIndex, featureProperties) {
          if (
            currentGeometry.type !== 'MultiPolygon' &&
            turf.booleanContains(currentGeometry, turf.point([x, y]))
          ) {
            await circonscriptions.push({
              nom: featureProperties.cas,
              num_circo: featureProperties.id,
              geometry: currentGeometry,
            });
          } else {
            for (const coords of currentGeometry.coordinates) {
              var polygon = {
                type: 'Polygon',
                coordinates: coords,
              };
              if (turf.booleanContains(polygon, turf.point([x, y]))) {
                await circonscriptions.push({
                  nom: featureProperties.cas,
                  num_circo: featureProperties.id,
                  geometry: currentGeometry,
                });
              }
            }
          }
        }
      );
      return circonscriptions[0] ? circonscriptions[0] : null;
    } catch (err) {
      console.log(err);
      return err;
    }
  }

  async getAdresse(voie, cp) {
    let adresse = [];
    if (voie.length >= 3) {
      const url = config.api_bano.url + '/search/?q="';
      const params = voie + '"&postcode=' + cp + '&limit=5';
      const opt = process.env.PROXY_HOST
        ? {
            proxy: {
              host: process.env.PROXY_HOST,
              port: process.env.PROXY_PORT,
            },
          }
        : {};
      await axios
        .get(url + params, opt)
        .then(async (body) => {
          await adresse.push({
            coordonnees: body.data.features[0].geometry.coordinates,
            city: body.data.features[0].properties.city,
          });
        })
        .catch((err) => {
          console.log('erreur getadresse:' + err);
          return err;
        });
      return adresse[0] ? adresse[0] : '';
    }
    return [];
  }
}

export default new Controller();
