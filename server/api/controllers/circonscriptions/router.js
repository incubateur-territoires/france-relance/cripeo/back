import * as express from 'express';
import controller from './controller';
import RoleAccess from '../../vendors/RoleAccess';
const keycloak = require('../../../common/keycloak').getKeycloak();

export default express
  .Router()
  .get(
    '/',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.getCirconscriptions
  )
  .get(
    '/adresse/:voie/:cp',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.getAdresse
  )
  .get(
    '/:coordonnees',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.getCirconscription
  );
