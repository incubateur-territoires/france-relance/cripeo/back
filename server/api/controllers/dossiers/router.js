import * as express from 'express';
import controller from './controller';
import RoleAccess from '../../vendors/RoleAccess';
import { check } from 'express-validator';
const keycloak = require('../../../common/keycloak').getKeycloak();

export default express
  .Router()
  .post(
    '/',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.create
  )
  .get('/', keycloak.protect(`realm:${RoleAccess.LECTURE}`), controller.all)
  .get(
    '/acteur',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.dossierByActeur
  )
  .post(
    '/individu',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.addIndividu
  )
  .post(
    '/delete/individu',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.removeIndividu
  )
  .post(
    '/update/individu',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.updateIndividu
  )
  .post(
    '/update/adresse',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.updateAdresse
  )
  .post(
    '/update/reference',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.updateReference
  )
  .put(
    '/secteurAdministratif',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.setSecteurDossier
  )
  .get(
    '/acteur/clos/:acteur_id',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.dossierClos
  )
  .get(
    '/acteur/encours/:acteur_id',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.dossierEncours
  )
  .get(
    '/acteur/entraitement/:acteur_id',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.dossierEntraitement
  )
  .get(
    '/acteur/encreation/:acteur_id',
    keycloak.protect(`realm:${RoleAccess.LECTURE}`),
    controller.enCreation
  )
  .get(
    '/:id',
    [check('id').isMongoId(), keycloak.protect(`realm:${RoleAccess.LECTURE}`)],
    controller.byId
  )

  .put(
    '/delete',
    keycloak.protect(`realm:${RoleAccess.ECRITURE}`),
    controller.deletOne
  );
