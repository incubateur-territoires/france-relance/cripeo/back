import DossiersService from '../../services/dossiers.service';
import EchangesService from '../../services/echanges.service';
import HistoriquesService from '../../services/historique.service';
import { textMessage } from '../../../common/messageHistorisation';

import GedService from '../../services/ged.service';
import UtilisateursService from '../../services/utilisateurs.service';
import ActeursService from '../../services/acteurs.service';
import LocalisationService from '../../services/localisation.service';

import MailsService from '../../services/mail.service';
import config from '../../../common/config';
import moment from 'moment';

import loggerWinston from '../../../common/loggerWinston';

import AccessResources from '../../vendors/AccessRessouces';

import { validationResult } from 'express-validator';
import DocumentsService from '../../services/documents.service';

import { rm } from 'node:fs/promises';

const logger = loggerWinston.logger;

export class Controller {
  all(req, res) {
    DossiersService.all().then((r) => res.json(r));
  }

  async byId(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    if (!req.kauth) return res.status(401).send('Authentification manquante');
    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();
    const user = await UtilisateursService.byIdKc(kc_id);
    const dossier = await DossiersService.byId(req.params.id);
    if (!dossier) return res.status(404).end();

    if (dossier && (await DossiersService.hasAccess(dossier, user))) {
      res.json(dossier);
    } else {
      res.status(404).end();
    }
  }

  async create(req, res) {
    if (!req.body.id) {
      let nom = req.body.individuPrincipal.nom;
      let timestamp = new Date().getTime();
      let retour;
      if (!req.body.acteur_id)
        return res
          .status(404)
          .send("Vous ne pouvez pas crée un dossier, id d'acteur manquant");

      try {
        retour = await GedService.ajoutDossierRacine(nom + ' - ' + timestamp);
      } catch (err) {
        res.status(500).send({ error: err });
      }
      let secteur = await addSecteurGeo(req.body.adresse);
      if (secteur && secteur !== undefined) {
        secteur.acteur_id = req.body.acteur_id;
        req.body.secteurGeo = [].concat(secteur);
      }
      if (retour && retour.data && retour.data.entry) {
        req.body.noderef = retour.data.entry.id;
        const dossier = await DossiersService.create(req.body);

        const accessResources = new AccessResources(req.kauth);
        const kc_id = accessResources.get_kcId();
        const user = await UtilisateursService.byIdKc(kc_id);
        const acteur = await ActeursService.byId(req.body.acteur_id);

        const text = textMessage('DOSSIER', 'creation');
        const data = {
          dossier_id: dossier._id,
          acteur_id: req.body.acteur_id,
          acteur: acteur.entite,
          createdBy: user.libelle,
          message: text,
          tag: 'DOSSIER',
        };
        await HistoriquesService.create(data);

        return res.json(dossier);
      } else {
        res.status(500).send({
          error: 'erreur de connexion vers le service de gestion des documents',
        });
      }
    } else {
      DossiersService.modify(req.body).then((r) => res.json(r));
    }
  }

  async addIndividu(req, res) {
    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();
    const user = await UtilisateursService.byIdKc(kc_id);
    const acteur = await ActeursService.byId(user.organisation.acteur_id);

    const text = textMessage('FICHES IDENTITÉ', 'creation');
    const data = {
      dossier_id: req.body.id,
      acteur_id: user.organisation.acteur_id,
      acteur: acteur.entite,
      createdBy: user.libelle,
      message: text,
      tag: 'FICHES IDENTITÉ',
    };
    await HistoriquesService.create(data);

    await DossiersService.addIndividu(req.body).then((r) => res.json(r));
  }

  async removeIndividu(req, res) {
    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();
    const user = await UtilisateursService.byIdKc(kc_id);
    const acteur = await ActeursService.byId(user.organisation.acteur_id);

    const text = textMessage('FICHES IDENTITÉ', 'suppression');
    const data = {
      dossier_id: req.body.id,
      acteur_id: user.organisation.acteur_id,
      acteur: acteur.entite,
      createdBy: user.libelle,
      message: text,
      tag: 'FICHES IDENTITÉ',
    };
    await HistoriquesService.create(data);
    await DossiersService.removeIndividu(req.body).then((r) => res.json(r));
  }

  async updateIndividu(req, res) {
    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();
    const user = await UtilisateursService.byIdKc(kc_id);
    const acteur = await ActeursService.byId(user.organisation.acteur_id);

    const object = {};
    object._id = req.body.id;
    object.civilite = req.body.civilite;
    object.nom = req.body.nom;
    object.prenom = req.body.prenom;
    object.dateNaissance = req.body.dateNaissance;
    object.communeNaissance = req.body.communeNaissance;
    object.paysNaissance = req.body.paysNaissance;
    object.commentaire = req.body.commentaire;
    object.lienParent = req.body.lienParent;
    object.refIndividu = req.body.refIndividu;

    const text = textMessage('FICHES IDENTITÉ', 'modify');
    const data = {
      dossier_id: req.body.id,
      acteur_id: user.organisation.acteur_id,
      acteur: acteur.entite,
      createdBy: user.libelle,
      message: text,
      tag: 'FICHES IDENTITÉ',
    };
    await HistoriquesService.create(data);

    await DossiersService.updateIndividu(req.body, object).then((r) =>
      res.json(r)
    );
  }

  async updateAdresse(req, res) {
    if (!req.kauth) return res.status(401).send('Authentification manquante');
    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();
    const user = await UtilisateursService.byIdKc(kc_id);

    let data = {
      id: req.body.id,
      adresse: {
        voie: req.body.adresse.voie,
        cp: req.body.adresse.cp,
        commune: req.body.adresse.commune,
        complement: req.body.adresse.complement || '',
        lieudit: req.body.adresse.lieudit || '',
      },
    };

    const acteurs = await ActeursService.all();
    await Promise.all(
      acteurs.map(async (acteur) => {
        let localisation;

        try {
          if (acteur.secteurGeo && acteur.secteurGeo.actif) {
            localisation = await LocalisationService.getAdresse(
              data.adresse.voie,
              data.adresse.cp
            );
          }
        } catch (err) {
          logger.error('Problème de récupération de coordonnées Bano');
        }

        if (
          localisation &&
          acteur.secteurGeo &&
          acteur.secteurGeo.actif &&
          acteur.secteurGeo.url
        ) {
          let secteur;
          try {
            secteur = await LocalisationService.getSecteur(
              acteur?.secteurGeo?.url,
              localisation.coordonnees
            );
          } catch (err) {
            logger.error("Impossible de trouver le secteur de l'adresse");
          }
          try {
            if (secteur)
              await DossiersService.addSecteurGeo(
                data.id,
                user.organisation.acteur_id,
                secteur
              );
          } catch (err) {
            logger.error("Impossible de d'enregistrer le secteur de l'adresse");
          }
        }
      })
    );

    try {
      const dossier = await DossiersService.updateAdresse(data);
      if (dossier) {
        const acteur = await ActeursService.byId(user.organisation.acteur_id);

        const text = textMessage('ADRESSE', 'modify');
        const value = {
          dossier_id: req.body.id,
          acteur_id: user.organisation.acteur_id,
          acteur: acteur.entite,
          createdBy: user.libelle,
          message: text,
          tag: 'ADRESSE',
        };
        await HistoriquesService.create(value);

        res.json(dossier);
      }
    } catch (err) {
      logger.error("Impossible de mettre à jour l'adresse");
    }
  }

  async updateReference(req, res) {
    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();
    const user = await UtilisateursService.byIdKc(kc_id);
    const acteur = await ActeursService.byId(user.organisation.acteur_id);

    const dossier = await DossiersService.checkReference(req.body);

    if (!dossier) {
      const text = textMessage('RÉFÉRENCE', 'creation');
      const data = {
        dossier_id: req.body.id,
        acteur_id: user.organisation.acteur_id,
        acteur: acteur.entite,
        createdBy: user.libelle,
        message: text,
        tag: 'RÉFÉRENCE',
      };
      await HistoriquesService.create(data);

      await DossiersService.createReference(req.body).then((r) => res.json(r));
    } else {
      const text = textMessage('RÉFÉRENCE', 'modify');
      const data = {
        dossier_id: req.body.id,
        acteur_id: user.organisation.acteur_id,
        acteur: acteur.entite,
        createdBy: user.libelle,
        message: text,
        tag: 'RÉFÉRENCE',
      };
      await HistoriquesService.create(data);

      await DossiersService.updateReference(req.body).then((r) => res.json(r));
    }
  }

  async dossierByActeur(req, res) {
    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();
    const user = await UtilisateursService.byIdKc(kc_id);

    try {
      // Récupérer tous les échanges de l'acteur
      let echangesDossiers = await EchangesService.byActeur(
        user.organisation.acteur_id
      );
      let allDossiers = await DossiersService.all();

      let aDossiers = await Promise.all(
        await allDossiers.map(async (dos) => {
          let dossier = JSON.parse(JSON.stringify(dos));
          let echanges = echangesDossiers.filter(
            (ech) => ech.dossier_id.toString() === dossier._id.toString()
          );
          dossier.acteurEstInfo = false;
          dossier.categories = [];

          echanges.map((echan) => {
            const infos = JSON.parse(JSON.stringify(echan.information));
            // si acteur est en information d'un echange
            if (infos.length !== 0) {
              infos.map((info) => {
                if (
                  info.acteur_id.toString() ===
                  user.organisation.acteur_id.toString()
                ) {
                  dossier.acteurEstInfo = true;
                }
              });
            }
            if (
              dossier.categories.findIndex(
                (cat) =>
                  cat.categorie_id.toString() ===
                  echan.categorie.categorie_id.toString()
              ) === -1
            ) {
              dossier.categories = dossier.categories.concat([echan.categorie]);
            }
          });

          dossier.etat = [];

          if (echanges && echanges.length !== 0) {
            dossier.nombreEchanges = echanges.length.toString();
            if (dossier && !dossier.clos) {
              const listsDossierEnCours =
                await DossiersService.listDossierEnCours(
                  [dossier],
                  echanges,
                  false,
                  user.organisation.acteur_id
                );

              if (listsDossierEnCours && listsDossierEnCours.length !== 0) {
                dossier.categories = listsDossierEnCours[0].categories;
                dossier.urgenceRoot = listsDossierEnCours[0].urgenceRoot;
                dossier.urgent = listsDossierEnCours[0].urgent;
                dossier.updated_at = listsDossierEnCours[0].updated_at;
                dossier.etat.push('enCours');
              }

              const listsDossierEntraitement =
                await DossiersService.listDossierEnCours(
                  [dossier],
                  echanges,
                  true,
                  user.organisation.acteur_id
                );
              if (
                listsDossierEntraitement &&
                listsDossierEntraitement.length !== 0
              ) {
                dossier.categories = listsDossierEntraitement[0].categories;
                dossier.urgenceRoot = listsDossierEntraitement[0].urgenceRoot;
                dossier.urgent = listsDossierEntraitement[0].urgent;
                dossier.updated_at = listsDossierEntraitement[0].updated_at;
                dossier.etat.push('aTraiter');
              }
            } else if (dossier && dossier.clos) {
              dossier.etat.push('clos');
            }
          } else {
            if (
              dossier &&
              dossier.acteur_id.toString() ===
                user.organisation.acteur_id.toString()
            ) {
              dossier.nombreEchanges = '0';

              if (dossier.clos) {
                dossier.etat.push('clos');
              } else {
                dossier.etat.push('creation');
              }
            }
          }
          return dossier;
        })
      );

      res.json(aDossiers);
    } catch (err) {
      console.log('Erreur récupération des dossiers ', err);
      res.status(502).json('Erreur récupération des dossiers');
    }
  }

  async dossierClos(req, res) {
    try {
      let dossiersTmpId = await EchangesService.byActeur(req.params.acteur_id);
      const dossiersClosVierges = await DossiersService.isClos(
        req.params.acteur_id
      );

      let dossiersId = [];
      for (let i = 0; i < dossiersTmpId.length; i++) {
        let found = false;
        for (let j = 0; j < dossiersId.length; j++) {
          if (String(dossiersTmpId[i].dossier_id) === String(dossiersId[j])) {
            found = true;
          }
        }
        if (!found) {
          dossiersId.push(dossiersTmpId[i].dossier_id);
        }
      }
      for (let k = 0; k < dossiersClosVierges.length; k++) {
        let found = false;
        for (let j = 0; j < dossiersId.length; j++) {
          if (String(dossiersClosVierges[k]._id) === String(dossiersId[j])) {
            found = true;
          }
        }
        if (!found) {
          dossiersId.push(dossiersClosVierges[k]._id);
        }
      }

      let dossiersResult = await DossiersService.byListId(dossiersId, true);

      let dossIdClos;
      dossiersTmpId.map((echan) => {
        if (echan.clos === 1) {
          const infos = JSON.parse(JSON.stringify(echan.information));
          infos.map((info) => {
            if (info.acteur_id.toString() === req.params.acteur_id.toString()) {
              dossIdClos = echan.dossier_id;
            }
          });
        }
      });

      const dossClos = await DossiersService.byListId(dossIdClos, false);

      if (dossClos.length !== 0) {
        dossiersResult = dossiersResult.concat(dossClos);
      }

      const doss = addCategories(dossiersResult, dossiersTmpId);

      for (const dos of doss) {
        const echanges = await EchangesService.byDossierIdByActeur(
          dos._id,
          req.params.acteur_id
        );

        // Recuperer le nombre des echanges qui sont dans chaque dossier
        dos.nombreEchanges = echanges.length.toString();

        // si acteur est en information d'un echange
        dos.acteurEstInfo = false;
        echanges.map((echan) => {
          if (echan.information.length !== 0) {
            const infos = JSON.parse(JSON.stringify(echan.information));
            infos.map((info) => {
              if (
                info.acteur_id.toString() === req.params.acteur_id.toString()
              ) {
                dos.acteurEstInfo = true;
              }
            });
          }
        });
      }
      res.json(doss);
    } catch (err) {
      console.log('Erreur récupération dossiers clos', err);
      res.status(502).json('Erreur récupération dossiers clos');
    }
  }

  async dossierEncours(req, res) {
    try {
      let dossiersTmpIdss = await EchangesService.byActeur(
        req.params.acteur_id
      );
      let dossiersTmpId = dossiersTmpIdss.filter((ech) => ech.clos !== 1);

      let dossiersId = [];
      for (let i = 0; i < dossiersTmpId.length; i++) {
        let found = false;
        for (let j = 0; j < dossiersId.length; j++) {
          if (String(dossiersTmpId[i].dossier_id) == String(dossiersId[j])) {
            found = true;
          }
        }
        if (!found) {
          dossiersId.push(dossiersTmpId[i].dossier_id);
        }
      }

      let dossiersResult = await DossiersService.byListId(dossiersId, false);

      const lists = await DossiersService.listDossierEnCours(
        dossiersResult,
        dossiersTmpId,
        false,
        req.params.acteur_id
      );

      const doss = addCategories(lists, dossiersTmpId);

      for (const dos of doss) {
        const echanges = await EchangesService.byDossierIdByActeur(
          dos._id,
          req.params.acteur_id
        );

        // Recuperer les nombre des echanges qui sont dans chaque dossier
        dos.nombreEchanges = echanges.length.toString();
        dos.acteurEstInfo = false;
        echanges.map((echan) => {
          const infos = JSON.parse(JSON.stringify(echan.information));
          // si acteur est en information d'un echange
          if (echan.information.length !== 0) {
            infos.map((info) => {
              if (
                info.acteur_id.toString() === req.params.acteur_id.toString()
              ) {
                dos.acteurEstInfo = true;
              }
            });
          }
        });
      }

      res.json(doss);
    } catch (err) {
      console.log('Erreur récupération dossiers en cours', err);
      res.status(502).json('Erreur récupération dossiers en cours');
    }
  }

  async dossierEntraitement(req, res) {
    try {
      let dossiersTmpIdss = await EchangesService.byActeurEnTraitement(
        req.params.acteur_id
      );
      let dossiersTmpId = dossiersTmpIdss.filter((ech) => ech.clos !== 1);
      let dossiersId = dossiersTmpId.map((ech) => ech.dossier_id);
      let dossiersResult = await DossiersService.byListId(dossiersId, false);

      const doss = await DossiersService.listDossierEnCours(
        dossiersResult,
        dossiersTmpId,
        true,
        req.params.acteur_id
      );

      for (const dos of doss) {
        const echanges = await EchangesService.byDossierIdByActeur(
          dos._id,
          req.params.acteur_id
        );

        // Recuperer les nombre des echanges qui sont dans chaque dossier
        dos.nombreEchanges = echanges.length.toString();

        dos.acteurEstInfo = false;
        echanges.map((echan) => {
          // si acteur est en information d'une echange
          if (echan.information.length !== 0) {
            const infos = JSON.parse(JSON.stringify(echan.information));
            infos.map((info) => {
              if (
                info.acteur_id.toString() === req.params.acteur_id.toString()
              ) {
                dos.acteurEstInfo = true;
              }
            });
          }
        });
      }
      res.json(doss);
    } catch (err) {
      console.log('Erreur récupération dossiers en traitement', err);
      res.status(502).json('Erreur récupération dossiers en traitement');
    }
  }

  async enCreation(req, res) {
    try {
      let dossiersTemp = await DossiersService.isOpen(req.params.acteur_id);
      let dossiers = [];
      for (const element of dossiersTemp) {
        let echange = await EchangesService.byDossierId(element._id);
        if (echange.length === 0) {
          dossiers.push(element);
        }
      }

      res.json(addCategories(dossiers, dossiersTemp));
    } catch (err) {
      console.log('Erreur récupération dossiers en création', err);
      res.status(502).json('Erreur récupération dossiers en création');
    }
  }

  async setSecteurDossier(req, res) {
    const { id, secteur, acteur_id } = req.body;
    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();
    const user = await UtilisateursService.byIdKc(kc_id);
    const acteur = await ActeursService.byId(user.organisation.acteur_id);

    const dossier = await DossiersService.setSecteur(id, acteur_id, secteur);
    if (dossier) {
      const text = textMessage('SECTEUR', 'modify');
      const data = {
        dossier_id: id,
        acteur_id: user.organisation.acteur_id,
        acteur: acteur.entite,
        createdBy: user.libelle,
        message: text,
        tag: 'SECTEUR',
      };
      await HistoriquesService.create(data);

      res.json(dossier);
    }
  }

  async deletOne(req, res) {
    if (!req.kauth) return res.status(401).send('Authentification manquante');
    const accessResources = new AccessResources(req.kauth);
    const kc_id = accessResources.get_kcId();
    const user = await UtilisateursService.byIdKc(kc_id);

    const dossier = await DossiersService.byId(req.body._id);
    const clientIP =
      req.headers['x-forwarded-for']?.split(',').shift() ||
      req.socket?.remoteAddress;
    logger.log({
      level: 'error',
      message: 'demande de suppression par ' + clientIP.toString(),
      ip: clientIP.toString(),
    });
    if (
      dossier.acteur_id.toString() != req.body.acteur_id.toString() ||
      dossier.acteur_id.toString() != user.organisation.acteur_id.toString()
    ) {
      return res.status(401).json({ message: 'Non autorisé' });
    }
    try {
      if (await DossiersService.deleteOne(req.body._id)) {
        const histories = await HistoriquesService.byDossierId(req.body._id);
        histories.map(async (history) => {
          await HistoriquesService.delete(history._id);
        });
        res.json({ message: 'dossier supprimé' });
      } else res.status(400).json({ message: 'Echec' });
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'erreur dans la supression' });
    }
  }

  async sendMailSuppression(req, res) {
    console.log(
      'rappels du cron tab pour envoie mail de suppression de dossier clos'
    );
    const { key } = req.params;
    const { cron_key } = config.cron;

    if (key !== cron_key)
      return res
        .status(500)
        .send(
          'Accès au sendMailSuppression dossierClos impossible, clé incorrecte'
        );

    const dossiers = await DossiersService.all();
    const acteurs = await ActeursService.all();
    let dossierId = [];

    await Promise.all(
      dossiers.map(async (dossier) => {
        let chec = false;
        let dateSuppression;
        if (dossier.clos) {
          if (
            dossier.dateSuppressionDossier !== undefined ||
            dossier.dateSuppressionDossier !== null
          ) {
            dateSuppression = new Date(dossier.dateSuppressionDossier);
            let dateSendMail = new Date(
              dateSuppression.setMonth(dateSuppression.getMonth() - 1)
            );
            chec =
              moment().isSame(dateSendMail, 'months') &&
              moment().isSame(dateSendMail, 'days');
          } else {
            let dateSuppressionDossier = new Date(2024, 2, 15);
            await DossiersService.cloture(
              dossier._id,
              true,
              undefined,
              dateSuppressionDossier
            );
            let dateSendMail = new Date(
              dateSuppression.setMonth(dateSuppression.getMonth() - 1)
            );
            chec =
              moment().isSame(dateSendMail, 'months') &&
              moment().isSame(dateSendMail, 'days');
          }
          if (chec) {
            dossierId = dossierId.concat(dossier._id);
          }
        }
      })
    );

    try {
      const checkSend = await Promise.all(
        acteurs.map(async (acteur) => {
          let checkWillSend = false;

          await Promise.all(
            dossierId.map(async (dossier) => {
              const echanges = await EchangesService.byDossierId(dossier);

              echanges.map((echange) => {
                const categorie = JSON.parse(JSON.stringify(echange.categorie));
                if (
                  categorie.emetteur_id.toString() === acteur._id.toString() ||
                  categorie.destinataire_id.toString() === acteur._id.toString()
                ) {
                  checkWillSend = true;
                }
              });
            })
          );

          if (checkWillSend) {
            const emails = await MailsService.getMail(acteur._id, [
              'classique',
            ]);

            if (emails) {
              const body = `Bonjour,<br/>
              <br/>Vous avez des dossiers clos sur Cripéo qui vont être supprimés dans 1 mois.
              <br/>Cliquez sur ce <a href="${config.app.uri}">LIEN</a> pour accéder à l'application.<br/>
              <br/>Ceci est un email automatique, merci de ne pas répondre.`;

              const mail = await MailsService.sendMail(
                emails,
                `CRIPÉO : Rappel des suppression les dossiers clos`,
                body
              );

              return mail.status;
            } else {
              return true;
            }
          } else {
            return true;
          }
        })
      );

      const erreurs = checkSend.reduce(
        (acc, check) => (check ? acc : acc + 1),
        0
      );

      res.json({ status: erreurs === 0, erreurs, message: 'mail envoyé' });
    } catch (err) {
      console.error(err);
      res.status(500).json({
        status: false,
        message: 'erreur dans envoie mail de suppression de dossier clos',
      });
    }
  }

  async suppressionDossiersClos(req, res) {
    console.log('rappels du cron tab pour suppression des dossiers clos');
    const { key } = req.params;
    const { cron_key } = config.cron;

    if (key !== cron_key)
      return res
        .status(500)
        .send('Accès au suppressionDossiersClos impossible, clé incorrecte');

    const dossiers = await DossiersService.all();

    dossiers.map(async (dossier) => {
      let checkWillSend = false;
      if (dossier.clos) {
        if (
          dossier.dateSuppressionDossier !== undefined ||
          dossier.dateSuppressionDossier !== null
        ) {
          let dateSuppression = new Date(dossier.dateSuppressionDossier);
          checkWillSend =
            moment().isSameOrAfter(dateSuppression, 'months') &&
            moment().isSameOrAfter(dateSuppression, 'days') &&
            moment().isSameOrAfter(dateSuppression, 'years');
        }
        if (checkWillSend) {
          try {
            if (await DossiersService.deleteDossierClos(dossier._id)) {
              const echanges = EchangesService.byDossierId(dossier._id);
              await echanges.map(async (ech) => {
                await EchangesService.delete(ech._id);
              });

              const documents = await DocumentsService.byDossierId(dossier._id);
              await documents.map(async (doc) => {
                if (doc.tag === 'INTERNE') {
                  try {
                    await rm(doc.nom);
                  } catch (e) {
                    return res.status(500).send({ error: e });
                  }
                } else {
                  try {
                    await GedService.effaceFichier(doc.noderef);
                  } catch (e) {
                    // eslint-disable-next-line no-undef
                    res.status(500).send({ error: e });
                  }
                }
                await DocumentsService.suppression(doc._id);
              });

              const histories = await HistoriquesService.byDossierId(
                dossier._id
              );
              await histories.map(async (history) => {
                await HistoriquesService.delete(history._id);
              });

              res.json({ status: true, message: 'dossier supprimé' });
            } else res.status(400).json({ status: false, message: 'Echec' });
          } catch (error) {
            console.error(error);
            res.status(500).json({
              status: false,
              message: 'erreur dans la supression dossier clos!',
            });
          }
        }
      }
    });
  }
}

const addSecteurGeo = async (adresse) => {
  const acteurs = await ActeursService.all();
  let secteur;
  await Promise.all(
    acteurs.map(async (acteur) => {
      let localisation;

      try {
        if (acteur.secteurGeo && acteur.secteurGeo.actif) {
          localisation = await LocalisationService.getAdresse(
            adresse.voie,
            adresse.cp
          );
        }
      } catch (err) {
        logger.error('Problème de récupération de coordonnées Bano');
      }

      if (
        localisation &&
        acteur.secteurGeo &&
        acteur.secteurGeo.actif &&
        acteur.secteurGeo.url
      ) {
        try {
          secteur = await LocalisationService.getSecteur(
            acteur?.secteurGeo?.url,
            localisation.coordonnees
          );
        } catch (err) {
          logger.error("Impossible de trouver le secteur de l'adresse");
        }
      }
    })
  );
  return secteur;
};

const addCategories = (dossiersResult, echanges) => {
  let dossiers = JSON.parse(JSON.stringify(dossiersResult));
  for (let l = 0; l < dossiers.length; l++) {
    dossiers[l].categories = [];
  }

  for (let i = 0; i < echanges.length; i++) {
    for (let j = 0; j < dossiers.length; j++) {
      if (String(echanges[i].dossier_id) === String(dossiers[j]._id)) {
        if (
          dossiers[j].categories.findIndex(
            (doss) =>
              String(doss.categorie_id) ===
              String(echanges[i].categorie.categorie_id)
          ) === -1
        ) {
          dossiers[j].categories = dossiers[j].categories.concat([
            echanges[i].categorie,
          ]);
        }
      }
    }
  }

  return dossiers;
};

export default new Controller();
