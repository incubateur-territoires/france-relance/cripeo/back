import mongoose from 'mongoose';

const Schema = mongoose.Schema;
/**
 * @typedef Acteur
 * @property {string} acteur_id
 */
/**
 * @typedef Action
 * @property {string} _id
 * @property {string} libelle
 * @property {Array<Acteur>} acteurs
 * @property {string} fonctionnalite
 * @property {string} categorieSuivante
 */
const ActionSchema = new Schema(
  {
    libelle: String,
    acteurs: [
      {
        acteur_id: Schema.Types.ObjectId,
      },
    ],
    fonctionnalite: String,
    categorieSuivante: String,
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

mongoose.model('Action', ActionSchema);
