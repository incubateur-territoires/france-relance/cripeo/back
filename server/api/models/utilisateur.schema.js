import mongoose from 'mongoose';
import GroupAccess from '../vendors/GroupAccess';
const Int32 = require('mongoose-int32').loadType(mongoose);

const Schema = mongoose.Schema;
/**
 * @typedef Notification
 * @property {string} dossier_id
 * @property {string} individu
 * @property {string} categorie
 * @property {boolean} lu
 * @property {Date} created
 */

/**
 * @typedef Utilisateur
 * @property {string} login
 * @property {string} k_userId
 * @property {string} k_groupId
 * @property {Array<Acteur>} organisation
 * @property {Array<Acteur>} equipes
 * @property {string} groupe
 * @property {boolean} actif
 * @property {Array<Notification>} notifs
 */
const UtilisateurSchema = new Schema(
  {
    login: String,
    k_userId: String,
    organisation: {
      acteur_id: Schema.Types.ObjectId,
      groupe: String,
    },
    equipes: [
      {
        acteur_id: Schema.Types.ObjectId,
        groupe: {
          type: String,
          default: GroupAccess.UTILISATEUR,
        },
      },
    ],
    libelle: String,
    actif: {
      type: Boolean,
      default: true,
    },
    groupe: {
      type: String,
    },
    k_groupId: String,
    notifs: [
      {
        dossier_id: Schema.Types.ObjectId,
        individu: String,
        categorie: String,
        lu: { type: Int32, required: false },
        created: Date,
        urgent: { type: Boolean, required: false, default: false },
      },
    ],
    filtres: [
      {
        cle: String,
        valeur: String,
      },
    ],
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

mongoose.model('Utilisateur', UtilisateurSchema);
