import mongoose from 'mongoose';
const Int32 = require('mongoose-int32').loadType(mongoose);

const Schema = mongoose.Schema;
/**
 * @typedef Information
 * @property {string} acteur_id
 */
/**
 * @typedef User
 * @property {string} nom
 * @property {string} prenom
 * @property {string} username
 */
/**
 * @typedef Etat
 * @property {string} id
 * @property {string} libelle
 * @property {Date} dateModification
 * @property {Array<User>} user
 */
/**
 * @typedef Document
 * @property {string} document_id
 */
/**
 * @typedef Action
 * @property {string} action_id
 * @property {string} libelle
 * @property {string} emetteur
 * @property {string} destinataire
 * @property {boolean} urgent
 * @property {Array<Document>} document
 */
/**
 * @typedef Categorie
 * @property {string} categorie_id
 * @property {string} libelle
 */
/**
 * @typedef Echange
 * @property {string} _id
 * @property {Array<Categorie>} categorie
 * @property {Array<Action>} action
 * @property {string} dossier_id
 * @property {string} parent_id
 * @property {string} noderef
 * @property {string} commentairePrive
 * @property {string} commentairePublic
 * @property {boolean} clos
 * @property {Array<Information>} information
 * @property {Array<Etat>} etat
 */

const EchangeSchema = new Schema(
  {
    dossier_id: Schema.Types.ObjectId,
    parent_id: Schema.Types.ObjectId,
    noderef: String,
    commentairePrive: String,
    commentairePublic: String,
    estLu: Date,
    clos: { type: Int32, required: false },
    information: [
      {
        acteur_id: Schema.Types.ObjectId,
        entite: String,
      },
    ],
    etat: [
      {
        etat_id: Schema.Types.ObjectId,
        libelle: String,
        dateModification: Date,
        user: {
          nom: String,
          prenom: String,
          username: String,
        },
      },
    ],
    action: [
      {
        action_id: String,
        libelle: String,
        emetteur: String,
        emetteur_id: Schema.Types.ObjectId,
        destinataire: String,
        destinataire_id: Schema.Types.ObjectId,
        dateRealisation: Date,
        commentairePrive: String,
        commentairePublic: String,
        createdBy: String,
        urgent: { type: Int32, required: false },
        document: [
          {
            document_id: String,
          },
        ],
      },
    ],
    categorie: {
      categorie_id: Schema.Types.ObjectId,
      libelle: String,
      emetteur: String,
      emetteur_id: Schema.Types.ObjectId,
      destinataire: String,
      destinataire_id: Schema.Types.ObjectId,
    },
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

mongoose.model('Echange', EchangeSchema);
