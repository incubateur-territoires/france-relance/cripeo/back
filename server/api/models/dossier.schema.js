import mongoose from 'mongoose';

const Schema = mongoose.Schema;
/**
 * @typedef Individu
 * @property {string} _id
 * @property {string} civilite
 * @property {string} nom
 * @property {string} prenom
 * @property {Date} dateNaissance
 * @property {string} communeNaissance
 * @property {string} paysNaissance
 * @property {string} individuSolis
 * @property {string} commentaire
 */
/**
 * @typedef Circo
 * @property {string} nom
 * @property {string} num_circo
 */
/**
 * @typedef Adresse
 * @property {string} voie
 * @property {string} cp
 * @property {string} commune
 * @property {string} complement
 * @property {string} lieudit
 * @property {Array<Circo>} circo
 */
/**
 * @typedef Dossier
 * @property {string} _id
 * @property {string} numero
 * @property {string} numeroSolis
 * @property {string} numeroCassiope
 * @property {string} numeroTPE
 * @property {string} numeroEN
 * @property {string} commentaire
 * @property {string} noderef
 * @property {Array<Adresse>} adresse
 * @property {Array<Individu>} individuPrincipal
 * @property {Array<Individu>} individuSecondaire
 * @property {string} acteur_id
 * @property {boolean} clos
 */
const DossierSchema = new Schema(
  {
    numero: String,
    individuPrincipalNom: String,
    numeroSolis: String, // TODO: remove this field
    numeroCassiope: String, // TODO: remove this field
    numeroTPE: String, // TODO: remove this field
    numeroEN: String, // TODO: remove this field
    numeroApplication: [
      {
        acteur_id: String,
        numero: String,
      },
    ],
    commentaire: String,
    noderef: String,
    adresse: {
      voie: String,
      cp: String,
      commune: String,
      complement: String,
      lieudit: String,
      circo: {
        nom: String,
        num_circo: String,
      },
    },
    secteurOrganisation: [
      {
        acteur_id: String,
        secteur_id: String,
        secteur: String,
      },
    ],
    secteurGeo: [
      {
        acteur_id: String,
        secteur_id: String,
        libelle: String,
        code: String,
      },
    ],
    individuPrincipal: {
      civilite: String,
      nom: String,
      prenom: String,
      dateNaissance: String,
      communeNaissance: String,
      paysNaissance: String,
      individuSolis: String, // TODO : Voir le métier : Non générique
      refIndividu: [
        {
          acteur_id: String,
          numero: String,
        },
      ],
      commentaire: String,
    },
    individuSecondaire: [
      {
        civilite: String,
        nom: String,
        prenom: String,
        dateNaissance: String,
        communeNaissance: String,
        paysNaissance: String,
        individuSolis: String, // TODO : Voir le métier : Non générique
        refIndividu: [
          {
            acteur_id: String,
            numero: String,
          },
        ],
        commentaire: String,
        lienParent: String,
      },
    ],
    acteur_id: Schema.Types.ObjectId,
    clos: {
      type: Boolean,
      default: false,
    },
    dateClos: Date,
    dateSuppressionDossier: Date,
    urgent: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

mongoose.model('Dossier', DossierSchema);
