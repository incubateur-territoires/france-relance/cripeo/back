import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
 * @typedef Historique
 * @property {string} _id
 * @property {string} dossier_id
 * @property {string} acteur_id
 * @property {string} acteur
 * @property {string} changedBy
 * @property {string} message
 * @property {string} tag
 */
const HistoriqueSchema = new Schema(
  {
    dossier_id: Schema.Types.ObjectId,
    acteur_id: Schema.Types.ObjectId,
    acteur: String,
    createdBy: String,
    message: String,
    tag: String,
  },
  { timestamps: { createdAt: 'created_at' } }
);

mongoose.model('Historique', HistoriqueSchema);