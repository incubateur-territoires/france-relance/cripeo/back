import mongoose from 'mongoose';
import TypeBoiteMail from '../vendors/TypeBoiteMail';

const validateEmail = function (email) {
  // eslint-disable-next-line no-useless-escape
  var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email);
};

const Schema = mongoose.Schema;
/**
 * @typedef Email
 * @property {boolean} actif
 * @property {string} adresse
 * @property {string} libelle
 */

/**
 * @typedef Acteur
 * @property {boolean} actif
 * @property {boolean} commentairePublic
 * @property {Array<Email>} emails
 * @property {string} entite
 * @property {string} service
 */
const ActeurSchema = new Schema(
  {
    actif: {
      type: Boolean,
      default: true,
    },
    commentairePublic: Boolean,
    frequenceRappel: Number,
    dateDernierRappel: Date,
    secteurs: [
      {
        valeur: String,
      },
    ],
    secteurGeo: {
      actif: {
        type: Boolean,
        default: false,
      },
      url: String,
      properties: [
        {
          code: String,
          libelle: String,
        },
      ],
    },
    emails: [
      {
        actif: {
          type: Boolean,
          default: true,
        },
        adresse: {
          type: String,
          trim: true,
          lowercase: true,
          required: 'Le champs email est obligatoire',
          validate: [validateEmail, "L'email est invalide"],
          match: [
            // eslint-disable-next-line no-useless-escape
            /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
            "L'email est invalide",
          ],
        },
        typeBoiteEnvoi: {
          type: [String],
          default: [
            TypeBoiteMail.URGENTE,
            TypeBoiteMail.CLASSIQUE,
            TypeBoiteMail.CREATION_URGENTE,
            TypeBoiteMail.CREATION_NON_URGENTE,
          ],
        },
        libelle: String,
      },
    ],
    entite: String,
    service: String,
    acteur_id: Schema.Types.ObjectId,
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

mongoose.model('Acteur', ActeurSchema);
