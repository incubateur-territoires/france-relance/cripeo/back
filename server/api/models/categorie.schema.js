import mongoose from 'mongoose';

const Schema = mongoose.Schema;
/**
 * @typedef Action
 * @property {string} _id
 * @property {string} libelle
 * @property {string} acteur_id
 * @property {string} entite
 * @property {string} fonctionnalite
 * @property {string} categorieSuivante
 */
/**
 * @typedef Acteur
 * @property {string} id
 * @property {string} entite
 */
/**
 * @typedef Information
 * @property {string} acteur_id
 */
/**
 * @typedef Categorie
 * @property {string} _id
 * @property {string} libelle
 * @property {Array<Acteur>} emetteur
 * @property {Array<Acteur>} destinataire
 * @property {Array<Action>} actions
 * @property {Array<Information>} information
 * @property {boolean} urgent
 * @property {boolean} actif
 */
const CategorieSchema = new Schema(
  {
    libelle: String,
    emetteur: {
      id: Schema.Types.ObjectId,
      entite: String,
    },
    destinataire: {
      id: Schema.Types.ObjectId,
      entite: String,
    },
    actions: [
      {
        libelle: String,
        acteur_id: String,
        entite: String,
        fonctionnalite: String,
        categorieSuivante: String,
      },
    ],
    information: [
      {
        acteur_id: Schema.Types.ObjectId,
        entite: String,
      },
    ],
    urgent: {
      type: Boolean,
      default: true,
    },
    actif: {
      type: Boolean,
      default: true,
    },
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

mongoose.model('Categorie', CategorieSchema);
