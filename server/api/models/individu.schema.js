import mongoose from 'mongoose';

const Schema = mongoose.Schema;
/**
 * @typedef Individu
 * @property {string} _id
 * @property {string} civilite
 * @property {string} nom
 * @property {string} prenom
 * @property {Date} dateNaissance
 * @property {string} communeNaissance
 * @property {string} paysNaissance
 * @property {string} numeroSolis
 * @property {string} commentaire
 * @property {string} lienParent
 */

const IndividuSchema = new Schema(
  {
    civilite: String,
    nom: String,
    prenom: String,
    dateNaissance: Date,
    communeNaissance: String,
    paysNaissance: String,
    numeroSolis: String,
    commentaire: String,
    lienParent: String,
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

mongoose.model('Individu', IndividuSchema);
