import mongoose from 'mongoose';

const Schema = mongoose.Schema;
/**
 * @typedef Etat
 * @property {string} _id
 * @property {string} libelle
 */
const EtatSchema = new Schema(
  {
    libelle: String,
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

mongoose.model('Etat', EtatSchema);
