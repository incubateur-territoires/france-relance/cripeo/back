import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
 * @typedef Acteur
 * @property {string} acteur_id
 */
/**
 * @typedef Document
 * @property {string} _id
 * @property {string} echange_id
 * @property {string} dossier_id
 * @property {string} action_id
 * @property {string} titre
 * @property {string} commentaire
 * @property {string} nom
 * @property {string} noderef
 * @property {string} extension
 * @property {string} tag
 * @property {Array<Acteur>} acteurs
 */

const DocumentSchema = new Schema(
  {
    action_id: Schema.Types.ObjectId,
    echange_id: Schema.Types.ObjectId,
    dossier_id: Schema.Types.ObjectId,
    titre: String,
    commentaire: String,
    nom: String,
    noderef: String,
    extension: String,
    tag: String,
    acteurs: [
      {
        acteur_id: Schema.Types.ObjectId,
      },
    ],
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

mongoose.model('Document', DocumentSchema);
