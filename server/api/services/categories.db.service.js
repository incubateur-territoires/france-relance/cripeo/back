const mongoose = require('mongoose');
const Categorie = mongoose.model('Categorie');

class CategoriesDatabase {
  constructor() {}

  async all() {
    return await Categorie.find();
  }

  async byId(id) {
    return await Categorie.findById(id);
  }

  async insert(data) {
    const categories = new Categorie({ ...data });
    return await categories.save();
  }

  async modify(data) {
    return await Categorie.findOneAndUpdate(
      { _id: data._id },
      {
        $set: {
          libelle: data.libelle,
          emetteur: data.emetteur,
          destinataire: data.destinataire,
          actif: data.actif,
          urgent: data.urgent,
          information: data.information,
        },
      }
    );
  }

  async addAction(id, data) {
    return await Categorie.findOneAndUpdate(
      { _id: id },
      {
        $push: {
          actions: data,
        },
      },
      {
        new: true,
      }
    );
  }

  async removeAction(id) {
    return Categorie.findOneAndUpdate(
      { 'actions._id': id },
      {
        $pull: {
          actions: { _id: id },
        },
      },
      { new: true }
    );
  }

  async getActionsById(id) {
    const categorie = await Categorie.findOne(
      {
        _id: id,
      },
      { actions: 1 }
    );
    return categorie.actions;
  }

  async updateAction(action) {
    return await Categorie.findOneAndUpdate(
      { 'actions._id': action._id },
      {
        $set: {
          'actions.$': { ...action },
        },
      },
      {
        new: true,
      }
    );
  }

  async byActeur(id) {
    if (id) return await Categorie.find({ 'emetteur.id': id });
    return false;
  }

  async byActeurEmeDes(id) {
    return Categorie.find({
      $or: [{ 'emetteur.id': id }, { 'destinataire.id': id }],
    });
  }

  async byActeurNonEchange(id) {
    if (id)
      return await Categorie.aggregate([
        {
          $match: {
            'emetteur.id': new mongoose.Types.ObjectId(id),
          },
        },
        {
          $lookup: {
            from: 'echanges',
            as: 'echanges',
            localField: '_id',
            foreignField: 'categorie.categorie_id',
          },
        },
        {
          $match: {
            'echanges.clos': { $nin: [0] },
          },
        },
        {
          $project: {
            libelle: 1,
            actif: 1,
            urgent: 1,
            emetteur: 1,
            destinataire: 1,
          },
        },
      ]);
    return false;
  }

  async removeCategorie(id) {
    return await Categorie.findByIdAndDelete(id);
  }
}

export default new CategoriesDatabase();
