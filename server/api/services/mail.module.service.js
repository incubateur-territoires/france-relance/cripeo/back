const nodemailer = require('nodemailer');
import config from '../../common/config';

class MailModuleService {
  constructor() {}

  getTransport() {
    try {
      let configMailer = {
        host: config.mailer.host,
        port: config.mailer.port,
      };

      if (config.mailer.authentication !== 'false') {
        configMailer.auth = {
          type: config.mailer.auth.type,
          user: config.mailer.auth.user,
          pass: config.mailer.auth.password,
        };
      }

      if (config.mailer.debug) {
        configMailer.logger = true;
        configMailer.debug = true;
      }

      if (config.mailer.ignoreCertificat)
        configMailer.tls = { rejectUnauthorized: false };

      return nodemailer.createTransport(configMailer);
    } catch (e) {
      console.log(e);
    }
  }

  /**
   * Envoie d'un mail
   * @param to Destinataires séparé par des virgules
   * @param subject Sujet
   * @param body Corps du mail
   * @param priority Optionnel, si à vrai le message est en priorité haute
   */
  async sendMail(to, subject, body, priority) {
    const m = this.getTransport();
    try {
      const info = await m.sendMail({
        from: config.mailer.emailFrom,
        to: to,
        subject: subject,
        priority: priority ? 'high' : 'normal',
        html: body,
      });
      return {
        status: true,
        message: info,
      };
    } catch (err) {
      console.error(err);
      return { status: false, message: err };
    }
  }
}

export default new MailModuleService();
