import l from '../../common/logger';
import db from './documents.db.service';

class DocumentsService {
  all() {
    l.info(`${this.constructor.name}.all()`);
    return db.all();
  }

  byId(id) {
    return db.byId(id);
  }

  create(data) {
    return db.insert(data);
  }

  byActeur(data) {
    return db.byActeur(data);
  }

  byEchangeId(data) {
    return db.byEchangeId(data);
  }

  byDossierId(data) {
    return db.byDossierId(data);
  }

  byDossierIdByActeur(dossierId, acteurId) {
    return db.byDossierIdByActeur(dossierId, acteurId);
  }

  async byDossierIdByEquipes(dossierId, user) {
    let documentList = await this.byDossierIdByActeur(
      dossierId,
      user.organisation.acteur_id
    );

    let documentEquipeList;
    if (user.equipes) {
      documentEquipeList = user.equipes.map(async (equipe) => {
        return await this.byDossierIdByActeur(dossierId, equipe.acteur_id);
      });
    }

    if (Array.isArray(documentEquipeList) && documentEquipeList.length > 0)
      documentList.push(documentEquipeList);

    return documentList;
  }

  suppression(id) {
    return db.delete(id);
  }

  async deleteByEchangeId(echangeId) {
    const documents = await db.byEchangeId(echangeId);
    for (const element of documents) {
      this.suppression(element.id);
    }
  }

  addActeurDocument(documentId, acteurId) {
    return db.addActeurDocument(documentId, acteurId);
  }

  modifierActionId(data) {
    return db.modifyActionId(data);
  }

  modifyDroits(data) {
    return db.modifyDroits(data);
  }

  modifyEchangeId(id, echangeId) {
    return db.modifyEchangeId(id, echangeId);
  }

  suppressionGlobal() {
    return db.deleteGlobal();
  }
  insertClear(data) {
    return db.insertClear(data);
  }
}

export default new DocumentsService();
