import l from '../../common/logger';
import db from './actions.db.service';
import dbCat from './categories.db.service';

class ActionsService {
  all() {
    l.info(`${this.constructor.name}.all()`);
    return db.all();
  }

  byId(id) {
    return db.byId(id);
  }

  create(data) {
    return db.insert(data);
  }

  modify(data) {
    return db.modify(data);
  }

  byCategorieId(id) {
    return dbCat.getActionsById(id);
  }
}

export default new ActionsService();
