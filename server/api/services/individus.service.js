import l from '../../common/logger';
import db from './individus.db.service';

class IndividusService {
  all() {
    l.info(`${this.constructor.name}.all()`);
    return db.all();
  }

  byId(id) {
    l.info(`${this.constructor.name}.byId(${id})`);
    return db.byId(id);
  }

  create(individu) {
    return db.insert(individu);
  }
}

export default new IndividusService();
