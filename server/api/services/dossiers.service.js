import l from '../../common/logger';
import db from './dossiers.db.service';
import ActeursService from './acteurs.service';
import EchangesService from './echanges.service';

class DossiersService {
  async hasAccess(dossier, user) {
    const organisation = user.organisation.acteur_id;
    if (dossier.acteur_id.toString() === organisation.toString()) return true;

    const echanges = await EchangesService.byDossierIdByActeur(
      dossier._id,
      organisation
    );

    if (echanges && echanges.length > 0) return true;

    return false;
  }

  all() {
    l.info(`${this.constructor.name}.all()`);
    return db.all();
  }

  async byId(id) {
    try {
      return await db.byId(id);
    } catch (err) {
      return false;
    }
  }

  create(dossier) {
    return db.insert(dossier);
  }

  modify(dossier) {
    return db.modify(dossier);
  }

  addIndividu(individu) {
    return db.addIndividu(individu);
  }

  removeIndividu(objet) {
    return db.removeIndividu(objet);
  }

  updateIndividu(data, objet) {
    return db.updateIndividu(data, objet);
  }

  updateAdresse(data) {
    return db.updateAdresse(data);
  }

  updateReference(data) {
    return db.updateReference(data);
  }

  byListId(data, cloture) {
    return db.byListId(data, cloture);
  }

  async cloture(id, clos, dateClos, dateSuppressionDossier) {
    return db.cloture(id, clos, dateClos, dateSuppressionDossier);
  }

  /**
   *
   * @param data
   * @returns {Array.<Dossier>}
   */
  async isClos(data) {
    return db.isClos(data);
  }

  /**
   *
   * @param data
   * @returns {Array.<Dossier>}
   */
  async isOpen(data) {
    return db.isOpen(data);
  }

  deleteSecteurDossiers(secteur_Id) {
    db.deleteSecteurDossiers(secteur_Id);
  }

  updateSecteurDossiers(secteur_id, secteur_value) {
    db.updateSecteurDossiers(secteur_id, secteur_value);
  }

  addSecteur(id, acteur_id, secteur) {
    return db.addSecteur(id, acteur_id, secteur._id, secteur.valeur);
  }

  async setSecteur(id, acteur_id, secteur_id) {
    const dossier = await this.byId(id);
    const secteur = dossier?.secteurOrganisation?.find(
      (sec) => sec.acteur_id === acteur_id
    );
    const acteur = await ActeursService.byId(acteur_id);
    const secteurActeur = acteur.secteurs.find(
      (sec) => String(sec._id) === String(secteur_id)
    );
    if (secteurActeur) {
      if (secteur) {
        return db.setSecteur(
          id,
          acteur_id,
          secteurActeur._id,
          secteurActeur.valeur
        );
      } else {
        return this.addSecteur(id, acteur_id, secteurActeur);
      }
    }
    return false;
  }

  deleteSecteur(id, secteur_Id) {
    return db.deleteSecteur(id, secteur_Id);
  }

  async addSecteurGeo(id, acteur_id, secteur_Obj) {
    const dossier = await this.byId(id);
    const secteur = dossier?.secteurGeo.reduce((element, secteur) => {
      if (element) return element;
      if (String(secteur.acteur_id) === String(acteur_id)) return secteur;
      return null;
    }, null);
    if (secteur) {
      this.setSecteurGeo(secteur._id, secteur_Obj);
    } else {
      db.addSecteurGeo(id, acteur_id, secteur_Obj.code, secteur_Obj.libelle);
    }
  }

  setSecteurGeo(id, secteur_Obj) {
    return db.setSecteurGeo(id, secteur_Obj.code, secteur_Obj.libelle);
  }

  deleteSecteurGeo(secteur_Id) {
    return db.deleteSecteurGeo(secteur_Id);
  }

  checkReference(data) {
    return db.checkReference(data);
  }

  createReference(data) {
    return db.createReference(data);
  }

  listDossierEnCours(dossier, echange, aTraiter, acteur) {
    return dossier.reduce((dossiers, dos) => {
      let dossier = JSON.parse(JSON.stringify(dos));
      dossier.categories = [];
      const echangeDuDossier = echange.filter(
        (echange) => String(echange.dossier_id) === String(dossier._id)
      );
      const echDossReduced = echangeDuDossier.reduce((prev, curr) => {
        return [...prev, !!curr.action[0].urgent];
      }, []);
      dossier.urgenceRoot = echDossReduced;
      const { urgent, gardeDossier, lastUpdate } = echange.reduce(
        ({ urgent, gardeDossier, lastUpdate }, ech) => {
          if (String(ech.dossier_id) !== String(dos._id)) {
            return { urgent, gardeDossier };
          }
          if (aTraiter && ech.clos === 1) return { urgent, gardeDossier };
          const triActions = EchangesService.triActionEchange(ech);
          let garde = aTraiter
            ? EchangesService.estGarde(triActions, acteur)
            : !EchangesService.estGarde(triActions, acteur);
          let estUrgent = EchangesService.estUrgent(triActions);
          let lastDateUpdate = ech.updated_at;

          if (
            dossier.categories.findIndex(
              (doss) =>
                String(doss.categorie_id) === String(ech.categorie.categorie_id)
            ) === -1
          ) {
            dossier.categories = dossier.categories.concat([ech.categorie]);
          }
          return {
            urgent: estUrgent || urgent,
            gardeDossier: garde || gardeDossier,
            lastUpdate:
              new Date(lastDateUpdate) > new Date(lastUpdate)
                ? lastDateUpdate
                : lastUpdate,
          };
        },
        { urgent: false, gardeDossier: false, lastUpdate: '2000-01-01' }
      );
      if (gardeDossier) {
        dossier.urgent = urgent === 1 ? true : false;
        dossier.updated_at =
          new Date(lastUpdate) > new Date(dossier.updated_at)
            ? lastUpdate
            : dossier.updated_at;
        dossiers.push(dossier);
      }
      return dossiers;
    }, []);
  }
  suppressionGlobal() {
    return db.deleteGlobal();
  }

  insertClear(dossier) {
    return db.insertClear(dossier);
  }

  deleteOne(id) {
    return db.deleteOne(id);
  }

  deleteDossierClos(id) {
    return db.deleteDossierClos(id);
  }
}

export default new DossiersService();
