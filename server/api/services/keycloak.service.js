import api from './keycloak.api.service';
import latinize from 'latinize';
import l from '../../common/logger';

import GroupAccess from '../vendors/GroupAccess';
import config from '../../common/config';

class KeycloakService {
  addToGroup(user, group) {
    return api.addUserToGroup(user, group);
  }

  removeFromGroupe(user, group) {
    return api.removeUserFromGroup(user, group);
  }

  getGroups() {
    return api.getGroupList();
  }

  getGroupsWithMask() {
    return api.getGroupList(GroupAccess.ADMIN_FONCTIONNEL);
  }

  getGroupesUser(userId) {
    return api.getGroupListUser(userId);
  }

  getUserInfo(id) {
    return api.getUser(id);
  }

  getUsers() {
    return null;
  }

  getUsersWithPredicate(predicate) {
    return api.getUsersWithPredicate(predicate);
  }

  async cleanGroupesUser(userId) {
    const groupes = await this.getGroupesUser(userId);
    for (let i = 0; i < groupes.length; i++) {
      await this.removeFromGroupe(userId, groupes[i].id);
    }
  }

  async getUserWithPredicate(predicate) {
    const res = await this.getUsersWithPredicate(predicate);
    return res && res.length > 0 ? res[0] : null;
  }

  async getFreeLogin(nom, prenom) {
    let i = 0;
    let res, username;
    do {
      i++;
      username = latinize(
        prenom.toLowerCase().substr(0, i) + '.' + nom.toLowerCase()
      );
      res = await this.getUsersWithPredicate({ username });
    } while (res.length > 0);
    return username;
  }

  async getUserOrCreate(nom, prenom, email, k_user_id) {
    let userKeycloak = k_user_id
      ? await this.getUserInfo(k_user_id)
      : await this.getUserWithPredicate({ email });
    if (!userKeycloak) {
      const username = await this.getFreeLogin(nom, prenom);

      userKeycloak = await this.addUser({
        lastName: nom,
        username,
        email,
        firstName: prenom,
        enabled: true,
        emailVerified: false,
        credentials: [
          {
            type: config.keycloak.newUserCredentials.typeCredential,
            value: config.keycloak.newUserCredentials.value,
            temporary: config.keycloak.newUserCredentials.temporary,
          },
        ],
      });
      l.info(`${this.constructor.name}.getUserOrCreate(${username})`);
    }
    return userKeycloak;
  }

  deleteUser(userId) {
    return api.deleteUser(userId);
  }

  addUser(user) {
    return api.addUser(user);
  }
}

export default new KeycloakService();
