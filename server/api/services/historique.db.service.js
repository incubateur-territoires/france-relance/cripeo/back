const mongoose = require('mongoose');
const Historique = mongoose.model('Historique');

class HistoriquesDatabase {
  constructor() {}

  async all() {
    return await Historique.find();
  }

  async byId(id) {
    return await Historique.findById(id);
  }

  async byDossierId(id) {
    return await Historique.find({ dossier_id: id });
  }

  async insert(data) {
    const historiques = new Historique({ ...data });
    return await historiques.save();
  }

  async delete(id){
    return await Historique.deleteOne({ _id: id});
  }
}
export default new HistoriquesDatabase();