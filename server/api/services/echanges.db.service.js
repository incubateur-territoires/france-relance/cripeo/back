const mongoose = require('mongoose');
const Echange = mongoose.model('Echange');
const { encrypt, decrypt } = require('../vendors/crypto');

class EchangesDatabase {
  constructor() {
    this.cypherEchange = this.cypherEchange.bind(this);
    this.cypherEtat = this.cypherEtat.bind(this);
    this.cypherCategorie = this.cypherCategorie.bind(this);
    this.cypherAction = this.cypherAction.bind(this);
    this.decypherEchange = this.decypherEchange.bind(this);
    this.decypherEtat = this.decypherEtat.bind(this);
    this.decypherCategorie = this.decypherCategorie.bind(this);
    this.decypherAction = this.decypherAction.bind(this);
    this.all = this.all.bind(this);
    this.byId = this.byId.bind(this);
    this.insert = this.insert.bind(this);
    this.byDossier = this.byDossier.bind(this);
    this.byDossierId = this.byDossierId.bind(this);
    this.countEchangeDossier = this.countEchangeDossier.bind(this);
  }

  cypherEchange(echange) {
    if (echange.commentairePrive)
      echange.commentairePrive = encrypt(echange.commentairePrive);
    if (echange.commentairePublic)
      echange.commentairePublic = encrypt(echange.commentairePublic);
    if (echange.action && echange.action.length > 0)
      echange.action = echange.action.map((act) => this.cypherAction(act));
    if (echange.categorie)
      echange.categorie = this.cypherCategorie(echange.categorie);
    if (echange.etat && echange.etat.length > 0)
      echange.etat = echange.etat.map((etat) => this.cypherEtat(etat));
    return echange;
  }

  cypherEtat(etat) {
    if (etat.libelle) etat.libelle = encrypt(etat.libelle);
    if (etat.user) {
      etat.user.nom = encrypt(etat.user.nom);
      etat.user.prenom = encrypt(etat.user.prenom);
      etat.user.username = encrypt(etat.user.username);
    }
    return etat;
  }

  cypherCategorie(categorie) {
    if (categorie.libelle) categorie.libelle = encrypt(categorie.libelle);
    if (categorie.emetteur) categorie.emetteur = encrypt(categorie.emetteur);
    if (categorie.destinataire)
      categorie.destinataire = encrypt(categorie.destinataire);
    return categorie;
  }

  cypherAction(action) {
    if (action.commentairePrive)
      action.commentairePrive = encrypt(action.commentairePrive);
    if (action.commentairePublic)
      action.commentairePublic = encrypt(action.commentairePublic);
    if (action.emetteur) action.emetteur = encrypt(action.emetteur);
    if (action.destinataire) action.destinataire = encrypt(action.destinataire);
    if (action.libelle) action.libelle = encrypt(action.libelle);
    if (action.createdBy) action.createdBy = encrypt(action.createdBy);
    return action;
  }

  cypherEditEchange(editEchange) {
    if (editEchange.commentairePrive)
      editEchange.commentairePrive = encrypt(editEchange.commentairePrive);
    if (editEchange.commentairePublic)
      editEchange.commentairePublic = encrypt(editEchange.commentairePublic);
    return editEchange;
  }

  decypherEchange(echange) {
    if (echange.commentairePrive)
      echange.commentairePrive = decrypt(echange.commentairePrive);
    if (echange.commentairePublic)
      echange.commentairePublic = decrypt(echange.commentairePublic);
    if (echange.action && echange.action.length > 0)
      echange.action = echange.action.map((act) => this.decypherAction(act));
    if (echange.categorie)
      echange.categorie = this.decypherCategorie(echange.categorie);
    if (echange.etat && echange.etat.length > 0)
      echange.etat = echange.etat.map((etat) => this.decypherEtat(etat));
    return echange;
  }

  decypherCategorie(categorie) {
    if (categorie.libelle) categorie.libelle = decrypt(categorie.libelle);
    if (categorie.emetteur) categorie.emetteur = decrypt(categorie.emetteur);
    if (categorie.destinataire)
      categorie.destinataire = decrypt(categorie.destinataire);
    return categorie;
  }

  decypherEtat(etat) {
    if (etat.libelle) etat.libelle = decrypt(etat.libelle);
    if (etat.user) {
      etat.user.nom = decrypt(etat.user.nom);
      etat.user.prenom = decrypt(etat.user.prenom);
      etat.user.username = decrypt(etat.user.username);
    }
    return etat;
  }

  decypherAction(action) {
    if (action.commentairePrive)
      action.commentairePrive = decrypt(action.commentairePrive);
    if (action.commentairePublic)
      action.commentairePublic = decrypt(action.commentairePublic);
    if (action.emetteur) action.emetteur = decrypt(action.emetteur);
    if (action.destinataire) action.destinataire = decrypt(action.destinataire);
    if (action.libelle) action.libelle = decrypt(action.libelle);
    if (action.createdBy) action.createdBy = decrypt(action.createdBy);
    return action;
  }

  async all() {
    const echanges = await Echange.find();
    return echanges.map((ech) => {
      return this.decypherEchange(ech);
    });
  }

  async byId(id) {
    let echange = await Echange.findById(id);
    echange = this.decypherEchange(echange);
    return echange;
  }

  async insert(data) {
    const echangesCypher = await this.cypherEchange(data);
    const echanges = new Echange({ ...echangesCypher });
    const echangeCyphered = await echanges.save();
    return this.decypherEchange(echangeCyphered);
  }

  async modify(id, data) {
    const cypherEditEchange = this.cypherEditEchange(data);
    let echange1 = await Echange.findOneAndUpdate(
      { _id: id },
      {
        $set: {
          ...cypherEditEchange,
        },
      },
      { new: true }
    );

    let echange = await Echange.findOneAndUpdate(
      { _id: id, 'action._id': echange1.action[0]._id },
      {
        $set: {
          'action.$.urgent': data.Prioritaire ? 1 : 0,
        },
      },
      { new: true }
    );

    echange = this.decypherEchange(echange);
    return echange;
  }

  async echangeEstLu(id, date) {
    let echange = await Echange.findOneAndUpdate(
      { _id: id },
      {
        $set: {
          estLu: date,
        },
      },
      { new: true }
    );

    echange = this.decypherEchange(echange);
    return echange;
  }

  async byActeur(acteur_id) {
    let echanges = await Echange.find(
      {
        $or: [
          { 'action.emetteur_id': acteur_id },
          { 'action.destinataire_id': acteur_id },
          { 'information.acteur_id': acteur_id },
        ],
      },
      {
        dossier_id: 1,
        _id: 0,
        updated_at: 1,
        'categorie.categorie_id': 1,
        'categorie.libelle': 1,
        'action.urgent': 1,
        'action.destinataire_id': 1,
        'action.dateRealisation': 1,
        'information.acteur_id': 1,
        clos: 1,
      }
    );
    return echanges.map((ech) => {
      return this.decypherEchange(ech);
    });
  }

  async byDossier(dossier_id) {
    let echanges = await Echange.find({ dossier_id: dossier_id });
    return echanges.map((ech) => {
      return this.decypherEchange(ech);
    });
  }

  async byActeurEnTraitement(acteur_id) {
    let echanges = await Echange.find(
      {
        'action.destinataire_id': acteur_id,
        'etat.libelle': { $ne: 'Répondu' },
      },
      {
        dossier_id: 1,
        'action.urgent': 1,
        'action.destinataire_id': 1,
        'action.dateRealisation': 1,
        'categorie.categorie_id': 1,
        'categorie.libelle': 1,
        clos: 1,
      }
    );
    return echanges.map((ech) => {
      return this.decypherEchange(ech);
    });
  }

  async byDossierId(dossier_id) {
    let echanges = await Echange.find({ dossier_id });
    return echanges.map((ech) => {
      return this.decypherEchange(ech);
    });
  }

  async byDossierIdByActeur(dossier_id, acteur_id) {
    if (dossier_id && acteur_id) {
      let echanges = await Echange.find({
        dossier_id,
        $or: [
          { 'information.acteur_id': acteur_id },
          { 'categorie.emetteur_id': acteur_id },
          { 'categorie.destinataire_id': acteur_id },
        ],
      });
      return echanges.map((ech) => {
        const eche = this.decypherEchange(ech);
        return eche;
      });
    } else {
      throw new Error('Dossier manquant');
    }
  }

  async byDossierIdByActeurAvecInformation(dossier_id, acteur_id) {
    if (dossier_id && acteur_id) {
      let echanges = await Echange.find({
        dossier_id,
        $or: [
          { 'information.acteur_id': acteur_id },
          { 'categorie.emetteur_id': acteur_id },
          { 'categorie.destinataire_id': acteur_id },
        ],
        information: { $not: { $size: 0 } },
      });
      return echanges.map((ech) => {
        const eche = this.decypherEchange(ech);
        return eche;
      });
    } else {
      throw new Error('Dossier manquant');
    }
  }

  async getEchangeActeur(echange_id, acteur_id) {
    let echanges = Echange.find({
      _id: echange_id,
      $or: [
        { 'categorie.emetteur_id': acteur_id },
        { 'categorie.destinataire_id': acteur_id },
      ],
    });
    return echanges.map((ech) => {
      return this.decypherEchange(ech);
    });
  }

  async getListAllActionsByDossierId(dossier_id) {
    let echanges = await Echange.find({
      dossier_id,
    });
    const actionList = echanges.reduce((prev, curr) => {
      let info = JSON.parse(JSON.stringify(curr.information));
      let acts = JSON.parse(JSON.stringify(curr.action));
      acts = acts.map((act) => {
        let newAct = { ...act };
        newAct.echange_id = curr._id;
        newAct.information = info;
        return newAct;
      });
      return [...prev, ...acts];
    }, []);
    return actionList;
  }

  async addAction(echange_id, data) {
    const cypherAction = this.cypherAction(data);
    let echange = await Echange.findOneAndUpdate(
      { _id: echange_id },
      {
        $push: {
          action: {
            ...cypherAction,
          },
        },
      },
      { new: true }
    );
    echange = this.decypherEchange(echange);
    return echange;
  }

  /**
   * Intègre des documents dans un echange
   * @param {string} echange_id
   * @param {string} documents
   * @returns Echange
   */
  async integreDocumentAction(echange_id, documents) {
    const echange = await this.byId(echange_id);

    let docs =
      echange.action && echange.action.length > 0
        ? [...echange.action[0].document]
        : [];
    documents.forEach((document) => docs.push({ document_id: document._id }));

    if (echange.action && echange.action.length > 0) {
      let echangeEnded = await Echange.findOneAndUpdate(
        { _id: echange_id, 'action._id': echange.action[0]._id },
        {
          $set: {
            'action.$.document': docs,
          },
        },
        {
          new: true,
        }
      );
      echangeEnded = this.decypherEchange(echangeEnded);
      return echangeEnded;
    } else {
      let echangeEnded = await Echange.findOneAndUpdate(
        { _id: echange_id },
        {
          new: true,
        }
      );
      echangeEnded = this.decypherEchange(echangeEnded);
      return echangeEnded;
    }
  }

  async copieEchange(echange_id, transfertActeur) {
    const echange_old = await this.byId(echange_id);

    let data = this.decypherEchange(echange_old);
    data = JSON.parse(JSON.stringify(data));
    delete data._id;
    delete data.__v;
    data.categorie.destinataire_id = transfertActeur;

    const echange_new = await this.insert(data);
    return echange_new;
  }

  async cloture(id) {
    const echange = await this.byId(id);
    const action =
      echange.action && echange.action.length > 0
        ? echange.action[echange.action.length - 1]
        : null;

    await Echange.findOneAndUpdate(
      { _id: id, 'action._id': action._id },
      {
        $set: {
          'action.$.urgent': false,
        },
      },
      { new: true }
    );

    const ech = await Echange.findOneAndUpdate(
      { _id: id },
      {
        $bit: {
          clos: { xor: 1 },
        },
      },
      { new: true }
    );

    return this.decypherEchange(ech);
  }

  async setDroits(id, information) {
    const ech = await Echange.findOneAndUpdate(
      { _id: id },
      {
        $set: {
          information: information,
        },
      },
      { new: true }
    );
    return this.decypherEchange(ech);
  }

  async setLastActionUrgent(echange_id) {
    const echange = await this.byId(echange_id);

    const action =
      echange.action && echange.action.length > 0
        ? echange.action[echange.action.length - 1]
        : null;

    if (action) {
      const ech = await Echange.findOneAndUpdate(
        { _id: echange_id, 'action._id': action._id },
        {
          $set: {
            'action.$.urgent': true,
          },
        },
        { new: true }
      );
      return this.decypherEchange(ech);
    }
    return false;
  }

  async delete(id) {
    return Echange.findOneAndDelete({ _id: id });
  }

  async changeLibelleCategorieAllEchange(newLib, categorie_id) {
    await Echange.updateMany(
      {
        'categorie.categorie_id': categorie_id,
      },
      {
        $set: {
          'categorie.libelle': encrypt(newLib),
        },
      }
    );
  }

  async deleteGlobal() {
    await Echange.deleteMany();
    return true;
  }

  async inserClear(data) {
    await Echange.save(data);
    return true;
  }

  async countEchangeDossier(dossier_id) {
    const echanges = await Echange.find({ dossier_id: dossier_id });
    return echanges.length;
  }
}

export default new EchangesDatabase();
