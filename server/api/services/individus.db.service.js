const mongoose = require('mongoose');
const Individu = mongoose.model('Individu');

class IndividusDatabase {
  constructor() {
    this._data = [];
    this._counter = 0;
  }

  async all() {
    return Individu.find();
  }

  async byId(id) {
    return Individu.findById(id);
  }

  async insert(individu) {
    const individus = new Individu({ ...individu });
    return await individus.save();
  }
}

export default new IndividusDatabase();
