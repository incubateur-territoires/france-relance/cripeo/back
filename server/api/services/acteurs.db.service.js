const mongoose = require('mongoose');
const Acteur = mongoose.model('Acteur');
const Echange = mongoose.model('Echange');

const { encrypt, decrypt } = require('../vendors/crypto');

class ActeursDatabase {
  constructor() {}

  cypherActeur(acteur) {
    if (acteur.secteurs && acteur.secteurs.length > 0)
      acteur.secteurs = acteur.secteurs.map((sec) => {
        return {
          ...sec,
          valeur: encrypt(sec.valeur),
        };
      });

    if (acteur.secteurGeo && acteur.secteurGeo.url)
      acteur.secteurGeo.url = encrypt(acteur.secteurGeo.url);

    if (acteur.entite) acteur.entite = encrypt(acteur.entite);
    if (acteur.service) acteur.service = encrypt(acteur.service);
    return acteur;
  }

  decypherActeur(acteur) {
    if (acteur.secteurs && acteur.secteurs.length > 0)
      acteur.secteurs = acteur?.secteurs?.map((sec) => {
        if (sec.valeur) sec.valeur = decrypt(sec.valeur);
        return sec;
      });

    if (acteur.secteurGeo && acteur.secteurGeo.url)
      acteur.secteurGeo.url = decrypt(acteur.secteurGeo.url);

    if (acteur.entite) acteur.entite = decrypt(acteur.entite);
    if (acteur.service) acteur.service = decrypt(acteur.service);

    return acteur;
  }

  async all() {
    const acteurs = await Acteur.find({ acteur_id: null });
    return acteurs.map((acteur) => this.decypherActeur(acteur));
  }

  async byId(id) {
    if (!id) return null;
    try {
      const acteur = await Acteur.findById(id);
      return this.decypherActeur(acteur);
    } catch (err) {
      throw new Error("Impossible de recuperer l'acteur");
    }
  }

  async insert(data) {
    const ActeurCypher = this.cypherActeur(data);
    const acteurs = new Acteur({ ...ActeurCypher });
    const acteur = await acteurs.save();
    return this.decypherActeur(acteur);
  }

  async modify(data) {
    const ActeurCypher = this.cypherActeur(data);
    const acteur = await Acteur.findOneAndUpdate(
      { _id: data._id },
      {
        $set: { ...ActeurCypher },
      }
    );
    return this.decypherActeur(acteur);
  }

  async addEmail(id, data) {
    const acteur = await Acteur.findOneAndUpdate(
      { _id: id },
      {
        $push: { emails: data },
      },
      { new: true }
    );
    return this.decypherActeur(acteur);
  }

  async modifyEmail(email) {
    const acteur = await Acteur.findOneAndUpdate(
      { 'emails._id': email._id },
      {
        $set: {
          'emails.$': { ...email },
        },
      },
      { new: true }
    );
    return this.decypherActeur(acteur);
  }

  async deleteEmail(id) {
    const acteur = await Acteur.findOneAndUpdate(
      { 'emails._id': id },
      {
        $pull: {
          emails: { _id: id },
        },
      },
      { new: true }
    );
    return this.decypherActeur(acteur);
  }

  async changeFrequenceMail(id, frequence) {
    const acteur = await Acteur.findOneAndUpdate(
      {
        _id: id,
      },
      {
        $set: { frequenceRappel: frequence },
      },
      {
        new: true,
      }
    );
    return this.decypherActeur(acteur);
  }

  async changeDateDernierRappel(id, date) {
    const acteur = await Acteur.findOneAndUpdate(
      {
        _id: id,
      },
      {
        $set: { dateDernierRappel: date },
      },
      {
        new: true,
      }
    );
    return this.decypherActeur(acteur);
  }

  async getEchangebyActeur(id) {
    const echanges = await Echange.find({
      clos: 0,
      'action.destinataire_id': id,
    });
    return echanges;
  }

  async getLastActions(id) {
    const echanges = await Echange.find({
      clos: 0,
      'action.destinataire_id': id,
    });

    const actions = echanges.reduce((acc, echange) => {
      if (echange.action.length === 0) return acc;
      let action = echange.action.sort((a, b) => {
        return a.dateRealisation < b.dateRealisation ? 1 : -1;
      })[0];

      if (`${action.destinataire_id}` === `${id}`) {
        const test = acc.concat([action]);
        return test;
      }
      return acc;
    }, []);

    return actions;
  }

  async getUrgentLastActions(id) {
    const echanges = await Echange.find({
      clos: 0,
      'action.destinataire_id': id,
      'action.urgent': 1,
    });

    const actions = echanges.reduce((acc, echange) => {
      if (echange.action.length === 0) return acc;
      let action = echange.action.sort((a, b) => {
        return a.dateRealisation < b.dateRealisation ? 1 : -1;
      })[0];

      if (`${action.destinataire_id}` === `${id}` && action.urgent === 1) {
        const test = acc.concat([action]);
        return test;
      }

      return acc;
    }, []);

    return actions;
  }

  async byParentId(id) {
    const acteur = await Acteur.find({ acteur_id: id });
    return acteur.map((acteur) => this.decypherActeur(acteur));
  }

  async addSecteur(id, valeur) {
    const acteur = await Acteur.findOneAndUpdate(
      { _id: id },
      {
        $push: {
          secteurs: {
            valeur: encrypt(valeur),
          },
        },
      },
      {
        new: true,
      }
    );

    const decypher = this.decypherActeur(acteur);
    return decypher;
  }

  async editSecteur(idSecteur, valeur) {
    const acteur = await Acteur.findOneAndUpdate(
      {
        'secteurs._id': idSecteur,
      },
      {
        $set: {
          'secteurs.$.valeur': encrypt(valeur),
        },
      },
      {
        new: true,
      }
    );

    return this.decypherActeur(acteur);
  }

  async deleteSecteur(idSecteur) {
    const acteur = await Acteur.findOneAndUpdate(
      {
        'secteurs._id': idSecteur,
      },
      {
        $pull: {
          secteurs: { _id: idSecteur },
        },
      },
      { new: true }
    );

    return this.decypherActeur(acteur);
  }

  async addSecteurGeo(id, url, properties) {
    const acteur = await Acteur.findOneAndUpdate(
      { _id: id },
      {
        $set: {
          'secteurGeo.url': encrypt(url),
          'secteurGeo.properties': properties,
        },
      },
      {
        new: true,
      }
    );

    return this.decypherActeur(acteur);
  }

  async editSecteurGeo(idActeur, actif, url, properties) {
    const acteur = await Acteur.findOneAndUpdate(
      {
        _id: idActeur,
      },
      {
        $set: {
          'secteurGeo.actif': actif,
          'secteurGeo.url': encrypt(url),
          'secteurGeo.properties': properties,
        },
      },
      {
        new: true,
      }
    );

    return this.decypherActeur(acteur);
  }
}

export default new ActeursDatabase();
