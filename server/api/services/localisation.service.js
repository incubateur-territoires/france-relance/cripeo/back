import axios from 'axios';
import config from '../../common/config';
const turf = require('@turf/turf');

class LocalisationService {
  async getAdresse(voie, cp) {
    let adresse = [];
    if (voie.length >= 3) {
      const url = config.api_bano.url + '/search/?q=';
      const params =
        encodeURIComponent(voie.trim()) +
        '&postcode=' +
        encodeURIComponent(cp.trim()) +
        '&limit=5';
      const opt = process.env.PROXY_HOST
        ? {
            proxy: {
              host: process.env.PROXY_HOST,
              port: process.env.PROXY_PORT,
            },
          }
        : {};

      await axios
        .get(url + params, opt)
        .then(async (body) => {
          await adresse.push({
            coordonnees: body.data.features[0].geometry.coordinates,
            city: body.data.features[0].properties.city,
          });
        })
        .catch((err) => {
          console.log('erreur getadresse:' + err);
          return err;
        });
      return adresse[0] ? adresse[0] : '';
    }
    return [];
  }

  async testSecteur(url) {
    try {
      let flux = await axios.get(url);
      return flux.status;
    } catch {
      return 400;
    }
  }

  async getSecteur(url, coordonnees) {
    const x = coordonnees[0];
    const y = coordonnees[1];
    const secteurs = [];

    try {
      let flux = await axios.get(url);

      const geojson = {
        type: 'FeatureCollection',
        features: [],
      };
      geojson.features = flux.data?.features?.map((feature) => {
        return {
          properties: {
            libelle: feature.properties.nom,
            code: feature.properties.id,
          },
          geometry: feature.geometry,
        };
      });

      await turf.geomEach(
        geojson,
        async function (currentGeometry, featureIndex, featureProperties) {
          if (
            currentGeometry.type !== 'MultiPolygon' &&
            turf.booleanContains(currentGeometry, turf.point([x, y]))
          ) {
            await secteurs.push({
              libelle: featureProperties.libelle,
              code: featureProperties.code,
              geometry: currentGeometry,
            });
          } else {
            for (const coords of currentGeometry.coordinates) {
              let polygon = {
                type: 'Polygon',
                coordinates: coords,
              };
              if (turf.booleanContains(polygon, turf.point([x, y]))) {
                await secteurs.push({
                  libelle: featureProperties.libelle,
                  code: featureProperties.code,
                  geometry: currentGeometry,
                });
              }
            }
          }
        }
      );
      return secteurs[0] ? secteurs[0] : null;
    } catch (err) {
      console.log('Erreur pendant la recherche du secteur', err);
      return err;
    }
  }
}

export default new LocalisationService();
