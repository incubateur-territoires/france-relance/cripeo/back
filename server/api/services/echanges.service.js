import l from '../../common/logger';
import db from './echanges.db.service';

class EchangesService {
  all() {
    l.info(`${this.constructor.name}.all()`);
    return db.all();
  }

  byId(id) {
    return db.byId(id);
  }

  create(data) {
    return db.insert(data);
  }

  modify(id, data) {
    return db.modify(id, data);
  }

  echangeEstLu(id, date) {
    return db.echangeEstLu(id, date);
  }

  byActeur(data) {
    return db.byActeur(data);
  }

  byActeurEnTraitement(data) {
    return db.byActeurEnTraitement(data);
  }

  byDossierId(data) {
    return db.byDossierId(data);
  }

  byDossierIdByActeur(dossier_id, acteur_id) {
    return db.byDossierIdByActeur(dossier_id, acteur_id);
  }

  byDossierIdByActeurAvecInformation(dossier_id, acteur_id) {
    return db.byDossierIdByActeurAvecInformation(dossier_id, acteur_id);
  }

  addAction(echange_id, data) {
    return db.addAction(echange_id, data);
  }

  getListAllActionsByDossierId(dossier_id) {
    return db.getListAllActionsByDossierId(dossier_id);
  }

  integreDocumentAction(echange_id, documents) {
    return db.integreDocumentAction(echange_id, documents);
  }

  copieEchange(echange_id, transfertActeur) {
    return db.copieEchange(echange_id, transfertActeur);
  }

  cloture(id) {
    return db.cloture(id);
  }

  async droitModificationActeur(echange_id, acteur_id) {
    const getEchange = await db.getEchangeActeur(echange_id, acteur_id);
    return getEchange !== null || null;
  }

  setDroits(data) {
    return db.setDroits(data.id, data.information);
  }

  setLastActionUrgent(echange_id) {
    return db.setLastActionUrgent(echange_id);
  }

  triActionEchange(echange) {
    if (echange.action && echange.action.length > 0) {
      return echange.action.sort((a, b) =>
        new Date(a.dateRealisation) > new Date(b.dateRealisation) ? 1 : -1
      );
    }
    return [];
  }

  estUrgent(actions) {
    if (!actions || actions.length === 0) return false;
    return actions[actions.length - 1].urgent || false;
  }

  estGarde(actions, destinataire) {
    if (!actions || actions.length === 0) return false;
    return (
      String(actions[actions.length - 1].destinataire_id) ===
      String(destinataire)
    );
  }

  lastUpdate(actions) {
    if (!actions || actions.length === 0) return '2020-01-01';
    return actions[actions.length - 1].updated_at;
  }

  delete(id) {
    return db.delete(id);
  }

  changeLibelleCategorieAllEchange(newLib, categorie_id) {
    return db.changeLibelleCategorieAllEchange(newLib, categorie_id);
  }
  suppressionGlobal() {
    return db.deleteGlobal();
  }

  insertClear(data) {
    return db.insertClear(data);
  }
}

export default new EchangesService();
