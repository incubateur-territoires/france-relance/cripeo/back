const mongoose = require('mongoose');
const Dossier = mongoose.model('Dossier');
const { encrypt, decrypt } = require('../vendors/crypto');
import echangesDbService from './echanges.db.service';

class DossiersDatabase {
  constructor() {
    this.decypherIndividu = this.decypherIndividu.bind(this);
    this.decypherAdresse = this.decypherAdresse.bind(this);
    this.cypherDossier = this.cypherDossier.bind(this);
    this.decypherDossier = this.decypherDossier.bind(this);
    this.cypherIndividu = this.cypherIndividu.bind(this);
    this.cypherAdresse = this.cypherAdresse.bind(this);
  }

  decypherIndividu(individu) {
    if (individu) {
      if (individu.civilite) individu.civilite = decrypt(individu.civilite);
      if (individu.nom) individu.nom = decrypt(individu.nom);
      if (individu.prenom) individu.prenom = decrypt(individu.prenom);
      if (individu.dateNaissance)
        individu.dateNaissance = decrypt(individu.dateNaissance);
      if (individu.communeNaissance)
        individu.communeNaissance = decrypt(individu.communeNaissance);

      if (individu.paysNaissance)
        individu.paysNaissance = decrypt(individu.paysNaissance);
      if (individu.commentaire)
        individu.commentaire = decrypt(individu.commentaire);

      if (individu.lienParent) {
        individu.lienParent = decrypt(individu.lienParent);
      }

      if (individu.refIndividu) {
        individu.refIndividu.map((ref) => {
          ref.acteur_id = decrypt(ref.acteur_id);
          ref.numero = decrypt(ref.numero);
          return ref;
        });
      }
    }

    return individu;
  }

  decypherAdresse(adresse) {
    if (adresse.voie) adresse.voie = decrypt(adresse.voie);
    if (adresse.cp) adresse.cp = decrypt(adresse.cp);
    if (adresse.commune) adresse.commune = decrypt(adresse.commune);
    if (adresse.complement) adresse.complement = decrypt(adresse.complement);
    if (adresse.lieudit) adresse.lieudit = decrypt(adresse.lieudit);
    return adresse;
  }

  cypherDossier(dossier) {
    if (dossier.individuPrincipal)
      dossier.individuPrincipal = this.cypherIndividu(
        dossier.individuPrincipal
      );
    if (dossier.individuSecondaire)
      dossier.individuSecondaire = dossier.individuSecondaire.map((indiv) =>
        this.cypherIndividu(indiv)
      );
    if (dossier.adresse) dossier.adresse = this.cypherAdresse(dossier.adresse);
    if (dossier.individuPrincipalNom)
      dossier.individuPrincipalNom = encrypt(dossier.individuPrincipalNom);

    if (dossier.numeroApplication) {
      dossier.numeroApplication = dossier.numeroApplication.map((num) => {
        return {
          ...num,
          numero: encrypt(num.numero),
        };
      });
    }
    if (dossier.secteurOrganisation) {
      dossier.secteurOrganisation = dossier.secteurOrganisation.map((sect) => {
        return {
          ...sect,
          secteur: encrypt(sect.secteur),
        };
      });
    }
    if (dossier.secteurGeo) {
      dossier.secteurGeo = dossier.secteurGeo.map((sect) => {
        return {
          ...sect,
          libelle: encrypt(sect.libelle),
          code: encrypt(sect.code),
        };
      });
    }

    return dossier;
  }

  decypherDossier(dossier) {
    if (dossier.individuPrincipalNom) {
      dossier.individuPrincipalNom = decrypt(dossier.individuPrincipalNom);
    }
    if (dossier.individuPrincipal)
      dossier.individuPrincipal = this.decypherIndividu(
        dossier.individuPrincipal
      );
    if (dossier.individuSecondaire)
      dossier.individuSecondaire = dossier.individuSecondaire.map((indiv) =>
        this.decypherIndividu(indiv)
      );
    if (dossier.adresse)
      dossier.adresse = this.decypherAdresse(dossier.adresse);
    if (dossier.numeroApplication) {
      dossier.numeroApplication = dossier.numeroApplication.map((num) => {
        num.numero = decrypt(num.numero);
        return num;
      });
    }

    if (dossier.secteurOrganisation) {
      dossier.secteurOrganisation = dossier.secteurOrganisation.map((sect) => {
        sect.secteur = decrypt(sect.secteur);
        return sect;
      });
    }

    if (dossier.secteurGeo) {
      dossier.secteurGeo = dossier.secteurGeo.map((sect) => {
        sect.code = decrypt(sect.code);
        sect.libelle = decrypt(sect.libelle);
        return sect;
      });
    }

    return dossier;
  }

  cypherIndividu(individu) {
    if (individu.civilite) individu.civilite = encrypt(individu.civilite);
    if (individu.nom) individu.nom = encrypt(individu.nom);
    if (individu.prenom) individu.prenom = encrypt(individu.prenom);
    if (individu.dateNaissance)
      individu.dateNaissance = encrypt(individu.dateNaissance);
    if (individu.communeNaissance)
      individu.communeNaissance = encrypt(individu.communeNaissance);
    if (individu.paysNaissance)
      individu.paysNaissance = encrypt(individu.paysNaissance);
    if (individu.commentaire)
      individu.commentaire = encrypt(individu.commentaire);
    if (individu.lienParent) {
      individu.lienParent = encrypt(individu.lienParent);
    }

    if (individu.refIndividu) {
      individu.refIndividu = [
        {
          acteur_id: encrypt(individu.refIndividu.orga),
          numero: encrypt(individu.refIndividu.ref),
        },
      ];
    }

    return individu;
  }

  cypherAdresse(adresse) {
    if (adresse.voie) adresse.voie = encrypt(adresse.voie);
    if (adresse.cp) adresse.cp = encrypt(adresse.cp);
    if (adresse.commune) adresse.commune = encrypt(adresse.commune);
    if (adresse.complement) adresse.complement = encrypt(adresse.complement);
    if (adresse.lieudit) adresse.lieudit = encrypt(adresse.lieudit);
    return adresse;
  }

  async all() {
    const dossiers = await Dossier.find();
    return dossiers.map((dos) => {
      return this.decypherDossier(dos);
    });
  }

  async byId(id) {
    try {
      let dossier = await Dossier.findById(id);
      if (!dossier) return false;
      dossier = this.decypherDossier(dossier);
      return dossier;
    } catch (err) {
      console.error('Erreur byId', err);
      return false;
    }
  }

  async insert(dossier) {
    const dossierCypher = this.cypherDossier(dossier);

    const dossiers = new Dossier({ ...dossierCypher });
    return await dossiers.save();
  }

  async modify(dossier) {
    const dossierCypher = this.cypherDossier(dossier);
    let dos = Dossier.findOneAndUpdate(
      { _id: dossier.id },
      {
        $set: {
          ...dossierCypher,
        },
      }
    );
    return this.decypherDossier(dos);
  }

  async addIndividu(individu) {
    const cypherIndividu = this.cypherIndividu(individu.individuSecondaire);
    try {
      let dossier = await Dossier.findOneAndUpdate(
        { _id: individu.id },
        {
          $push: {
            individuSecondaire: {
              ...cypherIndividu,
            },
          },
        },
        {
          new: true,
        }
      );
      return this.decypherDossier(dossier);
    } catch (error) {
      console.error('Erreur :', error);
      throw new Error("Erreur lors de l'Ajout d'un individu");
    }
  }

  async removeIndividu(objet) {
    let dossier = await Dossier.findOneAndUpdate(
      { _id: objet.id },
      {
        $pull: {
          individuSecondaire: {
            _id: objet.individu_id,
          },
        },
      },
      {
        new: true,
      }
    );
    return this.decypherDossier(dossier);
  }

  async updateIndividu(data, objet) {
    if (data.cible === 'individuPrincipal') {
      const oldDossier = await this.byId(data.id);
      let refsIndividu = oldDossier.individuPrincipal.refIndividu;

      if (
        refsIndividu.some((ref) => ref.acteur_id === objet.refIndividu.orga)
      ) {
        refsIndividu[
          refsIndividu.findIndex(
            (ref) => ref.acteur_id === objet.refIndividu.orga
          )
        ].numero = objet.refIndividu.ref;
      } else {
        refsIndividu = [
          ...refsIndividu,
          { numero: objet.refIndividu.ref, acteur_id: objet.refIndividu.orga },
        ];
      }
      const encRefsIndividu = refsIndividu.map((ref) => {
        ref.numero = encrypt(ref.numero);
        ref.acteur_id = encrypt(ref.acteur_id);
        return ref;
      });

      const name = objet.nom + ' ' + objet.prenom;

      let dossier = await Dossier.findOneAndUpdate(
        { _id: data.id },
        {
          $set: {
            'individuPrincipal.civilite': encrypt(objet.civilite),
            'individuPrincipal.nom': encrypt(objet.nom),
            'individuPrincipal.prenom': encrypt(objet.prenom),
            'individuPrincipal.dateNaissance': encrypt(objet.dateNaissance),
            'individuPrincipal.communeNaissance': encrypt(
              objet.communeNaissance
            ),
            'individuPrincipal.paysNaissance': encrypt(objet.paysNaissance),
            'individuPrincipal.commentaire': encrypt(objet.commentaire),
            individuPrincipalNom: encrypt(name),
            'individuPrincipal.refIndividu': encRefsIndividu,
          },
        },
        {
          new: true,
        }
      );

      return this.decypherDossier(dossier);
    } else {
      const oldDossier = await this.byId(data.id);

      const individuSecondaire =
        oldDossier.individuSecondaire[
          oldDossier.individuSecondaire.findIndex(
            (elem) => elem._id.toString() === data.individu_id.toString()
          )
        ];
      let refsIndividu = individuSecondaire.refIndividu;

      if (
        refsIndividu.some((ref) => ref.acteur_id === objet.refIndividu.orga)
      ) {
        refsIndividu[
          refsIndividu.findIndex(
            (ref) => ref.acteur_id === objet.refIndividu.orga
          )
        ].numero = objet.refIndividu.ref;
      } else {
        refsIndividu = [
          ...refsIndividu,
          { numero: objet.refIndividu.ref, acteur_id: objet.refIndividu.orga },
        ];
      }

      const encRefsIndividu = refsIndividu.map((ref) => {
        ref.numero = encrypt(ref.numero);
        ref.acteur_id = encrypt(ref.acteur_id);
        return ref;
      });

      let dossier = await Dossier.findOneAndUpdate(
        { 'individuSecondaire._id': data.individu_id },
        {
          $set: {
            'individuSecondaire.$.civilite': encrypt(objet.civilite),
            'individuSecondaire.$.nom': encrypt(objet.nom),
            'individuSecondaire.$.prenom': encrypt(objet.prenom),
            'individuSecondaire.$.dateNaissance': encrypt(objet.dateNaissance),
            'individuSecondaire.$.communeNaissance': encrypt(
              objet.communeNaissance
            ),
            'individuSecondaire.$.paysNaissance': encrypt(objet.paysNaissance),
            'individuSecondaire.$.individuSolis': objet.individuSolis,
            'individuSecondaire.$.commentaire': encrypt(objet.commentaire),
            'individuSecondaire.$.lienParent': encrypt(objet.lienParent),
            'individuSecondaire.$.refIndividu': encRefsIndividu,
          },
        },
        {
          new: true,
        }
      );

      return this.decypherDossier(dossier);
    }
  }

  async updateAdresse(data) {
    const { id, adresse } = data;
    const { voie, cp, commune, lieudit, complement } = adresse;
    let obj;
    try {
      obj = {
        'adresse.voie': await encrypt(voie),
        'adresse.cp': await encrypt(cp),
        'adresse.commune': await encrypt(commune),
        'adresse.lieudit': await encrypt(lieudit),
        'adresse.complement': await encrypt(complement),
      };
    } catch (error) {
      console.error('Erreur :', error);
      throw new Error("Erreur lors du chiffrement de l'adresse");
    }
    let dossier;
    try {
      dossier = await Dossier.findOneAndUpdate(
        { _id: id },
        {
          $set: { ...obj },
        },
        {
          new: true,
        }
      );
    } catch (error) {
      console.error('Erreur Modification:', error);
      throw new Error("Erreur lors de la modification de l'adresse");
    }
    try {
      return this.decypherDossier(dossier);
    } catch (error) {
      console.error('Erreur :', error);
      throw new Error("Erreur lors de la modification de l'adresse");
    }
  }

  async updateReference(data) {
    let dossier = await Dossier.findOneAndUpdate(
      { _id: data.id, 'numeroApplication.acteur_id': data.acteur_id },
      {
        $set: {
          'numeroApplication.$.numero': encrypt(data.numero),
        },
      },
      {
        new: true,
      }
    );
    return this.decypherDossier(dossier);
  }

  async byListId(data, cloture) {
    const dossiers = await Dossier.find({ _id: { $in: data }, clos: cloture });
    return dossiers.map((dos) => {
      return this.decypherDossier(dos);
    });
  }

  async cloture(id, clos, dateClos, dateSuppression) {
    return Dossier.findOneAndUpdate(
      { _id: id },
      {
        $set: {
          clos: clos,
          dateClos: dateClos,
          dateSuppressionDossier: dateSuppression,
        },
      },
      { new: true }
    );
  }

  async isClos(data) {
    const dossiers = await Dossier.find({ acteur_id: data, clos: true });
    return dossiers.map((dos) => {
      return this.decypherDossier(dos);
    });
  }

  async isClosExcludeIdArray(data, ids) {
    const dossiers = await Dossier.find({
      acteur_id: data,
      clos: true,
      _id: { $nin: ids },
    });
    return dossiers.map((dos) => {
      return this.decypherDossier(dos);
    });
  }

  async isOpen(data) {
    let dossiers = await Dossier.find({ acteur_id: data, clos: false });
    dossiers = dossiers.map((dos) => {
      return this.decypherDossier(dos);
    });
    return dossiers;
  }

  async deleteSecteurDossiers(secteur_Id) {
    Dossier.updateMany(
      {
        'secteurOrganisation.secteur_id': secteur_Id,
      },
      {
        $pull: {
          secteurOrganisation: { secteur_id: secteur_Id },
        },
      }
    );
  }

  async updateSecteurDossiers(secteur_id, secteur_value) {
    Dossier.updateMany(
      {
        'secteurOrganisation.secteur_id': secteur_id,
      },
      {
        $set: {
          'secteurOrganisation.$.secteur': encrypt(secteur_value),
        },
      }
    );
  }

  async setSecteur(id, acteur_Id, secteur_Id, secteur_Value) {
    try {
      const dossier = await Dossier.findOneAndUpdate(
        {
          _id: id,
          'secteurOrganisation.acteur_id': acteur_Id,
        },
        {
          $set: {
            'secteurOrganisation.$.secteur_id': secteur_Id,
            'secteurOrganisation.$.secteur': encrypt(secteur_Value),
          },
        },
        {
          new: true,
        }
      );

      return this.decypherDossier(dossier);
    } catch (err) {
      console.error(err);
      throw new Error(
        'Problème lors de la mise à jour du secteur administratif'
      );
    }
  }

  async addSecteur(id, acteur_id, secteur_id, secteur_value) {
    const dossier = await Dossier.findOneAndUpdate(
      {
        _id: id,
      },
      {
        $push: {
          secteurOrganisation: {
            secteur_id: secteur_id,
            acteur_id: acteur_id,
            secteur: encrypt(secteur_value),
          },
        },
      },
      {
        new: true,
      }
    );

    return this.decypherDossier(dossier);
  }

  async deleteSecteur(id) {
    const dossier = await Dossier.findOneAndUpdate(
      {
        'secteurOrganisation._id': id,
      },
      {
        $pull: {
          secteurOrganisation: { _id: id },
        },
      },
      {
        new: true,
      }
    );
    return this.decypherDossier(dossier);
  }

  async setSecteurGeo(secteur_id, secteur_code, secteur_libelle) {
    const dossier = await Dossier.findOneAndUpdate(
      {
        'secteurGeo._id': secteur_id,
      },
      {
        $set: {
          'secteurGeo.$.code': encrypt(secteur_code),
          'secteurGeo.$.libelle': encrypt(secteur_libelle),
        },
      },
      {
        new: true,
      }
    );

    return this.decypherDossier(dossier);
  }

  async addSecteurGeo(id, acteur_id, secteur_code, secteur_libelle) {
    const dossier = await Dossier.findOneAndUpdate(
      {
        _id: id,
      },
      {
        $push: {
          secteurGeo: {
            acteur_id: acteur_id,
            code: encrypt(secteur_code),
            libelle: encrypt(secteur_libelle),
          },
        },
      },
      {
        new: true,
      }
    );

    return this.decypherDossier(dossier);
  }

  async deleteSecteurGeo(id) {
    const dossier = await Dossier.findOneAndUpdate(
      {
        'secteurGeo._id': id,
      },
      {
        $pull: {
          secteurGeo: { _id: id },
        },
      },
      {
        new: true,
      }
    );
    return this.decypherDossier(dossier);
  }

  async checkReference(data) {
    const { id, acteur_id } = data;
    return Dossier.findOne({
      _id: id,
      'numeroApplication.acteur_id': acteur_id,
    });
  }

  async createReference(data) {
    const { id, acteur_id, numero } = data;
    let dossier = await Dossier.findOneAndUpdate(
      {
        _id: id,
      },
      {
        $push: {
          numeroApplication: {
            acteur_id: acteur_id,
            numero: encrypt(numero),
          },
        },
      },
      {
        new: true,
      }
    );
    return this.decypherDossier(dossier);
  }
  async deleteGlobal() {
    await Dossier.deleteMany();
    return true;
  }

  async insertClear(dossier) {
    const dossiers = new Dossier(dossier);
    return await dossiers.save();
  }

  async deleteOne(id) {
    const count = await echangesDbService.countEchangeDossier(id);
    if (count === 0) {
      await Dossier.deleteOne({ _id: id });
      return true;
    } else return false;
  }

  async deleteDossierClos(id) {
    const dossier = await Dossier.deleteOne({ _id: id });
    if (dossier.ok === 1) {
      return true;
    } else {
      return false;
    }
  }
}

export default new DossiersDatabase();
