import mail from './mail.module.service';
import { check, validationResult } from 'express-validator';
import ActeursService from './acteurs.db.service';
import UtilisateursService from './utilisateurs.db.service';
import KeycloakService from './keycloak.api.service';

import { mailer } from '../../common/config';

class MailService {
  /**
   * Envoie d'un mail
   * @param to String - Destinataire Si plusieurs adresses, les séparer par des virgules
   * @param subject String - Sujet
   * @param body String - Corps du mail (en html)
   * @param priority Boolean - Priorité, normal par défaut, true = priorité haute
   * @returns Object{message: boolean, status: boolean}
   */
  async sendMail(to, subject, body, priority) {
    if (mailer.actif) {
      let msg = false;
      if (!this.checkMail(to)) {
        msg = 'Adresse mail de destinataires invalides';
      }
      if (!subject || subject.length < 1) {
        msg = 'Sujet invalide';
      }
      if (msg) {
        return { status: false, message: msg };
      }

      return await mail.sendMail(to, subject, body, priority);
    } else {
      return { message: false, status: true };
    }
  }

  /**
   * Envoie d'un mail
   * @param to String - Destinataire Si plusieurs adresses, les séparer par des virgules
   * @param subject String - Sujet
   * @param body String - Corps du mail (en html)
   * @param priority Boolean - Priorité, normal par défaut, true = priorité haute
   * @returns Object{message: boolean, status: boolean}
   */
  async sendMailCreateUser(to, subject, body, priority) {
    let msg = false;
    if (!this.checkMail(to)) {
      msg = 'Adresse mail de destinataires invalides';
    }
    // if (!body || body.length < 1) {
    //   msg = "Message invalide";
    // }
    if (!subject || subject.length < 1) {
      msg = 'Sujet invalide';
    }
    if (msg) {
      return { status: false, message: msg };
    }

    return await mail.sendMail(to, subject, body, priority);
  }

  checkMail(emails) {
    const emailsArray = emails.split(',');
    let errors = [];
    if (emailsArray.length === 0) {
      return false;
    }

    for (let i = 0; i < emailsArray.length; i++) {
      const email = emailsArray[i];
      const checks = check(email).isEmail();
      const err = validationResult(checks);
      if (!err.isEmpty()) {
        errors.push(err);
      }
    }

    if (errors.length > 0) {
      console.log(`Erreur sur ${errors.length} adresses`);
      console.log(errors);
      return false;
    } else {
      return true;
    }
  }

  async getMail(idActeur, filtreTypeBoiteMail) {
    // = [TypeBoiteMail.URGENTE, TypeBoiteMail.CLASSIQUE]
    const acteur = await ActeursService.byId(idActeur);
    if (
      acteur.emails &&
      Array.isArray(acteur.emails) &&
      acteur.emails.length > 0
    ) {
      if (filtreTypeBoiteMail === undefined) return acteur.emails;
      return acteur.emails.reduce((acc, value) => {
        if (filtreTypeBoiteMail.length === 0) return acc;
        if (
          filtreTypeBoiteMail &&
          filtreTypeBoiteMail.every((boiteMail) =>
            value.typeBoiteEnvoi.find((element) => element === boiteMail)
          )
        ) {
          if (!acc) return value.adresse;
          return acc + ',' + value.adresse;
        }
        return acc;
      }, '');
    } else {
      const utilisateurs = await UtilisateursService.allByActeur(idActeur);
      if (utilisateurs) {
        try {
          const utils = await Promise.all(
            utilisateurs.map(async (user) => {
              const u = await KeycloakService.getUser(user.k_userId);
              return u.email;
            })
          );
          return utils.reduce((acc, value) => {
            if (!acc) return value;
            return acc + ',' + value;
          }, '');
        } catch (err) {
          console.error(
            "Erreur de fonction de getMail() dans mailService avec l'arguments de 'idActeur' et 'filtreTypeBoiteMail': ",
            idActeur,
            filtreTypeBoiteMail,
            err
          );
          return null;
        }
      } else {
        return false;
      }
    }
  }
}

export default new MailService();
