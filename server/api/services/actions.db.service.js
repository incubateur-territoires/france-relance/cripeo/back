const mongoose = require('mongoose');
const Action = mongoose.model('Action');

class ActionsDatabase {
  constructor() {}

  async all() {
    return Action.find();
  }

  async byId(id) {
    return Action.findById(id);
  }

  async insert(data) {
    const actions = new Action({ ...data });
    return await actions.save();
  }

  async modify(data) {
    return Action.findOneAndUpdate(
      { _id: data._id },
      {
        $set: {
          libelle: data.libelle,
        },
      }
    );
  }

  async byCatId(id) {
    return Action.find({
      categorie_id: id,
    });
  }
}

export default new ActionsDatabase();
