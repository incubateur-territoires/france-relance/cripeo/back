const keycloakADM = require('../../common/keycloak_admin');

const keycloak = new keycloakADM().getInstance();

const EMAIL_DOMAIN_PROTECT = process.env.EMAIL_DOMAIN_PROTECT;

class KeycloakApiService {
  getKcClient() {
    const con = keycloak.getKeycloak();
    return con;
  }

  async getUser(userId) {
    try {
      const kc = this.getKcClient();
      return await kc.users.findOne({
        id: userId,
      });
    } catch (e) {
      console.log('getUser', e);
      return false;
    }
  }

  async getUsers() {
    try {
      const kc = this.getKcClient();
      return await kc.users.find();
    } catch (e) {
      console.log('getUsers', e);
      return false;
    }
  }

  async getUsersWithPredicate(predicate) {
    try {
      const kc = this.getKcClient();
      return await kc.users.find(predicate);
    } catch (e) {
      console.log('getUsersWithPredicate', e);
      return false;
    }
  }

  async addUser(user) {
    try {
      const kc = this.getKcClient();
      return await kc.users.create(user);
    } catch (e) {
      console.log('addUser', e);
      return false;
    }
  }

  async getUsersFromGroup(groupId) {
    try {
      const kc = this.getKcClient();
      return await kc.groups.listMembers({ id: groupId });
    } catch (e) {
      console.log('getUsersFromGroup', e);
      return false;
    }
  }

  async addUserToGroup(userId, groupId) {
    try {
      const kc = this.getKcClient();
      await kc.users.addToGroup({ groupId: groupId, id: userId });
      return true;
    } catch (e) {
      console.log('addUserToGroup', e);
      return false;
    }
  }

  async removeUserFromGroup(userId, groupId) {
    try {
      const kc = this.getKcClient();
      await kc.users.delFromGroup({ groupId: groupId, id: userId });
      return true;
    } catch (e) {
      console.log('removeUserFromGroup', e);
      return false;
    }
  }

  async getGroupListUser(userId) {
    try {
      const kc = this.getKcClient();
      return await kc.users.listGroups({ id: userId });
    } catch (e) {
      console.log('getGroupListUser', e);
      return false;
    }
  }

  async getGroupList(mask = null) {
    try {
      const kc = this.getKcClient();
      const groups = (await kc.groups.find()).filter(
        (value) => mask != value.name
      );
      return groups;
    } catch (e) {
      console.log('getGroupList', e);
      return false;
    }
  }

  async deleteUser(userId) {
    try {
      const kc = this.getKcClient();
      const user = await kc.users.findOne({ id: userId });
      // si email de user n'a pas le domaine EMAIL_DOMAIN_PROTECT, on le supprime de keycloak.
      if (
        user &&
        user !== null &&
        ((EMAIL_DOMAIN_PROTECT && !user.email.includes(EMAIL_DOMAIN_PROTECT)) ||
          !EMAIL_DOMAIN_PROTECT)
      ) {
        await kc.users.del({ id: userId });
      }
      return true;
    } catch (e) {
      console.log('deleteUser', e);
      return false;
    }
  }
}

export default new KeycloakApiService();
