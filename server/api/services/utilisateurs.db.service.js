import l from '../../common/logger';

const mongoose = require('mongoose');
const Utilisateur = mongoose.model('Utilisateur');

const { encrypt, decrypt } = require('../vendors/crypto');

class UtilisateursDatabase {
  constructor() {
    this.decypherNotif = this.decypherNotif.bind(this);
    this.decypherEquipe = this.decypherEquipe.bind(this);
    this.decypherUtilisateur = this.decypherUtilisateur.bind(this);
    this.cypherUtilisateur = this.cypherUtilisateur.bind(this);
    this.cypherEquipe = this.cypherEquipe.bind(this);
    this.cypherNotif = this.cypherNotif.bind(this);
  }

  cypherUtilisateur(utilisateur) {
    try {
      if (utilisateur.login) utilisateur.login = encrypt(utilisateur.login);
      utilisateur.libelle = encrypt(utilisateur.libelle);
      if (utilisateur.equipes)
        utilisateur.equipes = utilisateur.equipes.map(this.cypherEquipe);
      if (utilisateur.notifs)
        utilisateur.notifs = utilisateur.notifs.map(this.cypherNotif);
      utilisateur.groupe = encrypt(utilisateur.groupe);

      return utilisateur;
    } catch (err) {
      console.error(
        "Erreur de fonction de crypherUtilisateur() avec l'arguments d'utilisateur: ",
        utilisateur,
        err
      );
      return null;
    }
  }

  cypherEquipe(equipe) {
    try {
      equipe.groupe = encrypt(equipe.groupe);
      return equipe;
    } catch (err) {
      console.error(
        "Erreur de fonction de cypherEquipe() avec l'arguments d'equipe: ",
        equipe,
        err
      );
      return null;
    }
  }

  cypherNotif(notif) {
    try {
      notif.categorie = encrypt(notif.categorie);
      notif.individu = encrypt(notif.individu);
      return notif;
    } catch (err) {
      console.error(
        "Erreur de fonction de cypherNotif() avec l'arguments de notif: ",
        notif,
        err
      );
      return null;
    }
  }

  cypherFiltres(filtres) {
    try {
      return filtres.map((filtre) => {
        return {
          cle: encrypt(filtre.cle),
          valeur: encrypt(filtre.valeur),
        };
      });
    } catch (err) {
      console.error(
        "Erreur de fonction de cypherFiltres() avec l'arguments de filtres: ",
        filtres,
        err
      );
      return null;
    }
  }

  decypherFiltres(filtres) {
    try {
      return filtres.map((filtre) => {
        return {
          cle: decrypt(filtre.cle),
          valeur: decrypt(filtre.valeur),
        };
      });
    } catch (err) {
      console.error(
        "Erreur de fonction de decypherFiltres() avec l'arguments de filtres: ",
        filtres,
        err
      );
      return null;
    }
  }

  decypherEquipe(equipe) {
    try {
      return equipe;
    } catch (err) {
      console.error(
        "Erreur de fonction de decypherEquipe() avec l'arguments d'equipe: ",
        equipe,
        err
      );
      return null;
    }
  }

  decypherNotif(notif) {
    try {
      notif.individu = decrypt(notif.individu);
      notif.categorie = decrypt(notif.categorie);
      return notif;
    } catch (err) {
      console.error(
        "Erreur de fonction de decypherNotif() avec l'arguments de notif: ",
        notif,
        err
      );
      return null;
    }
  }

  decypherUtilisateur(utilisateur) {
    if (!utilisateur) {
      console.error('Utilisateur non recupéré');
      return null;
    }
    try {
      if (utilisateur.login) utilisateur.login = decrypt(utilisateur.login);
      if (utilisateur.libelle)
        utilisateur.libelle = decrypt(utilisateur.libelle);
      if (utilisateur.equipes && utilisateur.equipes.length > 0)
        utilisateur.equipes = utilisateur.equipes.map(this.decypherEquipe);
      if (utilisateur.notifs && utilisateur.notifs.length > 0)
        utilisateur.notifs = utilisateur.notifs.map((notif) =>
          this.decypherNotif(notif)
        );
      utilisateur.groupe = decrypt(utilisateur.groupe);
      if (utilisateur.filtres)
        utilisateur.filtres = this.decypherFiltres(utilisateur.filtres);
      return utilisateur;
    } catch (err) {
      console.error(
        "Erreur de fonction de decypherUtilisateur() avec l'arguments d'utilisateur: ",
        utilisateur,
        err
      );
      return null;
    }
  }

  async all() {
    try {
      const users = await Utilisateur.find();
      return users.map(this.decypherUtilisateur);
    } catch (err) {
      console.error("Erreur de fonction de all() sans l'arguments ", err);
      return null;
    }
  }

  async byId(id) {
    try {
      const user = await Utilisateur.findById(id);
      if (!user) {
        console.error('Utilisateur non trouvé');
        return null;
      }
      return this.decypherUtilisateur(user);
    } catch (err) {
      console.error(
        "Erreur de fonction de byId() avec l'arguments de 'id' d'utilisateur: ",
        id,
        err
      );
      return null;
    }
  }

  async byIdKc(id_kc) {
    try {
      const r = await Utilisateur.findOne({ k_userId: id_kc });
      if (r) return this.decypherUtilisateur(r);
      throw new Error('Utilisateur inconnu');
    } catch (err) {
      console.error(
        "Erreur de fonction de byIdKc() avec l'arguments de 'id_kc' d'utilisateur: ",
        id_kc,
        err
      );
      return null;
    }
  }

  async byLogin(login) {
    try {
      const r = await Utilisateur.findOne({ login: login });
      return this.decypherUtilisateur(r);
    } catch (err) {
      console.error(
        "Erreur de fonction de byLogin() avec l'arguments de 'login' d'utilisateur: ",
        login,
        err
      );
      return null;
    }
  }

  async allByActeur(id) {
    try {
      const users = await Utilisateur.find({ 'organisation.acteur_id': id });
      return users.map(this.decypherUtilisateur);
    } catch (err) {
      console.error(
        "Erreur de fonction de allByActeur() avec l'arguments de 'id' d'acteur: ",
        id,
        err
      );
      return null;
    }
  }

  async insert(data) {
    try {
      const user = new Utilisateur({ ...this.cypherUtilisateur(data) });
      l.info(`${this.constructor.name}.insert()`);
      const saveUser = await user.save();
      return this.decypherUtilisateur(saveUser);
    } catch (err) {
      console.error(
        "Erreur de fonction de insert() avec l'arguments de 'data': ",
        data,
        err
      );
      return null;
    }
  }

  async modify(data) {
    try {
      l.info(`${this.constructor.name}.modify()`);
      const user = await Utilisateur.findOneAndUpdate(
        { _id: data._id },
        {
          $set: {
            login: encrypt(data.login),
            organisation: data.organisation,
            libelle: encrypt(data.libelle),
            groupe: encrypt(data.groupe),
            k_groupId: encrypt(data.k_groupId),
            actif: data.actif,
            equipes: data.equipes.map(this.cypherEquipe),
          },
        }
      );
      return this.decypherUtilisateur(user);
    } catch (err) {
      console.error(
        "Erreur de fonction de modify() avec l'arguments de 'data': ",
        data,
        err
      );
      return null;
    }
  }

  async updateEquipe(data) {
    try {
      l.info(`${this.constructor.name}.updateEquipe()`);
      return Utilisateur.findOneAndUpdate(
        { _id: data.utilisateur },
        {
          $set: {
            equipes: data.equipes.map(this.cypherEquipe),
          },
        }
      );
    } catch (err) {
      console.error(
        "Erreur de fonction de updateEquipe() avec l'arguments de 'data': ",
        data,
        err
      );
      return null;
    }
  }

  async updateOrganisation(data) {
    try {
      l.info(`${this.constructor.name}.updateOrganisation()`);
      const user = await Utilisateur.findOneAndUpdate(
        { _id: data._id },
        {
          $set: {
            'organisation.acteur_id': data.acteur_id,
          },
        }
      );
      return this.decypherUtilisateur(user);
    } catch (err) {
      console.error(
        "Erreur de fonction de updateOrganisation() avec l'arguments de 'data': ",
        data,
        err
      );
      return null;
    }
  }

  async addNotif(user_id, data) {
    try {
      const user = await Utilisateur.findOne({
        _id: user_id,
        'notifs.dossier_id': data.dossier_id,
        'notifs.categorie': data.categorie,
      });

      if (user !== null) {
        const userData = await Utilisateur.findOneAndUpdate(
          {
            _id: user_id,
            'notifs.dossier_id': data.dossier_id,
            'notifs.categorie': data.categorie,
          },
          {
            $set: {
              'notifs.$.lu': 0,
              'notifs.$.created': data.created,
              'notifs.$.urgent': data.urgent || false,
            },
          }
        );
        return this.decypherUtilisateur(userData);
      } else {
        const userData = await Utilisateur.findOneAndUpdate(
          { _id: user_id },
          {
            $push: {
              notifs: this.cypherNotif(data),
            },
          }
        );
        return this.decypherUtilisateur(userData);
      }
    } catch (err) {
      console.error(
        "Erreur de fonction de addNotif() avec l'arguments de 'user_id' et 'data': ",
        user_id,
        data,
        err
      );
      return null;
    }
  }

  async modifyNotif(id) {
    try {
      const user = await Utilisateur.findOneAndUpdate(
        { 'notifs._id': id },
        {
          $set: {
            'notifs.$.lu': 1,
          },
        },
        { new: true }
      );
      return this.decypherUtilisateur(user);
    } catch (err) {
      console.error(
        "Erreur de fonction de modifyNotif() avec l'arguments de 'id': ",
        id,
        err
      );
      return null;
    }
  }

  async deleteNotif(id) {
    try {
      const user = await Utilisateur.findOneAndUpdate(
        { 'notifs._id': id },
        {
          $pull: {
            notifs: { _id: id },
          },
        },
        { new: true }
      );
      return this.decypherUtilisateur(user);
    } catch (err) {
      console.error(
        "Erreur de fonction de deleteNotif() avec l'arguments de 'id': ",
        id,
        err
      );
      return null;
    }
  }

  async allByEquipe(id) {
    try {
      const users = await Utilisateur.find({ 'equipes.acteur_id': id });
      return users.map(this.decypherUtilisateur);
    } catch (err) {
      console.error(
        "Erreur de fonction de allByEquipe() avec l'arguments de 'id': ",
        id,
        err
      );
      return null;
    }
  }

  async setFiltresParams(id, filtres) {
    try {
      const user = await Utilisateur.findOneAndUpdate(
        {
          _id: id,
        },
        {
          $set: {
            filtres: this.cypherFiltres(filtres),
          },
        },
        {
          new: true,
        }
      );
      return this.decypherUtilisateur(user);
    } catch (err) {
      console.error(
        "Erreur de fonction de setFiltresParams() avec l'arguments de 'id' et 'filtres': ",
        id,
        filtres,
        err
      );
      return null;
    }
  }

  async refreshDataUtilisateur(id, data) {
    try {
      const user = await Utilisateur.findOneAndUpdate(
        {
          k_userId: id,
        },
        {
          $set: {
            login: encrypt(data.login),
            libelle: encrypt(data.libelle),
          },
        },
        {
          new: true,
        }
      );
      return this.decypherUtilisateur(user);
    } catch (err) {
      console.error(
        "Erreur de fonction de refreshDataUtilisateur() avec l'arguments de 'id' et 'data': ",
        id,
        data,
        err
      );
      return null;
    }
  }

  async deleteUtilisateur(id) {
    try {
      return await Utilisateur.findByIdAndDelete(id);
    } catch (err) {
      console.error(
        "Erreur de fonction de deleteUtilisateur() avec l'arguments de 'id': ",
        id,
        err
      );
      return null;
    }
  }
}

export default new UtilisateursDatabase();
