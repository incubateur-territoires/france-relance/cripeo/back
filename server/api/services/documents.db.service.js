const mongoose = require('mongoose');
const Document = mongoose.model('Document');

class DocumentsDatabase {
  constructor() {}

  async all() {
    return Document.find();
  }

  async byId(id) {
    return Document.findById(id);
  }

  async byEchangeId(echange_id) {
    return Document.find({ echange_id: echange_id });
  }

  async byDossierId(dossier_id) {
    return Document.find({ dossier_id: dossier_id });
  }

  async byDossierIdByActeur(dossierId, acteurId) {
    return Document.find({
      dossier_id: dossierId,
      'acteurs.acteur_id': acteurId,
    });
  }

  async insert(data) {
    const documents = new Document({ ...data });
    return await documents.save();
  }

  async byActeur(acteur_id) {
    return Document.find({
      'acteurs.acteur_id': acteur_id,
    });
  }

  async delete(id) {
    return Document.findByIdAndDelete(id);
  }

  async modifyActionId(data) {
    return Document.findOneAndUpdate(
      { _id: data.documentId },
      {
        $set: {
          action_id: data.actionId,
        },
      }
    );
  }

  async addActeurDocument(document_id, acteur_id) {
    return Document.findOneAndUpdate(
      { _id: document_id },
      {
        $push: {
          acteurs: {
            acteur_id: acteur_id,
          },
        },
      }
    );
  }

  async modifyDroits(data) {
    if (!data || !data.documentId || !data.acteurs)
      throw new Error('Les informations du documents ne sont pas valides');
    try {
      return Document.findOneAndUpdate(
        { _id: data.documentId },
        {
          $set: {
            acteurs: data.acteurs,
          },
        }
      );
    } catch (err) {
      throw new Error('Problème de modifications des droits du document', err);
    }
  }

  async modifyEchangeId(id, echangeId) {
    if (!id) throw new Error("Id de document n'est pas valide");
    try {
      return Document.findOneAndUpdate(
        { _id: id },
        {
          $set: {
            echange_id: echangeId,
          },
        }
      );
    } catch (err) {
      throw new Error('Problème de modification echange_id du document', err);
    }
  }

  async deleteGlobal() {
    await Document.deleteMany();
    return true;
  }

  async insertClear(data) {
    await Document.save(data);
    return true;
  }
}

export default new DocumentsDatabase();
