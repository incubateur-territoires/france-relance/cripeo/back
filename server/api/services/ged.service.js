import api from './ged.api.service.js';
import axios from 'axios';
const { decryptStream } = require('../vendors/crypto');

class GedService {
  getTicket() {
    return api.getTicket();
  }

  ajoutDossierRacine(nom) {
    if (!nom || nom.length < 1) throw 'Nom de dossier invalide';
    return api.creationDossier(nom);
  }

  ajoutSousDossier(nom, nodeRefParent) {
    if (!nom || nom.length < 1) throw 'Nom de dossier invalide';
    if (!nodeRefParent || nodeRefParent.length < 1)
      throw 'Noderef du dossier parent invalide';
    return api.creationDossierAvecDescription(nom, nodeRefParent);
  }

  ajoutFichier(fichier, nodeRefDestination) {
    if (!fichier || (Array.isArray(fichier) && fichier.length < 1))
      throw 'Fichier invalide';
    if (!nodeRefDestination || nodeRefDestination.length < 1)
      throw 'Noderef de destination invalide';
    return api.upload(fichier, nodeRefDestination);
  }

  b64EncodeUnicode(str) {
    // TODO Changer btoa deprecated
    // eslint-disable-next-line no-undef
    return btoa(
      encodeURIComponent(str).replace(
        /%([0-9A-F]{2})/g,
        function toSolidBytes(match, p1) {
          return String.fromCharCode('0x' + p1);
        }
      )
    );
  }

  async recupeFichier(url, zip) {
    // eslint-disable-next-line no-useless-catch
    try {
      const file = await axios({
        url,
        method: 'GET',
        responseType: 'arraybuffer',
      });
      let filedecrypted = await decryptStream(file.data);
      if (!zip) {
        const buf = Buffer.from(filedecrypted, 'binary').toString('base64');

        return buf;
      } else {
        return filedecrypted;
      }
    } catch (e) {
      throw e;
    }
  }

  effaceFichier(nodeRef) {
    if (!nodeRef || nodeRef.length < 1) throw 'NodeRef du fichier invalide';
    return api.effaceFichier(nodeRef);
  }
}

export default new GedService();
