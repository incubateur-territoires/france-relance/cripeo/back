import l from '../../common/logger';
import db from './historique.db.service';

class HistoriquesService {

  all() {
    l.info(`${this.constructor.name}.all()`);
    return db.all();
  }

  byId(id) {
    return db.byId(id);
  }

  byDossierId(id) {
    return db.byDossierId(id);
  }

  create(data) {
    return db.insert(data);
  }

  delete(id){
    return db.delete(id);
  }

}

export default new HistoriquesService();