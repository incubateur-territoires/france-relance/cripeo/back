import config from '../../common/config';
import axios from 'axios';
import loggerWinston from '../../common/loggerWinston';
const FormData = require('form-data');
const fs = require('fs');
const SERVEUR = config.ged.url;
const USER = config.ged.user;
const PASSWORD = config.ged.password;
const ENV_GED = config.ged.env;
const REF_SITE = config.ged.refSite;
const { encryptStream } = require('../vendors/crypto');

const logger = loggerWinston.logger;

const tab1 = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ';
const tab2 = 'aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn';

const replaceChaine = (chaine, parser) => {
  return chaine.replace(/./g, function ($0) {
    return parser[$0] ? parser[$0] : $0;
  });
};

const parseSpec = (chaine) => {
  const rep2 = tab1.split('');
  const rep = tab2.split('');
  let myarray = new Array();
  let i = -1;
  while (rep2[++i]) {
    myarray[rep2[i]] = rep[i];
  }
  myarray['Œ'] = 'OE';
  myarray['œ'] = 'oe';
  return replaceChaine(chaine, myarray);
};

const nomFichierRecette = (nom) => {
  return ENV_GED === 'Recette'
    ? `rec-${nom}`
    : ENV_GED === 'DEV'
    ? `dev-${nom}`
    : nom;
};

class GedApiService {
  async checkConfig() {
    if (
      !USER ||
      !PASSWORD ||
      !USER.length < 1 ||
      PASSWORD.length < 1 ||
      !SERVEUR ||
      SERVEUR.length < 1 ||
      !REF_SITE ||
      REF_SITE.length < 1
    ) {
      logger.error('Configuration incomplète pour la ged');
      throw 'Configuration incompléte pour la GED';
    }
  }

  async getTicket() {
    const data = {
      username: USER,
      password: PASSWORD,
    };

    const response = await axios
      .post(`${SERVEUR}/alfresco/service/api/login`, data)
      .catch((err) => {
        logger.error(
          '🚀 ~ file: ged.api.service.js ~ line 177 ~ GedApiService ~ getTicket ~ error',
          err
        );
        throw err;
      });

    if (
      !response ||
      !response.data ||
      !response.data.data ||
      !response.data.data.ticket ||
      response.data.data.ticket.length < 1
    ) {
      logger.error('Erreur ticket');
      throw 'Pas de ticket pour la ged';
    }
    return response.data.data.ticket;
  }

  async creationDossier(nom) {
    const ticket = await this.getTicket();
    const chaine = nomFichierRecette(parseSpec(nom));
    const data = {
      name: chaine,
      nodeType: 'cm:folder',
    };
    return await axios
      .post(
        `${SERVEUR}/alfresco/api/-default-/public/alfresco/versions/1/nodes/${REF_SITE}/children?alf_ticket=${ticket}`,
        data
      )
      .catch((err) => {
        logger.error(err);
        throw err;
      });
  }

  async creationDossierAvecDescription(nom, nodeRefDossier) {
    const ticket = await this.getTicket();
    // const chaine = nomFichierRecette(parseSpec(nom));
    const data = {
      name: nom,
      nodeType: 'cm:folder',
      properties: {
        'cm:title': nom,
        'cm:description': 'dossier' + nom,
      },
    };
    return await axios
      .post(
        `${SERVEUR}/alfresco/api/-default-/public/alfresco/versions/1/nodes/${nodeRefDossier}/children?alf_ticket=${ticket}`,
        data
      )
      .catch((err) => {
        logger.error(err);
        throw err;
      });
  }

  async upload(fichier, noderef) {
    const ticket = await this.getTicket();
    const timestamp = new Date().getTime();
    let filedata;
    if (fichier.filedata) filedata = fichier.filedata;
    else filedata = fichier;

    const formData = new FormData();

    let file;
    let filecrypted;

    try {
      file = await fs.createReadStream(filedata.tempFilePath);
      filecrypted = await encryptStream(file);
    } catch (err) {
      try {
        filecrypted = await encryptStream(file);
      } catch (e) {
        logger.error(e);
        throw e;
      }
      logger.error(err);
      throw err;
    }
    try {
      await formData.append('filename', timestamp);
      await formData.append(
        'destination',
        'workspace://SpacesStore/' + noderef
      );
      await formData.append('filedata', filecrypted, filedata.name);
    } catch (err) {
      logger.error(err);
    }

    try {
      const res = await axios.post(
        `${SERVEUR}/alfresco/service/api/upload?alf_ticket=${ticket}`,
        formData,
        {
          headers: {
            'Content-Type':
              'multipart/form-data; boundary=' + formData.getBoundary(),
          },
          maxContentLength: 1024 ** 3,
        }
      );
      fs.unlinkSync(filedata.tempFilePath);
      return res;
    } catch (error) {
      logger.error(
        '🚀 ~ file: ged.api.service.js ~ line 181 ~ GedApiService ~ upload ~ error',
        error
      );
      throw error;
    }
  }

  async effaceFichier(noderef) {
    const ticket = await this.getTicket();
    await axios
      .delete(
        `${SERVEUR}/alfresco/api/-default-/public/alfresco/versions/1/nodes/${noderef}?alf_ticket=${ticket}`
      )
      .catch((err) => {
        logger.error(err);
        throw err;
      });
    return true;
  }
}

export default new GedApiService();
