import l from '../../common/logger';
import db from './acteurs.db.service';
import DossierService from './dossiers.service';
import axios from 'axios';

class ActeursService {
  all() {
    l.info(`${this.constructor.name}.all()`);
    return db.all();
  }

  byId(id) {
    return db.byId(id);
  }

  create(data) {
    return db.insert(data);
  }

  modify(data) {
    return db.modify(data);
  }

  modifyEmail(data) {
    return db.modifyEmail(data);
  }

  deleteEmail(data) {
    return db.deleteEmail(data);
  }

  addEmail(id, data) {
    return db.addEmail(id, data);
  }

  changeFrequenceMail(id, frequence) {
    return db.changeFrequenceMail(id, frequence);
  }

  changeDateDernierRappel(id, date) {
    return db.changeDateDernierRappel(id, date);
  }

  async getEchangebyActeur(id) {
    return await db.getEchangebyActeur(id);
  }

  async getLastActions(id) {
    return await db.getLastActions(id);
  }

  async getUrgentLastActions(id) {
    return await db.getUrgentLastActions(id);
  }

  byParentId(id) {
    return db.byParentId(id);
  }

  addSecteur(id, secteur_Value) {
    return db.addSecteur(id, secteur_Value);
  }

  async editSecteur(id, valeur) {
    const secteur = await db.editSecteur(id, valeur);
    DossierService.updateSecteurDossiers(id, valeur);
    return secteur;
  }

  async deleteSecteur(secteur_Id) {
    // TODO : supprimer le secteur de chaque dossier
    const acteur = await db.deleteSecteur(secteur_Id);
    DossierService.deleteSecteurDossiers(secteur_Id);
    return acteur;
  }

  async addSecteurGeo(id, secteur_url) {
    const properties = await this.analyseSecteurGeo(secteur_url);
    if (properties) {
      return db.addSecteurGeo(id, secteur_url, properties);
    }
    return false;
  }

  async editSecteurGeo(id, actif, url) {
    const properties = await this.analyseSecteurGeo(url);
    if (properties) {
      const secteur = await db.editSecteurGeo(id, actif, url, properties);
      return secteur;
    }
    return false;
  }

  async analyseSecteurGeo(url) {
    let properties;
    let errors = false;
    try {
      const no_proxy = url.includes(process.env.NO_PROXY);
      const opt =
        process.env.PROXY_HOST && !no_proxy
          ? {
              proxy: {
                host: process.env.PROXY_HOST,
                port: process.env.PROXY_PORT,
              },
            }
          : {};
      const propertiesCall = await axios.get(url, opt);
      properties = propertiesCall.data?.features?.map((feature) => {
        if (feature?.properties?.id && feature?.properties?.nom)
          return {
            code: feature?.properties?.id,
            libelle: feature?.properties?.nom,
          };
        errors = true;
      });
    } catch (err) {
      console.error("L'url ne fonctionne pas", err);
      return false;
    }
    if (errors) {
      console.error('Le format GeoJSON ne correspond pas à la demande');
      return false;
    }
    return properties;
  }
}

export default new ActeursService();
