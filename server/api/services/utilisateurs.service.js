import db from './utilisateurs.db.service';

import DossierService from './dossiers.service';
import CategorieService from './categories.service';

import LoggerWinston from '../../common/loggerWinston';
const logger = LoggerWinston.logger;

class UtilisateursService {
  all() {
    logger.info(`${this.constructor.name}.all()`);
    return db.all();
  }

  byId(id) {
    return db.byId(id);
  }

  async byIdKc(id_kc) {
    try {
      return await db.byIdKc(id_kc);
    } catch (err) {
      return false;
    }
  }

  byLogin(login) {
    return db.byLogin(login);
  }

  allByActeur(id) {
    return db.allByActeur(id);
  }

  create(data) {
    return db.insert(data);
  }

  modify(data) {
    return db.modify(data);
  }

  updateEquipe(data) {
    return db.updateEquipe(data);
  }

  updateOrganisation(data) {
    return db.updateOrganisation(data);
  }

  checkLogin(login) {
    return this.byLogin(login);
  }

  async addNotif(dossier_id, categorie_id, acteur_id, urgent = false) {
    const dossier = await DossierService.byId(dossier_id);
    const utilisateurs = await this.allByActeur(acteur_id);
    const categorie = await CategorieService.byId(categorie_id);

    await Promise.all(
      utilisateurs.map(async (user) => {
        const obj = {
          dossier_id: dossier_id,
          categorie: categorie.libelle,
          individu:
            dossier.individuPrincipal.nom +
            ' ' +
            dossier.individuPrincipal.prenom,
          created: new Date(),
          urgent,
        };
        await db.addNotif(user._id, obj);
      })
    );
  }

  modifyNotif(id) {
    return db.modifyNotif(id);
  }

  deleteNotif(id) {
    return db.deleteNotif(id);
  }

  allByEquipe(id) {
    return db.allByEquipe(id);
  }

  setFiltresParams(id, filtres) {
    return db.setFiltresParams(id, filtres);
  }

  refreshDataUtilisateur(id, data) {
    return db.refreshDataUtilisateur(id, data);
  }

  deleteUtilisateur(id) {
    return db.deleteUtilisateur(id);
  }
}

export default new UtilisateursService();
