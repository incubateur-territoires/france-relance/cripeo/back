import l from '../../common/logger';
import db from './categories.db.service';
import EchangeService from './echanges.service';
class CategoriesService {
  all() {
    l.info(`${this.constructor.name}.all()`);
    return db.all();
  }

  byId(id) {
    return db.byId(id);
  }

  create(data) {
    return db.insert(data);
  }

  async modify(data) {
    const categorie = await db.modify(data);
    await EchangeService.changeLibelleCategorieAllEchange(
      data.libelle,
      data._id
    );
    return categorie;
  }

  addAction(id, data) {
    return db.addAction(id, data);
  }

  removeAction(data) {
    return db.removeAction(data);
  }

  updateAction(data) {
    return db.updateAction(data);
  }

  byActeur(id) {
    return db.byActeur(id);
  }

  byActeurEmeDes(id) {
    return db.byActeurEmeDes(id);
  }

  byActeurNonEchange(id) {
    return db.byActeurNonEchange(id);
  }

  removeCategorie(id) {
    return db.removeCategorie(id);
  }
}

export default new CategoriesService();
