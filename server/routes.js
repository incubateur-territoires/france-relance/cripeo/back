import dossiersRouter from './api/controllers/dossiers/router';
import historiquesRouter from './api/controllers/historiques/router';
import acteursRouter from './api/controllers/acteurs/router';
import civilitesRouter from './api/controllers/civilites/router';
import utilisateursRouter from './api/controllers/utilisateurs/router';
import rolesRouter from './api/controllers/role/router';
import keycloakRouter from './api/controllers/keycloak/router';
import categoriesRouter from './api/controllers/categories/router';
import actionsRouter from './api/controllers/actions/router';
import echangesRouter from './api/controllers/echanges/router';
import circonscriptionsRouter from './api/controllers/circonscriptions/router';
import gedsRouter from './api/controllers/ged/router';
import documentsRouter from './api/controllers/documents/router';
import frequencesRouter from './api/controllers/frequences/router';
import rappelsRouter from './api/controllers/rappels/router';
import videosTutoRouter from './api/controllers/tutoriel/router';

export default function routes(app) {
  app.use('/api/v1/rappels', rappelsRouter);
  app.use('/api/v1/dossiers', dossiersRouter);
  app.use('/api/v1/historiques', historiquesRouter);
  app.use('/api/v1/acteurs', acteursRouter);
  app.use('/api/v1/civilites', civilitesRouter);
  app.use('/api/v1/utilisateurs', utilisateursRouter);
  app.use('/api/v1/roles', rolesRouter);
  app.use('/api/v1/keycloak', keycloakRouter);
  app.use('/api/v1/categories', categoriesRouter);
  app.use('/api/v1/actions', actionsRouter);
  app.use('/api/v1/echanges', echangesRouter);
  app.use('/api/v1/circonscriptions', circonscriptionsRouter);
  app.use('/api/v1/ged', gedsRouter);
  app.use('/api/v1/documents', documentsRouter);
  app.use('/api/v1/frequences', frequencesRouter);
  app.use('/api/v1/tutoriel', videosTutoRouter);
}
