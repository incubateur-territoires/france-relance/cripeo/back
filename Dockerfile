# Etape 1 : Création du build de production
FROM node:20.9.0

# Installation des dépendances du projet
WORKDIR /

COPY package.json package.json
COPY .babelrc .babelrc
COPY .env .env

# Récupération des fichiers du projets
COPY dist .
COPY node_modules /node_modules
COPY public ./public

EXPOSE 8080
RUN echo $(ls)
CMD ["node", "index.js"]
