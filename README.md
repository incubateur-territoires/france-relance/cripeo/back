# Application Cripéo

## Fonctionnement de Cripéo

Cripéo utilise les outils suivants pour que tout fonctionne correctement : 
- Keycloak
- Graylog
- Un serveur SMTP

### Keycloak

#### Liste des rôles necessaires :
 - 'user',
 - 'Dossier_Lecture'
 - 'Dossier_Ecriture'
 - 'Organisation_Creation'
 - 'Organisations_Globales'
 - 'Organisation_Gestion'

#### Liste des groupes necessaires :
 - 'Administrateur Fonctionnel'
 - 'Administrateur Organisationnel'
 - 'Utilisateur'

##### Les rôles de l'Administrateur Fonctionnel
 - 'user',
 - 'Dossier_Lecture'
 - 'Dossier_Ecriture'
 - 'Organisation_Creation'
 - 'Organisations_Globales'
 - 'Organisation_Gestion'

##### Les rôles de l'Administrateur Organisationnel
 - 'user',
 - 'Dossier_Lecture'
 - 'Dossier_Ecriture'
 - 'Organisation_Gestion'

##### Les rôles de l'Utilisateur
 - 'user',
 - 'Dossier_Lecture'
 - 'Dossier_Ecriture'

### Keycloak Admin
Pour l'ajout d'utilisateurs, vous devrez créer un utilisateur admin 
Ce compte permet d'avoir une liaison vers Keycloak avec une intéraction forte de l'api.

Vous devrez spécifier les droits de ce compte.

### GED Alfresco et stockage des documents

Anciennement l'outil utilisait une GED Alfesco pour stocker les documents, le stockage se fait maintenant directement sur le serveur, vous pouvez lier un volume docker sur le répertoire : 
`documentsInterne`

La ged Alfresco est toujours utilisée dans l'application pour stocker encore les derniers documents. Si vous débutez une nouvelle instance de cripéo, vous pouvez modifier et supprimer l'utilisation de cette ged.

### Equipe

Dans Cripéo vous pouvez créer des organisations.
Une fonctionnalité demandée était d'ajouter des équipes dans ces organisations qui devaient correspondre à des organisations simplifiées.

Les équipes permettent de faire des silos d'échanges. Le reste de l'organisation ne peut pas voir les informations et documents.

Cependant la fonctionnalité a été dépréciée. Mais il est toujours possible de l'utiliser. Mais l'utilisation peut générer des anomalies.

`Eviter l'utilisation`

### Secteur géographique

Il est possible d'ajouter des secteurs géographiques via un flux geojson.

`/!\` Cette fonctionnalité a été demandée pour une organisation, un seul flux a été mis en place,si vous devez l'utiliser, il sera preferable de tester avant toute utilisation en production.

## Configuration du fichier docker-compose

### Variables du Back-end

```
  - PROXY_HOST: L'adresse de votre serveur proxy
  - PROXY_PORT: Le port de votre serveur proxy
  - PORT: Port interne à l'application, par défaut "5000"
  - FLUX_CIRCO: L'adresse de l'API Geojson concernant le découpage des circonscriptions d'action sociale
  - API_BANO_URL: Adresse de l'API BANO du gouvernement ("https://api-adresse.data.gouv.fr")
  - KEYCLOAK_ADMIN_URL: L'adresse du serveur Keycloak pour l'administration
  - KEYCLOAK_ADMIN_USER: L'utilisateur Keycloak
  - KEYCLOAK_ADMIN_PWD: Mot de passe pour Keycloak
  - KEYCLOAK_ADMIN_GRANT_TYPE: "password"
  - KEYCLOAK_ADMIN_CLIENT_ID: L'ID du client dans Keycloak pour l'administration
  - KEYCLOAK_ADMIN_REAL_NAME: Le nom du royaume Keycloak pour l'administration
  - KEYCLOAK_CLIENT_REALM: Le nom du royaume Keycloak pour le client (Souvent identique à KEYCLOAK_ADMIN_REAL_NAME)
  - KEYCLOAK_CLIENT_BEARERONLY: "true"
  - KEYCLOAK_CLIENT_SERVER_URL: L'adresse du serveur Keycloak pour le client (Souvent identique à KEYCLOAK_ADMIN_URL)
  - KEYCLOAK_CLIENT_SSL_REQUIRED: "external"
  - KEYCLOAK_CLIENT_CLIENT_ID: L'ID du client dans Keycloak 
  - KEYCLOAK_CLIENT_REALM_PUBLIC_KEY: Clé publique du royaume 
  - KEYCLOAK_CLIENT_SECRET: Clé privée du royaume

  - KEYCLOAK_NEWUSER_CREDENTIAL_TYPE: Type de credential, l'application est utilisé en type password
  - KEYCLOAK_NEWUSER_CREDENTIAL_DEFAULT_VALUE : Mot de passe par defaut à la création d'un nouvel utilisateur
  - KEYCLOAK_NEWUSER_CREDENTIAL_TEMPORARY: Le mot de passe est-il temporaire

  - ENCRYPTION_KEY: La clé de cryptage des données

  - MAILER_AUTH_TYPE: login ou oauth2 (login par défaut)
  - MAILER_HOST: Adresse du serveur SMTP
  - MAILER_PORT: Port du serveur SMTP
  - MAILER_IGNORE_CERTIFICATE: "false" par défaut
  - MAILER_DEBUG: "false" par défaut
  - MAILER_AUTHENTICATION: "true" si votre serveur SMTP est sécurisé
  - MAILER_AUTH_USER: Compte utilisateur de l'application pour le serveur SMTP
  - MAILER_AUTH_PASSWORD: Mot de passe du compte MAILER_AUTH_USER
  - MAILER_EMAIL: Courriel du compte MAILER_AUTH_USER

  - APP_NAME: Nom du container (par exemple "cripeo-back")
  - APP_SERVER: Serveur du back-end (par exemple "localhost")
  - APP_DB: Lien vers la base de données
  - WHITELISTCORS: Liste des CORS (par exemple "http://localhost:4584")

  - GRAYLOG_HOST: URL du serveur Graylog 
  - GRAYLOG_PORT: Port du serveur Graylog GELF

  - ACTIF_MAIL: Booléen permettant d'activer ou désactiver l'envoi de mails

  - EMAIL_DOMAIN_PROTECT: "@mondomaine.fr" Empêche la suppression les comptes de keycloak 
```
  #### Variables pour l'initialisation de la base de données (ces données doivent correspondre à celles de votre utilisateur Keycloak)
```

  - INSTALLATION_ACTEUR_LIBELLE: Libellé de l'acteur de l'administrateur
  - INSTALLATION_UTILISATEUR_LOGIN: Compte de l'administrateur
  - INSTALLATION_UTILISATEUR_LIBELLE: Libellé du compte
  - INSTALLATION_UTILISATEUR_K_USER: ID Keycloak de l'administrateur (par exemple "0294d907-443d-424e-afed-d85420af9462")
  - INSTALLATION_UTILISATEUR_K_GROUP: ID Keycloak du groupe de l'acteur (par exemple "bfe9f176-eef1-481d-b2bc-059b39d90387")`
  ```